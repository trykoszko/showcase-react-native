import React, {useEffect, useState} from 'react';
import {RegisterFormFirstStep} from '../components/register-form/RegisterFormFirstStep';
import {RegisterFormSecondStepPrivate} from '../components/register-form/RegisterFormSecondStepPrivate';
import {yupResolver} from "@hookform/resolvers/yup";
import {useForm} from "react-hook-form";
import * as yup from 'yup';
import {RegisterFormThirdStepPrivate} from "../components/register-form/RegisterFormThirdStepPrivate";
import {RegisterFormFourthStepPrivate} from "../components/register-form/RegisterFormFourthStepPrivate";
import {RegisterFormFifthStepPrivate} from "../components/register-form/RegisterFormFifthStepPrivate";
import {RegisterFormSecondStepCompany} from "../components/register-form/RegisterFormSecondStepCompany";
import {RegisterFormThirdStepCompany} from "../components/register-form/RegisterFormThirdStepCompany";
import {RegisterFormFourthStepCompany} from "../components/register-form/RegisterFormFourthStepCompany";
import {RegisterFormFifthStepCompany} from "../components/register-form/RegisterFormFifthStepCompany";
import {RegisterFormSixthStepCompany} from "../components/register-form/RegisterFormSixthStepCompany";

const formSchema = yup.object().shape({
    type: yup.string().required(),
    company_name: yup.string().optional(),
    company_description: yup.string().optional(),
    email: yup.string().required().min(3),
    password: yup.string().required().min(8),
    // image: yup..optional(),
    company_type: yup.string().optional(),
    kind: yup.string().optional(),
    people_limit: yup.number().optional(),
    has_staff: yup.boolean().optional(),
    can_travel: yup.boolean().optional(),
    can_invoice: yup.boolean().optional(),
    handled_events: yup.array().of(yup.object().shape({
        id: yup.number().required(),
        uuid: yup.string().required(),
        name: yup.string().required(),
        slug: yup.string().optional(),
        icon: yup.string().optional(),
        color: yup.string().optional()
    })).optional(),
    handled_languages: yup.array().of(yup.object().shape({
        id: yup.number().required(),
        uuid: yup.string().required(),
        name: yup.string().required(),
        native_name: yup.string().optional()
    })).optional(),
    age_restriction: yup.string().optional(),
    preferred_contact: yup.array().of(yup.string()).optional(),
    other_info: yup.string().optional(),
    service_time: yup.string().optional(),
    materials: yup.string().optional(),
    aim: yup.string().optional(),
    style: yup.string().optional(),
    first_name: yup.string().optional().min(3),
    last_name: yup.string().optional().min(3),
    birthdate: yup.date().optional(),
    city: yup.object().shape({
        id: yup.number().required(),
        uuid: yup.string().required(),
        name: yup.string().required(),
        country: yup.object().shape({
            id: yup.number().required(),
            uuid: yup.string().required(),
            name: yup.string().required(),
            native_name: yup.string().optional(),
            alpha_2: yup.string().optional(),
        })
    }).required(),
    elements: yup.array().of(yup.object()).optional()
});

export default function RegisterScreen() {
    const [formType, setFormType] = useState<string>('private');
    const [currentStep, setCurrentStep] = useState<number>(1);

    const {control, handleSubmit, watch, formState: {errors}, setValue, getValues} = useForm({
        resolver: yupResolver(formSchema)
    });

    useEffect(() => {
        if (currentStep < 1 || currentStep > 6) {
            setCurrentStep(1);
        }
    }, [currentStep]);

    useEffect(() => {
        setValue('type', formType);
    }, [formType]);

    return (
        <>
            {currentStep === 1 && (
                <RegisterFormFirstStep
                    setFormType={setFormType}
                    setCurrentStep={setCurrentStep}
                />
            )}
            {currentStep === 2 && (
                (formType === 'private' && (
                    <RegisterFormSecondStepPrivate
                        control={control}
                        watch={watch}
                        errors={errors}
                        setCurrentStep={setCurrentStep}
                    />
                ))
                ||
                (formType === 'company' && (
                    <RegisterFormSecondStepCompany
                        control={control}
                        watch={watch}
                        errors={errors}
                        setCurrentStep={setCurrentStep}
                    />
                ))
            )}
            {currentStep === 3 && (
                (formType === 'private' && (
                    <RegisterFormThirdStepPrivate
                        control={control}
                        watch={watch}
                        errors={errors}
                        setCurrentStep={setCurrentStep}
                    />
                ))
                ||
                (formType === 'company' && (
                    <RegisterFormThirdStepCompany
                        control={control}
                        watch={watch}
                        errors={errors}
                        setCurrentStep={setCurrentStep}
                    />
                ))
            )}
            {currentStep === 4 && (
                (formType === 'private' && (
                    <RegisterFormFourthStepPrivate
                        setCurrentStep={setCurrentStep}
                    />
                ))
                ||
                (formType === 'company' && (
                    <RegisterFormFourthStepCompany
                        control={control}
                        errors={errors}
                        setCurrentStep={setCurrentStep}
                        getValues={getValues}
                    />
                ))
            )}
            {currentStep === 5 && (
                (formType === 'private' && (
                    <RegisterFormFifthStepPrivate
                        control={control}
                        errors={errors}
                        handleSubmit={handleSubmit}
                        setCurrentStep={setCurrentStep}
                    />
                ))
                ||
                (formType === 'company' && (
                    <RegisterFormFifthStepCompany
                        setCurrentStep={setCurrentStep}
                    />
                ))
            )}
            {currentStep === 6 && (
                (formType === 'company' && (
                    <RegisterFormSixthStepCompany
                        control={control}
                        errors={errors}
                        handleSubmit={handleSubmit}
                        setCurrentStep={setCurrentStep}
                    />
                ))
            )}
        </>
    );
}
