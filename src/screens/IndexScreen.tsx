import {Text, View} from 'react-native';
import i18n from '../i18n';
import {EventsCarouselPublic} from "../components/events-carousel/EventsCarouselPublic";
import {CoorganiseListPublic} from "../components/coorganise-list/CoorganiseListPublic";

export default function IndexScreen()
{
    return (
        <View className="h-full flex flex-column">
            <View className="px-3 mt-5">
                <Text className="text-2xl font-bold">{i18n.indexScreen.title()}</Text>
                <Text className="text-md font-bold text-primary mb-4">
                    {i18n.indexScreen.subtitle}
                </Text>
            </View>
            <EventsCarouselPublic />
            <CoorganiseListPublic />
        </View>
    );
}
