import {KeyboardAvoidingView, Text, View} from 'react-native';
import i18n from '../i18n';
import LoginForm from '../components/login-form/LoginForm';
import {useTermsStore} from '../store/TermsStore';
import React, {useEffect} from 'react';

export default function AuthenticationScreen() {
    const setIsTermsAccepted = useTermsStore(state => state.setIsTermsAccepted);

    useEffect(() => {
        setIsTermsAccepted(true);
    }, []);

    return (
        <View className="h-full flex flex-column p-3">
            <KeyboardAvoidingView
                behavior={'height'}
                keyboardVerticalOffset={70}
                style={{flex: 1}}
            >
                <Text className="mb-8 text-gray-500">
                    {i18n.authenticationScreen.signIn}
                </Text>
                <LoginForm/>
            </KeyboardAvoidingView>
        </View>
    );
}
