import {ImageBackground} from 'expo-image';
import {RefreshControl, Text, View} from 'react-native';
import ScrollableLayoutFullheight from '../../components/layout/ScrollableLayoutFullheight';
import React, {useState} from 'react';
import {EventHeader} from "../../components/event-header/EventHeader";
import {EventDetails} from "../../components/event-details/EventDetails";
import {EventDescription} from "../../components/event-description/EventDescription";
import {EventLocation} from "../../components/event-location/EventLocation";
import {EventParticipants} from "../../components/event-participants/EventParticipants";
import {EventCoorganiseItem} from "../../components/event-coorganise-item/EventCoorganiseItem";
import {EventUser} from "../../types/EventUser.type";
import {EventDetail} from "../../types/EventDetail.type";
import {apiConfig} from "../../config/variables";
import {useAuthStore} from "../../store/AuthStore";
import {MyUser} from "../../types/MyUser.type";
import i18n from "../../i18n";
import {EventCoorganiseModal} from "../../components/coorganise-modal/EventCoorganiseModal";

type Props = {
    event: EventDetail | undefined,
    isFetching: boolean,
    refetch: () => void
}

export const EventScreen = ({event, isFetching, refetch}: Props) => {
    if (!event) {
        return (
            <ScrollableLayoutFullheight
                pb={100}
            >
                <Text>{i18n.eventScreen.eventNotFound}</Text>
            </ScrollableLayoutFullheight>
        );
    }

    const userData: MyUser | undefined = useAuthStore(state => state.userData);
    const eventOwner: EventUser | undefined = event?.users?.find((eventUser: EventUser) => eventUser.role === 'owner') ?? undefined;
    const isOwner: boolean = eventOwner?.uuid === userData?.uuid;
    const [isOfferModalVisible, setIsOfferModalVisible] = useState<boolean>(false);
    const [offerModalUuid, setOfferModalUuid] = useState<string>('');
    const [offerModalEventUuid, setOfferModalEventUuid] = useState<string>(event.uuid);

    return (
        <ScrollableLayoutFullheight
            pb={100}
            refreshControl={<RefreshControl refreshing={isFetching} onRefresh={refetch}/>}
        >
            <ImageBackground
                source={{uri: event?.image?.url ?? `${apiConfig.publicUrl}/assets/event-bg.jpg`}}
                style={{width: '100%', height: 220}}
                imageStyle={{borderTopLeftRadius: 4, borderTopRightRadius: 4}}
                className="flex flex-col justify-between items-between"
            />

            <View
                style={{
                    marginTop: -150
                }}
            >
                <EventHeader
                    dateTime={new Date(event.start_date)}
                    duration={event.duration}
                    eventUuid={event.uuid}
                />

                {eventOwner && (
                    <EventDetails
                        dateTime={new Date(event.start_date)}
                        duration={event.duration}
                        title={event.title}
                        creatorUuid={eventOwner.uuid}
                        creatorProfilePictureUrl={eventOwner?.avatar?.url ?? `${apiConfig.publicUrl}/assets/user.png`}
                        creatorName={eventOwner?.name}
                    />
                )}

                {event?.description && (
                    <EventDescription description={event.description}/>
                )}

                {event?.city && (
                    <EventLocation
                        locationName={event?.city?.name}
                        // distance={2}
                    />
                )}

                {event?.users?.length && (
                    <EventParticipants
                        participants={event?.users}
                        slots={event.people_limit}
                        slotsLeft={event.people_limit - event?.users?.length}
                        isOwner={isOwner}
                    />
                )}

                {event?.event_elements?.map((eventElement: any, index: number) => {
                    eventElement.event = event;

                    return (
                        <EventCoorganiseItem
                            key={index}
                            eventElement={eventElement}
                            isOwner={isOwner}
                            isFavourite={userData?.saved_event_event_elements?.find(uuid => uuid === eventElement.uuid) !== undefined}
                            setIsOfferModalVisible={setIsOfferModalVisible}
                            setOfferModalUuid={setOfferModalUuid}
                        />
                    )
                })}
            </View>

            <EventCoorganiseModal
                isOfferModalVisible={isOfferModalVisible}
                setIsOfferModalVisible={setIsOfferModalVisible}
                eventUuid={offerModalEventUuid}
                offerModalUuid={offerModalUuid}
                setOfferModalUuid={setOfferModalUuid}
            />
       </ScrollableLayoutFullheight>
    );
};
