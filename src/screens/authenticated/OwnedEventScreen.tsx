import {View} from 'react-native';
import ScrollableLayoutFullheight from '../../components/layout/ScrollableLayoutFullheight';
import React from 'react';
import {EventDetail} from "../../types/EventDetail.type";
import {ImageBackground} from "expo-image";
import {apiConfig} from "../../config/variables";
import {EventHeader} from "../../components/event-header/EventHeader";
import {EventDetails} from "../../components/event-details/EventDetails";
import {EventDescription} from "../../components/event-description/EventDescription";
import {EventLocation} from "../../components/event-location/EventLocation";
import {EventParticipants} from "../../components/event-participants/EventParticipants";
import {EventCoorganiseItem} from "../../components/event-coorganise-item/EventCoorganiseItem";
import {EventUser} from "../../types/EventUser.type";

type Props = {
    event: EventDetail | undefined,
    isFetching: boolean,
    refetch: () => void
}

export const OwnedEventScreen = ({event, isFetching, refetch}: Props) =>
{
    if (!event) {
        return <></>;
    }

    const eventOwner: EventUser | undefined = event?.users?.find((eventUser: EventUser) => eventUser.role === 'owner') ?? undefined;

    return (
        <ScrollableLayoutFullheight
            pb={100}
        >
            <ImageBackground
                source={{uri: event?.image?.url ?? `${apiConfig.publicUrl}/assets/event-bg.jpg`}}
                style={{width: '100%', height: 220}}
                imageStyle={{borderTopLeftRadius: 4, borderTopRightRadius: 4}}
                className="flex flex-col justify-between items-between"
            />

            <View
                style={{
                    marginTop: -150
                }}
            >
                <EventHeader
                    dateTime={new Date(event.start_date)}
                    duration={event.duration}
                    eventUuid={event.uuid}
                />

                {eventOwner && (
                    <EventDetails
                        dateTime={new Date(event.start_date)}
                        duration={event.duration}
                        title={event.title}
                        creatorUuid={eventOwner.uuid}
                        creatorProfilePictureUrl={eventOwner?.avatar?.url ?? `${apiConfig.publicUrl}/assets/user.png`}
                        creatorName={eventOwner.company_name ?? `${eventOwner?.first_name} ${eventOwner?.last_name}`}
                    />
                )}

                {event?.description && (
                    <EventDescription description={event.description}/>
                )}

                {event?.city && (
                    <EventLocation
                        locationName={event?.city?.name}
                        // distance={2}
                    />
                )}

                {event?.users?.length && (
                    <EventParticipants
                        participants={event?.users}
                        slots={event.people_limit}
                        slotsLeft={event.people_limit - event?.users?.length}
                    />
                )}

                {event?.event_elements?.map((eventElement: any, index: number) =>
                    <EventCoorganiseItem
                        key={index}
                        eventElement={eventElement}
                        isOwner={true}
                    />
                )}

            </View>
        </ScrollableLayoutFullheight>
    );
};
