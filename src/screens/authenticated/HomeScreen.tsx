import {Platform, Text, View} from 'react-native';
import {EventsCarousel} from '../../components/events-carousel/EventsCarousel';
import {StatusCircles} from '../../components/status-circles/StatusCircles';
import {CoorganiseList} from '../../components/coorganise-list/CoorganiseList';
import i18n from '../../i18n';
import {useAuthStore} from '../../store/AuthStore';
import ScrollableLayoutWithBottomBar from '../../components/layout/ScrollableLayoutWithBottomBar';
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {sizes} from "../../constants/sizes";

export const HomeScreen = () =>
{
    const userData = useAuthStore(state => state.userData);
    const insets = useSafeAreaInsets();

    return (
        <ScrollableLayoutWithBottomBar
            pb={
                Platform.OS === 'ios' ? sizes.bottomBarHeight : (80 + insets.bottom)
            }
        >
            <View className="flex flex-col mb-5 px-3">
                <Text
                    className="text-2xl font-bold text-darkGrey">{i18n.homeScreen.title(userData?.company_name ?? userData?.first_name)}</Text>
                <Text className="text-md font-bold text-primaryDark">{i18n.homeScreen.subtitle}</Text>
            </View>
            <StatusCircles/>
            <EventsCarousel/>
            <CoorganiseList/>
        </ScrollableLayoutWithBottomBar>
    );
};
