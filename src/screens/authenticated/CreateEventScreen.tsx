import {useState} from 'react';
import {CreateEventFirstStep} from '../../components/create-event-form/CreateEventFirstStep';
import {CreateEventSecondStep} from '../../components/create-event-form/CreateEventSecondStep';
import {CreateEventThirdStep} from '../../components/create-event-form/CreateEventThirdStep';
import {CreateEventFourthStep} from '../../components/create-event-form/CreateEventFourthStep';
import {CreateEventFifthStep} from '../../components/create-event-form/CreateEventFifthStep';
import {useGlobalLoaderStore} from '../../store/GlobalLoaderStore';
import {CreateEventFormDataType} from '../../types/CreateEventFormDataType.type';

export const CreateEventScreen = () =>
{
    const [currentStep, setCurrentStep] = useState<number>(1);
    const [formData, setFormData]       = useState<CreateEventFormDataType>({});
    const setIsLoading                  = useGlobalLoaderStore(state => state.setIsLoading);

    // useEffect(() =>
    //           {
    //               console.log('formData', formData);
    //           }, [formData]);

    const submitForm = async () =>
    {
        setTimeout(() =>
                   {
                       setIsLoading(false);
                       // @TODO: handle submit
                       // router.replace(`/event/${eventsMock[0].uuid}`);
                   }, 1000);
    };

    return (
        <>
            {currentStep === 1 && (
                <CreateEventFirstStep
                    setCurrentStep={setCurrentStep}
                    formData={formData}
                    setFormData={setFormData}
                />
            )}
            {currentStep === 2 && (
                <CreateEventSecondStep
                    setCurrentStep={setCurrentStep}
                    formData={formData}
                    setFormData={setFormData}
                />
            )}
            {currentStep === 3 && (
                <CreateEventThirdStep
                    setCurrentStep={setCurrentStep}
                    formData={formData}
                    setFormData={setFormData}
                />
            )}
            {currentStep === 4 && (
                <CreateEventFourthStep
                    setCurrentStep={setCurrentStep}
                    formData={formData}
                    setFormData={setFormData}
                />
            )}
            {currentStep === 5 && (
                <CreateEventFifthStep
                    setCurrentStep={setCurrentStep}
                    formData={formData}
                    setFormData={setFormData}
                    submitForm={submitForm}
                />
            )}
        </>
    );
};
