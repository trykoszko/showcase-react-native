import {
    Dimensions,
    KeyboardAvoidingView,
    Platform,
    RefreshControl,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {Chat} from '../../types/Chat.type';
import Layout from '../../components/layout/Layout';
import Ionicons from '@expo/vector-icons/Ionicons';
import {router} from 'expo-router';
import {ChatMessage} from '../../types/ChatMessage.type';
import {useAuthStore} from '../../store/AuthStore';
import {Message} from '../../components/chat-message/ChatMessage';
import {getFormattedDate} from '../../helpers/date-helpers';
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {useApi} from "../../hooks/useApi";
import {useAxios} from "../../hooks/useAxios";
import {useQuery} from "react-query";
import {AxiosResponse} from "axios";
import ScrollableLayoutFullheight from "../../components/layout/ScrollableLayoutFullheight";
import Loader from "../../components/loader/Loader";
import {apiConfig} from "../../config/variables";
import i18n from "../../i18n";

type Props = {
    chatTypeAndUuid: string
}

export const ChatScreen = ({chatTypeAndUuid}: Props) => {
    const axios = useAxios();
    const {sendChatMessage} = useApi();
    const insets = useSafeAreaInsets();
    const userData = useAuthStore(state => state.userData);
    const [messageContent, setMessageContent] = useState<string>('');
    const scrollViewRef = useRef<ScrollView>(null);

    const scrollToBottom = () => {
        setTimeout(() => {
            if (scrollViewRef?.current) {
                scrollViewRef.current?.scrollToEnd({animated: true});
            }
        }, 300);
    };

    const submitMessage = async () => {
        if (!messageContent) {
            return;
        }

        await sendChatMessage(chatTypeAndUuid, messageContent);
        await refetch();

        setMessageContent('');
        scrollToBottom();
    };

    const {data, error, isFetching, refetch, isFetched} = useQuery<unknown, unknown, Chat>({
        queryKey: ['chat', chatTypeAndUuid],
        queryFn: async () => {
            try {
                const res: AxiosResponse = await axios.get(`chat/${chatTypeAndUuid}`)

                return res?.data ?? null;
            } catch (e) {
                console.log('chat exception', e)
            }
        }
    });

    useEffect(() => {
        if (isFetched) {
            scrollToBottom();
        }
    }, [isFetched]);

    if (error) {
        return (
            <ScrollableLayoutFullheight>
                <Text>{i18n.chatScreen.error}: {JSON.stringify(error)}</Text>
            </ScrollableLayoutFullheight>
        )
    }

    if (!data) {
        return (
            <View
                className={"flex-1 items-center justify-center"}
            >
                <Loader/>
            </View>
        );
    }

    let chatTitle: string = data?.title
        ? (
            `${data?.recipient?.name} - ${data?.title}`
        ) : (
        data?.recipient?.name
    );
    if (chatTitle.length > 40) {
        chatTitle = chatTitle.substring(0, 40) + '...';
    }

    return (
        <Layout
            backButton={(
                <TouchableOpacity
                    onPress={() => {
                        router.back();
                    }}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
            )}
            title={chatTitle}
            titlePosition={'left'}
            subTitle={
                data?.updated_at
                    ? `Ostatnia wiadomość: ${getFormattedDate(data?.updated_at)}`
                    : null
            }
            bgColor={'bg-gray-50'}
            height={Dimensions.get('window').height - (Platform.OS === 'ios' ? (insets.top + insets.bottom) : 0)}
        >
            <KeyboardAvoidingView
                behavior={'height'}
                keyboardVerticalOffset={70}
                style={{flex: 1}}
            >
                <ScrollView
                    horizontal={false}
                    showsVerticalScrollIndicator={true}
                    className={'flex flex-col bg-gray-100'}
                    ref={scrollViewRef}
                    refreshControl={<RefreshControl refreshing={isFetching} onRefresh={refetch}/>}
                >
                    {(data?.messages && data?.messages?.length > 0) ? (
                        <View
                            className={'flex flex-col py-4 px-2'}
                        >
                            {data?.messages?.map((message: ChatMessage, index: number) => (
                                <Message
                                    key={index}
                                    message={message}
                                    friendUuid={data?.recipient?.uuid}
                                    friendAvatar={data?.recipient?.avatar?.url ?? `${apiConfig.publicUrl}/assets/user.png`}
                                    friendName={data?.recipient?.name}
                                    currentUserUuid={userData?.uuid}
                                    currentUserAvatar={userData?.avatar?.url}
                                />
                            ))}
                        </View>
                    ) : (
                        <View
                            className={"flex flex-col py-20 px-2 items-center justify-center"}
                        >
                            <Ionicons name={'chatbox-ellipses-outline'} size={48} color={'gray'}/>
                            <Text
                                className={'mt-2 text-gray-500'}
                            >{i18n.chatScreen.noMessages}</Text>
                        </View>
                    )}
                </ScrollView>
                <View
                    className={'flex flex-row items-center justify-between px-3 py-2 bg-white border-t border-t-gray-200'}
                >
                    <View
                        className={"flex flex-row border border-gray-200 w-full rounded-xl"}
                    >
                        <TextInput
                            placeholder={i18n.chatScreen.insertMessage}
                            className={'px-2 py-2'}
                            onChangeText={text => {
                                setMessageContent(text);
                            }}
                            value={messageContent}
                            onSubmitEditing={submitMessage}
                            blurOnSubmit={false}
                            multiline={true}
                            onFocus={scrollToBottom}
                            style={{
                                width: '90%'
                            }}
                        />
                        <TouchableOpacity
                            className={'flex flex-row items-center justify-center px-2 py-1'}
                            onPress={submitMessage}
                        >
                            <Ionicons name="send" size={22} color="darkgrey"/>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>
        </Layout>
    );
};
