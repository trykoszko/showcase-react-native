import {Alert, Pressable, RefreshControl, Share, Text, TextInput, TouchableOpacity, View} from 'react-native';
import ScrollableLayout from '../../components/layout/ScrollableLayout';
import Ionicons from '@expo/vector-icons/Ionicons';
import React, {useEffect, useState} from 'react';
import {User} from '../../types/User.type';
import {useRouter} from 'expo-router';
import {Image} from 'expo-image';
import {getFormattedDate} from '../../helpers/date-helpers';
import i18n from '../../i18n';
import FontAwesome5 from '@expo/vector-icons/FontAwesome5';
import {useAxios} from "../../hooks/useAxios";
import {useQuery} from "react-query";
import {AxiosResponse} from "axios";
import {apiConfig} from "../../config/variables";
import {useApi} from "../../hooks/useApi";
import {useAuthStore} from "../../store/AuthStore";

export const FriendsScreen = () => {
    const [searchVisible, setSearchVisible] = useState<boolean>(false);
    const axios = useAxios();
    const userData = useAuthStore(state => state.userData);
    const {deleteFriend} = useApi();
    const router = useRouter();

    const {status, data, refetch, isFetching} = useQuery<unknown, unknown, User[]>(
        ['friends', userData?.id ?? ''],
        async () => {
            try {
                const res: AxiosResponse = await axios.get(
                    `/user/friend`
                );

                return res?.data ?? [];
            } catch (e) {
                console.log('friends exception', e)
            }
        }
    )

    useEffect(() => {
        refetch();
    }, [userData?.friends]);

    return (
        <ScrollableLayout
            backTo="home"
            title={'Friends'}
            rightButton={(
                <TouchableOpacity
                    onPress={() => setSearchVisible(!searchVisible)}
                >
                    <Ionicons
                        name={`${searchVisible ? 'close' : 'add'}`}
                        size={24}
                        color="black"
                    />
                </TouchableOpacity>
            )}
            refreshControl={<RefreshControl refreshing={isFetching} onRefresh={refetch}/>}
        >
            <View
                className="flex flex-col px-3"
            >
                {searchVisible ? (
                    <View
                        className="flex flex-col gap-5 mb-4"
                    >
                        <TouchableOpacity
                            className="flex flex-row items-center px-4 py-5 border border-gray-200 rounded-lg"
                            onPress={() => {
                                Share.share({
                                        title: i18n.friendsScreen.inviteTitle,
                                        message: i18n.friendsScreen.inviteMessage
                                    },
                                    {
                                        dialogTitle: i18n.friendsScreen.inviteTitle
                                    });
                            }}
                        >
                            <FontAwesome5 name="user-friends" size={18} color="grey"/>
                            <Text
                                className={'text-base ml-4 text-gray-600'}
                            >{i18n.friendsScreen.inviteFriendsOutsideApp}</Text>
                        </TouchableOpacity>
                        <View
                            className="flex flex-row items-center justify-between px-4 py-3 border border-gray-200 rounded-full"
                        >
                            <TextInput
                                placeholder={i18n.friendsScreen.searchFriendInApp}
                                className={'text-gray-600 text-base'}
                            />
                            <Ionicons name="search" size={18} color="grey"/>
                        </View>
                    </View>
                ) : (
                    <View className="flex-col pb-8">
                        {data?.map((friend: User, index: number) => {
                            const isCompany: boolean = !!friend?.company_name;

                            return (
                                <Pressable
                                    key={index}
                                    className="flex flex-row items-center py-2 border-b border-b-gray-100"
                                    onPress={() => Alert.alert(
                                        friend?.name,
                                        undefined,
                                        [
                                            {
                                                text: i18n.friendsScreen.seeFriend,
                                                onPress: async () => {
                                                    router.push(`/user/${friend.uuid}`);
                                                },
                                                style: 'default',
                                            },
                                            {
                                                text: i18n.friendsScreen.removeFriend,
                                                onPress: async () => {
                                                    await deleteFriend(friend.id);

                                                    Alert.alert(i18n.friendsScreen.friendRemoved);
                                                },
                                                style: 'destructive'
                                            },
                                            {
                                                text: i18n.friendsScreen.cancel,
                                                style: 'cancel'
                                            }
                                        ],
                                    )}
                                >
                                    <Pressable
                                        onPress={() => {
                                            router.push(`/user/${friend.uuid}`);
                                        }}
                                    >
                                        <Image
                                            source={{uri: friend?.avatar?.url ?? `${apiConfig.publicUrl}/assets/user.png`}}
                                            style={{
                                                width: 64,
                                                height: 64,
                                                borderRadius: 64
                                            }}
                                            className={'mr-3'}
                                        />
                                    </Pressable>
                                    <View
                                        className="flex row justify-between items-start"
                                    >
                                        <View
                                            className="flex flex-col items-start gap-y-1"
                                        >
                                            <View
                                                className={"flex flex-row items-center gap-2"}
                                            >
                                                <Text>{friend?.name}</Text>
                                                {isCompany && (
                                                    <FontAwesome5 name={'building'} size={18} color={'grey'}/>
                                                )}
                                            </View>
                                            <Text
                                                className={'text-gray-400'}
                                            >{`${i18n.friendsScreen.addedOn} ${getFormattedDate(new Date())}`}</Text>
                                        </View>
                                    </View>
                                    <View
                                        className={'ml-auto'}
                                    >
                                        <TouchableOpacity
                                            onPress={() => {
                                                router.push(`/chat/user:${friend.uuid}`);
                                            }}
                                            className={'p-2'}
                                        >
                                            <Ionicons name={'mail-outline'} size={24}
                                                      color={'black'}/>
                                        </TouchableOpacity>
                                    </View>
                                </Pressable>
                            );
                        })
                        }
                    </View>
                )}
            </View>
        </ScrollableLayout>
    );
};
