import {RefreshControl, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import ScrollableLayout from '../../components/layout/ScrollableLayout';
import i18n from '../../i18n';
import Ionicons from '@expo/vector-icons/Ionicons';
import React, {useState} from 'react';
import {ModalComponent} from '../../components/modal/Modal';
import {EventFilter, useEventFilters} from '../../constants/eventFilters';
import {Event} from '../../types/Event.type';
import {router} from 'expo-router';
import {EventListItem} from '../../components/event-list-item/EventListItem';
import Loader from '../../components/loader/Loader';
import {useAxios} from "../../hooks/useAxios";
import {useQuery} from "react-query";
import {AxiosInstance, AxiosResponse} from "axios";

export const EventsScreen = () =>
{
    const filters: EventFilter[]                         = useEventFilters();
    const [isFilterOpen, setIsFilterOpen] = useState<boolean>(false);
    const [activeFilter, setActiveFilter] = useState<string>(filters[3].key);
    const axios: AxiosInstance = useAxios();

    const {status, data, isFetching, error, refetch} = useQuery<unknown, unknown, { data: Event[], meta: any }>(
        ['events_list', activeFilter],
        async () => {
            try {
                const res: AxiosResponse = await axios.get(`event?limit=100&filter=${activeFilter}`)

                return res?.data ?? [];
            } catch (e) {
                console.log('events_list exception', e)
            }
        }
    )

    return (
        <ScrollableLayout
            backTo="home"
            title={i18n.eventsScreen.title}
            rightButton={(
                <TouchableOpacity
                    onPress={() => setIsFilterOpen(!isFilterOpen)}
                >
                    <Ionicons
                        name="filter"
                        size={24}
                        color="black"
                    />
                </TouchableOpacity>
            )}
            refreshControl={<RefreshControl refreshing={isFetching} onRefresh={refetch}/>}
        >
            <View>
                <View
                    className={'px-3'}
                >
                    <TouchableOpacity
                        onPress={() =>
                        {
                            router.push('create-event');
                        }}
                        className="w-full bg-primary flex flex-row items-center justify-center p-2 rounded-md mb-4"
                    >
                        <Text
                            className="color-white font-bold text-base"
                        >{i18n.eventsScreen.create}</Text>
                    </TouchableOpacity>
                </View>

                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    scrollEventThrottle={16}
                    className="mb-4"
                >
                    <View
                        className={'flex flex-row items-center ml-3'}
                    >
                        <Ionicons name={'funnel'} size={20} color={'grey'}/>
                        {filters.map((filter, index) => (
                            <TouchableOpacity
                                key={index}
                                onPress={() => setActiveFilter(filter.key)}
                                className={`${activeFilter === filter.key ? 'bg-primary' : 'bg-gray-200'} rounded-full px-4 py-2 ${index === filters.length - 1 ? 'mr-3' : 'mr-2'} ${index === 0 ? 'ml-3' : ''}`}
                            >
                                <Text
                                    className={`${activeFilter === filter.key ? 'text-white' : 'text-gray-700'} text-xs`}
                                >{filter.label}</Text>
                            </TouchableOpacity>
                        ))}
                    </View>
                </ScrollView>

                {isFetching ? (
                    <View
                        className={'flex flex-row justify-center items-center m-4 px-4 py-8'}
                    >
                        <Loader/>
                    </View>
                ) : (
                    (status === 'error' ? (
                        <Text>{i18n.eventsScreen.error}</Text>
                    ) : (
                        (data?.data?.length ? (
                            <View
                                className={'px-3 mb-6'}
                            >
                                {data?.data?.map((event: Event, index: number) => (
                                    <EventListItem event={event} key={index}/>
                                ))}
                            </View>
                        ) : (
                            <View
                                className={"flex flex-col items-center justify-center py-10"}
                            >
                                <Ionicons name={'funnel-outline'} size={32} color={'grey'}/>
                                <Text
                                    className={"text-center text-md text-darkGrey pt-2 pb-4"}
                                >
                                    {i18n.eventsScreen.noEventsFoundMatchingFilter}
                                </Text>
                            </View>
                        ))
                    ))
                 )}
            </View>

            <ModalComponent
                isVisible={isFilterOpen}
                setIsVisible={setIsFilterOpen}
                title={i18n.eventsScreen.filtersTitle}
                content={(
                    <View className="flex-col">
                        {filters?.map((filter, index) => (
                            <TouchableOpacity
                                onPress={() => {
                                    setActiveFilter(filter.key)
                                    setIsFilterOpen(false)
                                }}
                                key={index}
                            >
                                <View className="flex-row justify-between items-center pb-5">
                                    <Text className="font-bold">{filter.label}</Text>
                                    <Ionicons
                                        name={activeFilter === filter.key ? 'checkmark-circle' : 'checkmark-circle-outline'}
                                        size={24}
                                        color="black"
                                    />
                                </View>
                            </TouchableOpacity>
                        )) ?? <></>}
                    </View>
                )}
            />
        </ScrollableLayout>
    );
};
