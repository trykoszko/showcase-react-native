import {Alert, Pressable, Text, TouchableOpacity, View} from 'react-native';
import ScrollableLayout from '../../components/layout/ScrollableLayout';
import {User} from '../../types/User.type';
import React, {useEffect, useState} from 'react';
import {Image} from 'expo-image';
import Ionicons from '@expo/vector-icons/Ionicons';
import i18n from '../../i18n';
import {ModalComponent} from '../../components/modal/Modal';
import {apiConfig} from "../../config/variables";
import {EventElement} from "../../types/EventElement.type";
import {EventElementComponent} from "../../components/event-element-component/EventElementComponent";
import {StaticEventsCarousel} from "../../components/events-carousel/StaticEventsCarousel";
import {UserEvent} from "../../types/UserEvent.type";
import {useAuthStore} from "../../store/AuthStore";
import {useApi} from "../../hooks/useApi";
import {useRouter} from "expo-router";

type Props = {
    user: User,
    refetch: () => void
}

export const UserScreen = ({user, refetch}: Props) =>
{
    const [isAttributesModalOpen, setIsAttributesModalOpen] = useState<boolean>(false);
    const userData = useAuthStore(state => state.userData);
    const {deleteFriend, addFriend, acceptFriendRequest} = useApi();
    const router = useRouter();

    const [friendAction, setFriendAction] = useState<string>('none');

    useEffect(() => {
        const isFriend: boolean = (userData?.friends?.some((friendId: number) => friendId === user?.id) ?? false);
        const isInviteSent: boolean = (userData?.friend_invites_sent?.some((friendId: number) => friendId === user?.id) ?? false);
        const isInviteReceived: boolean = (userData?.friend_invites_received?.some((friendId: number) => friendId === user?.id) ?? false);

        if (isFriend) {
            setFriendAction('delete');
        }
        if (isInviteReceived) {
            setFriendAction('accept');
        }
        if (!isFriend && !isInviteReceived && !isInviteSent) {
            setFriendAction('add');
        }
    }, [userData?.friends]);

    const friendsActions: any = {
        'add': {
            label: i18n.userScreen.addToFriends,
            question: i18n.userScreen.addToFriendsQuestion,
            action: async () => {
                await addFriend(user?.id);
                refetch();
            }
        },
        'delete': {
            label: i18n.userScreen.deleteFriend,
            question: i18n.userScreen.deleteFriendQuestion,
            action: async () => {
                await deleteFriend(user?.id);
                refetch();
            }
        },
        'accept': {
            label: i18n.userScreen.acceptFriendRequest,
            question: i18n.userScreen.acceptFriendRequestQuestion,
            action: async () => {
                await acceptFriendRequest(user?.id);
                refetch();
            }
        },
        'none': {
            label: i18n.userScreen.requestSent,
            question: '',
            action: () => {
            }
        }
    };

    return (
        <>
            <ScrollableLayout
                backTo={'/home'}
                pb={120}
                rightButton={(
                    <Pressable
                        onPress={() =>
                        {
                            if (friendAction !== 'none') {
                                Alert.alert(
                                    friendsActions[friendAction].question,
                                    undefined,
                                    [
                                        {
                                            text: i18n.userScreen.yes,
                                            onPress: friendsActions[friendAction].action
                                        },
                                        {
                                            text: i18n.userScreen.cancel,
                                            onPress: () => {
                                            },
                                            style: 'cancel'
                                        }
                                    ]
                                );
                            }
                        }}
                    >
                        <Text
                            className={'text-primary font-bold'}
                        >
                            {friendsActions[friendAction].label}
                        </Text>
                    </Pressable>
                )}
            >
                <View
                    className={'w-full px-3 mb-4 flex-col items-center justify-center'}
                >
                    <Image
                        source={{uri: user?.avatar?.url ?? `${apiConfig.publicUrl}/assets/user.png`}}
                        style={{
                            width:  100,
                            height: 100,
                        }}
                        className={'rounded-full mb-5'}
                    />
                    <View
                        className={'flex flex-row items-center justify-center mb-6'}
                    >
                        <Text
                            className={'text-2xl font-bold mr-1.5'}
                        >
                            {user?.name}
                        </Text>
                        {user?.company_name && (
                            <Ionicons name={'checkmark-circle'} size={16} color={'green'}/>
                        )}
                    </View>
                    <View
                        className={'flex flex-row items-center justify-center w-full mb-6'}
                    >
                        <View
                            className={'flex flex-col items-center justify-center'}
                            style={{
                                width: 150
                            }}
                        >
                            <Text
                                className={'text-xl font-bold'}
                            >{user?.friends_count ?? 0}</Text>
                            <Text
                                className={'text-darkGrey'}
                            >{i18n.userScreen.friends}</Text>
                        </View>
                        <View
                            className={'w-0.5 h-full bg-gray-200'}
                        />
                        <View
                            className={'flex flex-col items-center justify-center'}
                            style={{
                                width: 150
                            }}
                        >
                            <Text
                                className={'text-xl font-bold'}
                            >{user?.events?.length ?? 0}</Text>
                            <Text
                                className={'text-darkGrey'}
                            >{i18n.userScreen.events}</Text>
                        </View>
                    </View>
                    <TouchableOpacity
                        className={'border border-gray-200 flex flex-row items-center justify-between p-4 rounded-xl mb-3 w-full'}
                        onPress={() => setIsAttributesModalOpen(true)}
                    >
                        <Ionicons name={'bulb'} size={20} color={'grey'}/>
                        <Text
                            className={'ml-3 mr-auto'}
                        >
                            {i18n.userScreen.userAttributes}
                        </Text>
                        <Ionicons name={'chevron-forward'} size={20} color={'grey'}/>
                    </TouchableOpacity>
                    <TouchableOpacity
                        className={'border border-gray-200 flex flex-row items-center justify-between p-4 rounded-xl mb-3 w-full'}
                        onPress={() =>
                        {
                            router.push(`/chat/user:${user.uuid}`);
                        }}
                    >
                        <Ionicons name={'mail-outline'} size={20} color={'grey'}/>
                        <Text
                            className={'ml-3 mr-auto'}
                        >
                            {i18n.userScreen.sendMessage}
                        </Text>
                        <Ionicons name={'chevron-forward'} size={20} color={'grey'}/>
                    </TouchableOpacity>
                </View>
                {(user?.events?.length) ? (
                    <StaticEventsCarousel events={user?.events?.map((userEvent: UserEvent) => userEvent.event) ?? []}/>
                ) : <></>}
            </ScrollableLayout>

            <ModalComponent
                isVisible={isAttributesModalOpen}
                setIsVisible={setIsAttributesModalOpen}
                title={i18n.userScreen.attributes}
                content={(
                    <View className="flex-col">
                        {user?.event_elements?.length &&
                            user?.event_elements?.map((attribute: EventElement, index: number) => (
                                <View key={index}>
                                    <EventElementComponent element={attribute}/>
                                </View>
                            ))
                        }
                    </View>
                )}
            />
        </>
    );
};
