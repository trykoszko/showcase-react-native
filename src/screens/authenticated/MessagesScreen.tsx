import {Dimensions, Platform, RefreshControl, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {Chat} from '../../types/Chat.type';
import {router} from 'expo-router';
import {useQuery} from "react-query";
import {AxiosResponse} from "axios";
import ScrollableLayoutFullheight from "../../components/layout/ScrollableLayoutFullheight";
import Loader from "../../components/loader/Loader";
import {useAxios} from "../../hooks/useAxios";
import {Image} from "expo-image";
import {getFormattedDate} from "../../helpers/date-helpers";
import Layout from '../../components/layout/Layout';
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {sizes} from "../../constants/sizes";
import {ChatTypeEnum} from "../../enums/ChatType.enum";
import {apiConfig} from "../../config/variables";
import i18n from "../../i18n";

export const MessagesScreen = () => {
    const axios = useAxios();
    const safeAreaInsets = useSafeAreaInsets();

    const {data, error, isFetching, refetch} = useQuery<unknown, unknown, Chat[]>({
        queryKey: ['chats'],
        queryFn: async () => {
            try {
                const res: AxiosResponse = await axios.get(`chat`)

                return res?.data ?? null;
            } catch (e) {
                console.log('chats exception', e)
            }
        }
    });

    if (error) {
        return (
            <ScrollableLayoutFullheight>
                <Text>{i18n.messagesScreen.error}: {JSON.stringify(error)}</Text>
            </ScrollableLayoutFullheight>
        )
    }

    if (!data) {
        return (
            <View
                className={"flex-1 items-center justify-center"}
            >
                <Loader/>
            </View>
        );
    }

    return (
        <Layout
            height={Dimensions.get('window').height - 58 - (Platform.OS === 'ios' ? sizes.bottomBarHeight : (sizes.bottomBarHeight + safeAreaInsets.bottom))}
        >
            <View>
                <Text
                    className="text-2xl font-bold text-darkGrey px-3 pt-1 pb-3"
                >
                    {i18n.messagesScreen.chats}
                </Text>
            </View>
            <ScrollView
                horizontal={false}
                showsVerticalScrollIndicator={true}
                className={'flex flex-col'}
                refreshControl={<RefreshControl refreshing={isFetching} onRefresh={refetch}/>}
                style={{
                    flex: 1,
                }}
            >
                {(data?.length > 0) ? (
                    <View className="flex flex-col mb-2 px-3">
                        {data?.map((chat: Chat, key: number) => {
                            return (
                                <TouchableOpacity
                                    key={key}
                                    className="flex flex-row justify-start items-center border-b border-b-gray-200 py-3"
                                    onPress={() => {
                                        router.push(`/chat/${chat.uuid}`);
                                    }}
                                >
                                    <View>
                                        <Image
                                            source={{
                                                uri: chat?.type === ChatTypeEnum.EVENT
                                                    ? (
                                                        chat?.event?.image?.url ?? `${apiConfig.publicUrl}/assets/event-bg.jpg`
                                                    )
                                                    : (
                                                        chat?.recipient?.avatar?.url
                                                        ?? `${apiConfig.publicUrl}/assets/user.png`
                                                    )
                                            }}
                                            style={{width: 64, height: 64}}
                                            className={chat?.type !== 'event' ? 'rounded-full' : 'rounded-md'}
                                        />
                                    </View>
                                    <View
                                        className="flex flex-col justify-center items-start ml-3"
                                    >
                                        {chat?.title ? (
                                            <View
                                                className="flex flex-col justify-start items-start"
                                            >
                                                <Text
                                                    // className={`text-base ${!chat?.isRead ? 'font-bold' : ''} ${chat?.type === 'event' ? 'text-primary' : 'text-darkGrey'}`}
                                                    className={`text-base font-bold text-darkGrey`}
                                                >
                                                    {chat.title}
                                                </Text>
                                                <Text
                                                    className="text-sm mt-1"
                                                >
                                                    {chat?.recipient?.name}
                                                </Text>
                                                {/*{!chat?.isRead && (*/}
                                                {/*    <View*/}
                                                {/*        className={'w-2 h-2 rounded-full bg-green-500 ml-2'}*/}
                                                {/*    />*/}
                                                {/*)}*/}
                                            </View>
                                        ) : (
                                            <Text
                                                // className={`text-base ${!chat?.isRead ? 'font-bold' : ''} ${chat?.type === 'event' ? 'text-primary' : 'text-darkGrey'}`}
                                                className={`text-base text-darkGrey`}
                                            >
                                                <Text
                                                    className="text-sm mt-1"
                                                >
                                                    {chat.recipient?.name}
                                                </Text>
                                            </Text>
                                        )}
                                        {chat?.updated_at && (
                                            <Text
                                                className="text-xs text-gray-500 mt-2"
                                            >
                                                {getFormattedDate(chat.updated_at)}
                                            </Text>
                                        )}
                                    </View>
                                </TouchableOpacity>
                            )
                        })}
                    </View>
                ) : (
                    <View className="flex flex-col mb-5 px-3">
                        <Text
                            className="font-bold text-darkGrey mt-4 mb-3"
                        >{i18n.messagesScreen.noMessagesTitle}</Text>
                        <Text
                            className={'font-light text-darkGrey'}
                        >
                            {i18n.messagesScreen.noMessagesText}
                        </Text>
                    </View>
                )}
            </ScrollView>
        </Layout>
    );
};
