import {Alert, Dimensions, Text, TouchableOpacity, View} from 'react-native';
import ScrollableLayout from '../../components/layout/ScrollableLayout';
import React, {useState} from 'react';
import {Image} from 'expo-image';
import Ionicons from '@expo/vector-icons/Ionicons';
import {EventsCarousel} from '../../components/events-carousel/EventsCarousel';
import i18n from '../../i18n';
import {ModalComponent} from '../../components/modal/Modal';
import {router} from 'expo-router';
import {useAuthStore} from '../../store/AuthStore';
import {apiConfig} from "../../config/variables";
import {MyUser} from "../../types/MyUser.type";
import {EventElement} from "../../types/EventElement.type";
import {EventElementComponent} from "../../components/event-element-component/EventElementComponent";
import Loader from "../../components/loader/Loader";

const LogoutButton = () => {
    const setApiToken = useAuthStore(state => state.setApiToken);

    return (
        <View
            className={'w-full px-3 mb-8 flex-col items-center justify-center'}
        >
            <TouchableOpacity
                className={'border border-gray-200 flex flex-row items-center justify-between p-4 rounded-xl mb-3 w-full'}
                onPress={() => {
                    setApiToken('');
                }}
            >
                <Ionicons name={'log-out'} size={20} color={'grey'}/>
                <Text
                    className={'ml-3 mr-auto'}
                >
                    {i18n.profileScreen.logout}
                </Text>
                <Ionicons name={'chevron-forward'} size={20} color={'grey'}/>
            </TouchableOpacity>
        </View>
    )
}

export const ProfileScreen = () => {
    const [isAttributesModalOpen, setIsAttributesModalOpen] = useState<boolean>(false);
    const userData: MyUser | undefined = useAuthStore(state => state.userData);
    const setApiToken = useAuthStore(state => state.setApiToken);

    if (!userData || (!userData.first_name && !userData.last_name && !userData.company_name)) {
        return (
            <View
                className={"flex-1 items-center justify-end"}
                style={{
                    maxHeight: Dimensions.get('window').height - 60
                }}
            >
                <View
                    className={"flex-1 items-center justify-center"}
                >
                    <Loader/>
                </View>
                <LogoutButton />
            </View>
        );
    }

    return (
        <>
            <ScrollableLayout
                backTo={'/home'}
                rightButton={(
                    <TouchableOpacity
                        onPress={() => {
                            Alert.alert('clicked');
                        }}
                    >
                        <Text
                            className={'text-primary font-bold'}
                        >{i18n.profileScreen.editProfile}</Text>
                    </TouchableOpacity>
                )}
            >
                <View
                    className={'w-full px-3 pt-4 mb-4 flex-col items-center justify-center'}
                >
                    <Image
                        source={{uri: userData?.avatar?.url ?? `${apiConfig.publicUrl}/assets/user.png`}}
                        style={{
                            width: 100,
                            height: 100,
                        }}
                        className={'rounded-full mb-5'}
                    />
                    <View
                        className={'flex flex-row items-center justify-center mb-6'}
                    >
                        <Text
                            className={'text-2xl font-bold mr-1.5'}
                        >
                            {userData.name}
                        </Text>
                        {userData?.company_name && (
                            <Ionicons name={'checkmark-circle'} size={16} color={'green'}/>
                        )}
                    </View>
                    <View
                        className={'flex flex-row items-center justify-center w-full mb-6'}
                    >
                        <TouchableOpacity
                            className={'flex flex-col items-center justify-center'}
                            style={{
                                width: 150
                            }}
                            onPress={() => router.push('/friends')}
                        >
                            <Text
                                className={'text-xl font-bold'}
                            >{userData?.friends?.length}</Text>
                            <Text
                                className={'text-darkGrey'}
                            >{i18n.profileScreen.friends}</Text>
                        </TouchableOpacity>
                        <View
                            className={'w-0.5 h-full bg-gray-200'}
                        />
                        <View
                            className={'flex flex-col items-center justify-center'}
                            style={{
                                width: 150
                            }}
                        >
                            <Text
                                className={'text-xl font-bold'}
                            >{userData?.events?.length}</Text>
                            <Text
                                className={'text-darkGrey'}
                            >{i18n.profileScreen.events}</Text>
                        </View>
                    </View>
                    <TouchableOpacity
                        className={'border border-gray-200 flex flex-row items-center justify-between p-4 rounded-xl mb-3 w-full'}
                        onPress={() => setIsAttributesModalOpen(true)}
                    >
                        <Ionicons name={'bulb'} size={20} color={'grey'}/>
                        <Text
                            className={'ml-3 mr-auto'}
                        >
                            {i18n.profileScreen.userAttributes}
                        </Text>
                        <Ionicons name={'chevron-forward'} size={20} color={'grey'}/>
                    </TouchableOpacity>
                    <TouchableOpacity
                        className={'border border-gray-200 flex flex-row items-center justify-between p-4 rounded-xl mb-3 w-full'}
                        onPress={() => {
                            Alert.alert('clicked');
                        }}
                    >
                        <Ionicons name={'cash-outline'} size={20} color={'purple'}/>
                        <Text
                            className={'ml-3 mr-auto'}
                        >
                            {i18n.profileScreen.startEarning}
                        </Text>
                        <Ionicons name={'chevron-forward'} size={20} color={'grey'}/>
                    </TouchableOpacity>
                </View>

                <EventsCarousel hasFilters={false} title={'Twoje wydarzenia'}/>

                <LogoutButton />
            </ScrollableLayout>

            <ModalComponent
                isVisible={isAttributesModalOpen}
                setIsVisible={setIsAttributesModalOpen}
                title={i18n.profileScreen.attributes}
                content={(
                    <View className="flex-col">
                        {userData?.event_elements?.length &&
                            userData?.event_elements?.map((attribute: EventElement, index: number) => (
                                <View key={index}>
                                    <EventElementComponent element={attribute}/>
                                </View>
                            ))
                        }
                    </View>
                )}
            />
        </>
    );
};
