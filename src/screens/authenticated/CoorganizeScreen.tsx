import {RefreshControl, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import ScrollableLayout from '../../components/layout/ScrollableLayout';
import i18n from '../../i18n';
import Ionicons from '@expo/vector-icons/Ionicons';
import React, {useState} from 'react';
import {ModalComponent} from '../../components/modal/Modal';
import Loader from '../../components/loader/Loader';
import {CoorganizeListItem} from '../../components/coorganise-list/CoorganiseListItem';
import {useCoorganizeFilters} from '../../constants/coorganizeFilters';
import {AxiosInstance, AxiosResponse} from "axios";
import {useAxios} from "../../hooks/useAxios";
import {useQuery} from "react-query";
import {EventEventElement} from "../../types/EventEventElement.type";
import {useAuthStore} from "../../store/AuthStore";

export const CoorganizeScreen = () => {
    const filters = useCoorganizeFilters();
    const userData = useAuthStore(state => state.userData);
    const [isFilterOpen, setIsFilterOpen] = useState<boolean>(false);
    const [activeFilter, setActiveFilter] = useState<string>(filters[0].key);
    const axios: AxiosInstance = useAxios();
    const [isError, setIsError] = useState<boolean>(false);

    const {status, data, isFetching, error, refetch} = useQuery<unknown, unknown, { data: EventEventElement[], meta: any }>(
        ['coorganize_items_list', activeFilter],
        async () => {
            try {
                setIsError(false);
                const res: AxiosResponse = await axios.get(
                    `event-coorganize-item?limit=200&filter=${activeFilter}`
                );

                return res?.data ?? [];
            } catch (e) {
                setIsError(true);
                console.log('events_carousel exception', e)
            }
        }
    );

    return (
        <ScrollableLayout
            backTo="home"
            title={i18n.coorganizeScreen.title}
            rightButton={(
                <TouchableOpacity
                    onPress={() => setIsFilterOpen(!isFilterOpen)}
                >
                    <Ionicons
                        name="filter"
                        size={24}
                        color="black"
                    />
                </TouchableOpacity>
            )}
            refreshControl={(
                <RefreshControl
                    refreshing={isFetching}
                    onRefresh={refetch}
                />
            )}
        >
            <View
                className={'mb-6'}
            >
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    className="mb-4"
                >
                    <View
                        className={'flex flex-row items-center ml-3 pr-1'}
                    >
                        <Ionicons name={'funnel'} size={20} color={'grey'}/>
                        {userData?.event_elements?.length &&
                            userData?.event_elements?.map((attribute, index) => (
                                <TouchableOpacity
                                    key={index}
                                    onPress={() => {
                                        const filter: string =`attribute-${attribute.id}`;
                                        if (activeFilter === filter) {
                                            setActiveFilter('');
                                        } else {
                                            setActiveFilter(filter);
                                        }
                                    }}
                                    className={`${activeFilter === `attribute-${attribute.id}` ? 'bg-primary' : 'bg-gray-200'} rounded-full px-4 py-2 mr-2 ${index === 0 ? 'ml-3' : ''}`}
                                >
                                    <Text
                                        className={`${activeFilter === `attribute-${attribute.id}` ? 'text-white' : 'text-gray-700'} text-xs`}
                                    >{attribute.icon} {attribute.name}</Text>
                                </TouchableOpacity>
                            ))
                        }
                    </View>
                </ScrollView>

                {status === 'loading' ? (
                    <View
                        className={'flex flex-row justify-center items-center m-4 px-4 py-8'}
                    >
                        <Loader/>
                    </View>
                ) : (
                    (isError ? (
                        <View
                            className={"flex flex-col items-center justify-center"}
                            style={{
                                height: 271
                            }}
                        >
                            <Ionicons name={'alert-circle-outline'} size={32} color={'grey'}/>
                            <Text
                                className={"text-center text-md text-darkGrey pt-2 pb-4"}
                            >
                                {i18n.coorganizeScreen.elementsFetchError}
                            </Text>
                        </View>
                    ) : (
                        (data?.data?.length ? (
                            <View
                                className={'px-3'}
                            >
                                {data?.data?.map((item: EventEventElement, index) => (
                                    <CoorganizeListItem item={item} key={index}/>
                                ))}
                            </View>
                        ) : (
                            <View
                                className={"flex flex-col items-center justify-center"}
                                style={{
                                    height: 271
                                }}
                            >
                                <Ionicons name={'funnel-outline'} size={32} color={'grey'}/>
                                <Text
                                    className={"text-center text-md text-darkGrey pt-2 pb-4"}
                                >
                                    {i18n.coorganizeScreen.noItemsFoundMatchingFilter}
                                </Text>
                            </View>
                        ))
                    ))
                )}
            </View>

            <ModalComponent
                isVisible={isFilterOpen}
                setIsVisible={setIsFilterOpen}
                title={i18n.coorganizeScreen.filterTitle}
                content={(
                    <View className="flex-col">
                        {filters?.map((filter, index) => (
                            <TouchableOpacity
                                onPress={() => {
                                    setActiveFilter(filter.key);
                                    setIsFilterOpen(false);
                                }}
                                key={index}
                            >
                                <View className="flex-row justify-between items-center pb-5">
                                    <Text className="font-bold">{filter.label}</Text>
                                    <Ionicons
                                        name={activeFilter === filter.key ? 'checkmark-circle' : 'checkmark-circle-outline'}
                                        size={24}
                                        color="black"
                                    />
                                </View>
                            </TouchableOpacity>
                        )) ?? <></>}
                    </View>
                )}
            />
        </ScrollableLayout>
    );
};
