import {ImageBackground} from 'expo-image';
import {Text, TouchableOpacity, View} from 'react-native';
import {EventDescription} from '../components/event-description/EventDescription';
import {EventLocation} from '../components/event-location/EventLocation';
import {EventHeader} from '../components/event-header/EventHeader';
import {EventDetails} from '../components/event-details/EventDetails';
import React from 'react';
import {BlurView} from 'expo-blur';
import Ionicons from '@expo/vector-icons/Ionicons';
import {router} from 'expo-router';
import i18n from '../i18n';
import {EventCoorganiseItem} from "../components/event-coorganise-item/EventCoorganiseItem";
import {EventEventElement} from "../types/EventEventElement.type";
import {apiConfig} from "../config/variables";
import {EventUser} from "../types/EventUser.type";
import * as WebBrowser from 'expo-web-browser';

export const LockedEventScreen = () =>
{
    const event: any = {
        "id": 14,
        "uuid": "3596a4d1-d45f-4339-bb76-e7861996f27d",
        "title": "Obudź Się",
        "slug": "obud-si-pfm043vbzz",
        "description": "Placeat libero placeat labore. Omnis aspernatur nobis. Nam sequi iure sapiente voluptatum explicabo possimus nihil maxime possimus. Perspiciatis labore reiciendis quasi incidunt odit.",
        "start_date": new Date("2025-02-17T07:36:47.944Z"),
        "unlimited_people": false,
        "people_limit": 42,
        "limited_duration": false,
        "duration": 11,
        "private_event": false,
        "review_needed": false,
        "paid_event": false,
        "can_join_in_progress": true,
        "city": {
            "id": 1,
            "uuid": "d223ef8a-ca96-4d7d-ad12-d01fea688d03",
            "name": "Warszawa",
            "country": {
                "id": 1,
                "uuid": "be0260f3-5182-472a-bda9-239e5fb56403",
                "name": "Poland",
                "native_name": "Polska",
                "alpha_2": "PL"
            }
        },
        "image": {
            "id": 5,
            "uuid": "66c4f867-f115-4bf5-b2d0-0ca1fb5f1a14",
            "url": "https://loremflickr.com/800/600/nature?lock=37651"
        },
        "event_template": {
            "id": 3,
            "uuid": "6dde0ee8-d080-4131-8b70-30729b77b3f9",
            "name": "Chrzciny",
            "slug": "chrzciny",
            "icon": "🎨",
            "color": "#0e6c4b"
        },
        "event_elements": [
            {
                "id": 87,
                "uuid": "f90acbb6-bc4c-4da6-a70b-c34c4dd0e048",
                "client_has": false,
                "client_description": "Quam nisi alias. Amet nisi voluptatibus. Deserunt quis quaerat nam similique doloribus deleniti nihil ut molestiae. Accusantium rem quam necessitatibus labore possimus expedita nemo. Possimus hic saepe eligendi quam facilis quam voluptas.",
                "offer_price": 526,
                "event_element": {
                    "id": 3,
                    "uuid": "b39dce6a-b15e-4248-a4bc-0ad6d6bb85c4",
                    "name": "Sprzątanie",
                    "slug": "sprztanie",
                    "icon": "🛒",
                    "color": "#506916"
                },
                "offers": []
            },
            {
                "id": 86,
                "uuid": "8e7035ac-a9a5-464d-95bf-c4b0b4e53e24",
                "client_has": false,
                "client_description": "Saepe alias fugit sint ab placeat aliquid tenetur atque fugiat. Nemo autem incidunt delectus explicabo ut ducimus et dolorum. Soluta ipsa rerum dolore architecto excepturi odit aut nam dignissimos. Labore magni possimus. Inventore omnis ad amet quisquam consequatur beatae fugit magnam. Quibusdam magni tempore repellat deleniti ut commodi.",
                "offer_price": 284,
                "event_element": {
                    "id": 5,
                    "uuid": "d38abcf0-2ca9-4cb7-9da4-f3a2386279de",
                    "name": "Ochrona",
                    "slug": "ochrona",
                    "icon": "🎢",
                    "color": "#5f3241"
                },
                "offers": []
            },
            {
                "id": 85,
                "uuid": "f21d58bf-ef62-4846-a82f-fc563f7108bc",
                "client_has": false,
                "client_description": "Eaque ipsam facere. Repellat vitae quibusdam perferendis temporibus ut hic officiis accusantium architecto. Nihil eligendi aut dicta aliquid. Rerum dicta quia cupiditate maxime. Assumenda mollitia recusandae maiores corporis.",
                "offer_price": 980,
                "event_element": {
                    "id": 12,
                    "uuid": "c0fbcdbc-b692-4cbb-b091-5af44ba2287b",
                    "name": "Inne",
                    "slug": "inne",
                    "icon": "🎉",
                    "color": "#065c7f"
                },
                "offers": []
            },
            {
                "id": 84,
                "uuid": "3c31e78b-4ded-48f7-8ff1-55ef316511eb",
                "client_has": false,
                "client_description": "Temporibus necessitatibus dignissimos ex. Voluptate ratione error officiis molestias excepturi voluptate. Id illum omnis vero. Nulla atque sapiente ut delectus quae rem sunt minima veritatis. Itaque odit fugiat dignissimos.",
                "offer_price": 578,
                "event_element": {
                    "id": 6,
                    "uuid": "3536d0b2-3b9b-44d2-b587-75973805b6e8",
                    "name": "Zaopatrzenie",
                    "slug": "zaopatrzenie",
                    "icon": "🍽",
                    "color": "#5b4845"
                },
                "offers": []
            },
            {
                "id": 83,
                "uuid": "1686c7ac-13a4-43e6-b8d9-e4eca25515e0",
                "client_has": false,
                "client_description": "Dolorum quod repellat sint laudantium aut culpa magni. Quis est ratione odio maiores. Exercitationem ea magni. Commodi officiis tempora vel.",
                "offer_price": 92,
                "event_element": {
                    "id": 7,
                    "uuid": "6c9bb6ac-0fc1-4d49-9efd-d689ad11a845",
                    "name": "Lokal",
                    "slug": "lokal",
                    "icon": "🛡",
                    "color": "#512903"
                },
                "offers": []
            },
            {
                "id": 82,
                "uuid": "e4c633b3-6389-4b6a-ad31-b860dc011205",
                "client_has": false,
                "client_description": "Quam nisi ipsum ipsa voluptates quos. Cum voluptas dicta officiis quo corrupti nulla tempora. Autem odit nam quaerat quod optio vero ipsa voluptatibus. Officia consequuntur ab a ratione ex maxime.",
                "offer_price": 626,
                "event_element": {
                    "id": 8,
                    "uuid": "3cd3c143-4bc6-4580-bc3a-c063abd46b9c",
                    "name": "Dekoracje",
                    "slug": "dekoracje",
                    "icon": "📸",
                    "color": "#4b7a06"
                },
                "offers": []
            },
            {
                "id": 81,
                "uuid": "5fbd8db7-4173-414a-9651-899322c5f38b",
                "client_has": false,
                "client_description": "Suscipit distinctio expedita. Molestias animi dolor quasi dolor. Animi dolorem commodi.",
                "offer_price": 441,
                "event_element": {
                    "id": 9,
                    "uuid": "5103d0f6-d34a-4fb0-9031-205556e67a4b",
                    "name": "Atrakcje",
                    "slug": "atrakcje",
                    "icon": "🚗",
                    "color": "#326117"
                },
                "offers": []
            },
            {
                "id": 80,
                "uuid": "3d67ef5a-4341-4e9e-82f1-936f7c85cc20",
                "client_has": true,
                "client_description": "Ducimus eligendi aperiam labore officia. Aliquam architecto consequatur accusantium tempora consequatur. Aspernatur nemo expedita qui placeat similique.",
                "offer_price": null,
                "event_element": {
                    "id": 2,
                    "uuid": "e5dde276-a73f-4005-b780-5bd5f174301d",
                    "name": "Gastronomia",
                    "slug": "gastronomia",
                    "icon": "🏠",
                    "color": "#50527e"
                },
                "offers": []
            }
        ],
        "users": []
    };

    const eventOwner = event.users.find((eventUser: EventUser) => eventUser.role === 'owner');

    return (
        <View
            style={{
                backgroundColor: 'white',
                paddingBottom:   0,
            }}
        >
            <ImageBackground
                source={{uri: event?.image?.url ?? `${apiConfig.publicUrl}/assets/event-bg.jpg`}}
                style={{width: '100%', height: 220}}
                imageStyle={{borderTopLeftRadius: 4, borderTopRightRadius: 4}}
                className="flex flex-col justify-between items-between"
            />

            <View
                style={{
                    marginTop: -150
                }}
                className={'h-full'}
            >
                <EventHeader
                    dateTime={event.start_date}
                    duration={event.duration}
                    eventUuid={event.uuid}
                />

                {eventOwner && (
                    <EventDetails
                        dateTime={event.start_date}
                        duration={event.duration}
                        title={event.title}
                        creatorUuid={eventOwner.uuid}
                        creatorProfilePictureUrl={eventOwner.avatar.url}
                        creatorName={eventOwner.first_name}
                        creatorRole={eventOwner.role}
                    />
                )}

                {event.description && (
                    <EventDescription description={event.description}/>
                )}

                {event.city && (
                    <EventLocation locationName={event.city.name} distance={2}/>
                )}

                {event?.event_elements?.map((eventElement: EventEventElement, index: number) =>
                    <View key={index}>
                        <EventCoorganiseItem
                            eventElement={eventElement}
                        />
                    </View>
                )}
            </View>
            <BlurView
                intensity={25}
                className="z-20 absolute top-0 left-0 right-0 bottom-0 h-full w-full p-3 flex flex-col items-start"
            >
                <TouchableOpacity
                    className="bg-white rounded-full p-2 mt-14"
                    onPress={() => router.back()}
                >
                    <Ionicons name="arrow-back" size={20} color="black"/>
                </TouchableOpacity>

                <View className="flex flex-col w-full mt-auto h-56 pt-2">
                    <TouchableOpacity
                        className="rounded-md bg-primary"
                        onPress={() =>
                        {
                            router.push('/authentication');
                        }}
                    >
                        <Text className="text-white py-3 px-4 text-center">
                            {i18n.lockedEventScreen.logIn}
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        className="rounded-md mt-3"
                        onPress={async () =>
                        {
                            await WebBrowser.openBrowserAsync('https://app.com/about');
                        }}
                    >
                        <Text className="text-primary py-3 px-4 text-center">
                            {i18n.lockedEventScreen.about}
                        </Text>
                    </TouchableOpacity>
                </View>
            </BlurView>
        </View>
    );
};
