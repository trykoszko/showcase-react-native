import {useRouter} from "expo-router";
import {Text, TouchableOpacity, View} from "react-native";
import Layout from "../components/layout/Layout";
import i18n from "../i18n";

export default function PrivacyPolicyScreen() {
    const router = useRouter();

    return (
        <Layout title={i18n.privacyPolicyScreen.title}>
            <View className="flex flex-column h-full">
                <Text className="mb-3">
                    {i18n.privacyPolicyScreen.content}
                </Text>
                <TouchableOpacity className="bg-primary p-3 rounded mt-auto" onPress={() => {
                    router.push('/authentication')
                }}>
                    <Text className="text-center font-bold text-white">{i18n.privacyPolicyScreen.buttonText}</Text>
                </TouchableOpacity>
            </View>
        </Layout>
    )
}
