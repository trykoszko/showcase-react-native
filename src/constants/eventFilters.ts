import i18n from "../i18n";

export type EventFilter = {
    key: string;
    label: string;
}

export const useEventFilters = (isLocked?: boolean): EventFilter[] =>
    isLocked ? [
        {
            key: 'all',
            label: i18n.eventFilters.all,
        },
        {
            key: 'today',
            label: i18n.eventFilters.today,
        },
        {
            key: 'recent',
            label: i18n.eventFilters.recent,
        }
    ] : [
        {
            key: 'created',
            label: i18n.eventFilters.created,
        },
        {
            key: 'coorganized',
            label: i18n.eventFilters.coorganized,
        },
        {
            key: 'around',
            label: i18n.eventFilters.around
        },
        {
            key: 'all',
            label: i18n.eventFilters.all,
        },
        {
            key: 'today',
            label: i18n.eventFilters.today,
        },
        {
            key: 'recent',
            label: i18n.eventFilters.recent,
        },
        {
            key: 'joined',
            label: i18n.eventFilters.joined,
        },
        {
            key: 'saved',
            label: i18n.eventFilters.saved,
        }
    ];
