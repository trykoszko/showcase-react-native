import i18n from '../i18n';

export type CoorganizeFilter = {
    key: string;
    label: string;
}

export const useCoorganizeFilters = (): CoorganizeFilter[] => [
    {
        key:   'all',
        label: i18n.coorganizeFilters.all
    },
    {
        key:   'saved',
        label: i18n.coorganizeFilters.saved
    },
    {
        key:   'near-me',
        label: i18n.coorganizeFilters.nearMe,
    }
];
