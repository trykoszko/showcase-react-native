import {Text, View} from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import i18n from '../../i18n';

type Props = {
    totalPeople: number
}

export const EventChatInfo = ({totalPeople}: Props) =>
{
    return (
        <View
            className="flex flex-col mx-3 px-4 mb-3 py-5 bg-white border border-borderGrey rounded-2xl shadow-xl shadow-gray-200"
        >
            <Text className="text-xl font-bold mb-3">{i18n.eventChatInfo.title}</Text>
            <View className="flex flex-row justify-start items-center w-full">
                <View
                    className="flex flex-row justify-center items-center rounded-xl"
                    style={{
                        width:           48,
                        height:          48,
                        backgroundColor: '#efedfc'
                    }}
                >
                    <Ionicons name="people" size={22} color="#672171"/>
                </View>
                <View
                    className="flex flex-row justify-between items-center ml-3 w-full"
                >
                    <Text className="ml-3">{totalPeople} {i18n.eventChatInfo.people}</Text>
                    <Ionicons name="chatbox-outline" size={22} color="black"/>
                </View>
            </View>
        </View>
    );
};
