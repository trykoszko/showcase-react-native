import {ScrollView, Text, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Link} from 'expo-router';
import Ionicons from '@expo/vector-icons/Ionicons';
import {useTopBlurStore} from '../../store/TopBlurStore';
import {ReactElement} from "react";

type Props = {
    children: React.ReactNode,
    title?: string,
    backTo?: string,
    rightButton?: React.ReactNode,
    pb?: number,
    refreshControl?: ReactElement
}

export default function ScrollableLayoutWithBottomBar({children, title, backTo, rightButton, pb, refreshControl}: Props)
{
    const setIsTopBlurVisible = useTopBlurStore(state => state.setIsTopBlurVisible);

    return (
        <>
            {/*<GlobalLoader/>*/}

            <SafeAreaView
                style={{
                    flex:            1,
                    backgroundColor: '#fbfbfb',
                    height:          '100%',
                    paddingBottom:   pb || 0
                }}
                edges={[]}
            >
                <ScrollView
                    contentContainerStyle={{
                        flexGrow:        1,
                        backgroundColor: '#fbfbfb',
                        paddingTop:      60
                    }}
                    onScroll={e =>
                    {
                        setIsTopBlurVisible(e.nativeEvent.contentOffset.y > 4);
                    }}
                    scrollEventThrottle={16}
                    refreshControl={refreshControl ?? undefined}
                >
                    <View>
                        {(title || backTo) && (
                            <View
                                className={`flex flex-row ${backTo ? 'justify-between' : 'justify-center'} items-center border-gray-300 border-b pt-2 pb-6 px-3 mt-16`}>
                                {backTo && (
                                    <Link href={backTo}>
                                        <Ionicons name="chevron-back" size={24} color="black"/>
                                    </Link>
                                )}
                                {title && (
                                    <Text className="text-sm font-bold">{title}</Text>
                                )}
                                {backTo && (
                                    rightButton ? rightButton : <View style={{width: 24}}/>
                                )}
                            </View>
                        )}
                        <View
                            className={`pt-6 pb-4`}
                        >
                            {children}
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
}
