import Ionicons from '@expo/vector-icons/Ionicons';
import {Link} from 'expo-router';
import {Platform, StatusBar, Text, View} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';

type Props = {
    children: React.ReactNode,
    title?: string,
    titlePosition?: 'center' | 'left',
    subTitle?: string | null,
    backTo?: string,
    backButton?: React.ReactNode
    rightButton?: React.ReactNode,
    edges?: Array<'top' | 'right' | 'bottom' | 'left'>,
    bgColor?: string,
    height?: number
}

export default function Layout({
                                   children,
                                   title,
                                   titlePosition = 'center',
                                   subTitle,
                                   backTo,
                                   backButton,
                                   rightButton,
                                   edges = [],
                                   bgColor = '#fbfbfb',
                                   height = undefined
                               }: Props)
{
    const insets = useSafeAreaInsets();

    return (
        <>
            <SafeAreaView
                style={{
                    backgroundColor: 'white',
                    height:          height,
                    marginTop: Platform.OS === 'ios'
                        ? insets.top
                        : StatusBar.currentHeight
                }}
                edges={edges}
            >
                <StatusBar barStyle="dark-content" backgroundColor="white"/>
                {(title || backButton || rightButton) && (
                    <View
                        className={`flex flex-row ${(backTo || backButton) ? 'justify-between' : 'justify-center'} items-center border-gray-300 border-b py-3 px-3`}>
                        {backTo && (
                            <Link href={backTo}>
                                <Ionicons name="chevron-back" size={24} color="black"/>
                            </Link>
                        )}
                        {backButton && backButton}
                        {title && (
                            <View
                                className={` ${titlePosition === 'center' ? '' : 'mr-auto ml-4'}`}
                            >
                                <Text
                                    className={`text-sm font-bold`}>{title}</Text>
                                {subTitle && (
                                    <Text
                                        className={`text-xs text-gray-400`}>{subTitle}</Text>
                                )}
                            </View>
                        )}
                        {(backTo || backButton) && (
                            <View style={{width: 24}}/>
                        )}
                        {rightButton && rightButton}
                    </View>
                )}
                {children}
            </SafeAreaView>
        </>
    );
}
