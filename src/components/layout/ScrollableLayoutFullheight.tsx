import {ScrollView, View} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';
import {useTopBlurStore} from '../../store/TopBlurStore';
import {ReactElement} from "react";

type Props = {
    children: React.ReactNode,
    title?: string,
    backTo?: string,
    rightButton?: React.ReactNode,
    pb?: number,
    refreshControl?: ReactElement
}

export default function ScrollableLayoutFullheight({children, title, backTo, rightButton, pb, refreshControl}: Props)
{
    const setIsTopBlurVisible = useTopBlurStore(state => state.setIsTopBlurVisible);

    return (
        <>
            <SafeAreaView
                style={{
                    flex:            1,
                    backgroundColor: '#fbfbfb',
                    height:          '100%',
                    paddingBottom:   pb || 0
                }}
                edges={[]}
            >
                <ScrollView
                    contentContainerStyle={{
                        flexGrow:        1,
                        backgroundColor: '#fbfbfb'
                    }}
                    onScroll={e =>
                    {
                        setIsTopBlurVisible(e.nativeEvent.contentOffset.y > 0);
                    }}
                    scrollEventThrottle={16}
                    refreshControl={refreshControl || undefined}
                >
                    <View className={`pb-4`}>
                        {children}
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
}
