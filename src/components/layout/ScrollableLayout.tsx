import Ionicons from '@expo/vector-icons/Ionicons';
import {router} from 'expo-router';
import {Dimensions, Platform, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {SafeAreaView, useSafeAreaInsets} from 'react-native-safe-area-context';
import {useTopBlurStore} from '../../store/TopBlurStore';
import {ReactElement} from "react";
import {sizes} from "../../constants/sizes";

type Props = {
    children: React.ReactNode,
    title?: string,
    backTo?: string, rightButton?: React.ReactNode
    pb?: number,
    refreshControl?: ReactElement
}

export default function ScrollableLayout({children, title, backTo, rightButton, pb, refreshControl}: Props) {
    const safeAreaInsets = useSafeAreaInsets()
    const setIsTopBlurVisible = useTopBlurStore(state => state.setIsTopBlurVisible);

    return (
        <>
            <SafeAreaView
                style={{
                    backgroundColor: '#fbfbfb',
                    paddingBottom: 0,
                    height: Dimensions.get('window').height - (Platform.OS === 'ios' ? sizes.bottomBarHeight : (80 + safeAreaInsets.bottom))
                }}
                edges={[]}
            >
                {(title || backTo) && (
                    <View
                        className={`flex flex-row ${backTo ? 'justify-between' : 'justify-center'} items-center border-gray-300 border-b pt-2 pb-4 px-3`}
                        style={{
                            paddingTop: safeAreaInsets.top + 10
                        }}
                    >
                        {backTo && (
                            <TouchableOpacity
                                onPress={() => router.back()}
                            >
                                <Ionicons name="chevron-back" size={24} color="black"/>
                            </TouchableOpacity>
                        )}
                        {title && (
                            <Text className="text-sm font-bold">{title}</Text>
                        )}
                        {backTo && (
                            rightButton ? rightButton : <View style={{width: 24}}/>
                        )}
                    </View>
                )}
                <ScrollView
                    contentContainerStyle={{
                        backgroundColor: '#fbfbfb'
                    }}
                    onScroll={e => {
                        setIsTopBlurVisible(e.nativeEvent.contentOffset.y > 0);
                    }}
                    scrollEventThrottle={16}
                    refreshControl={refreshControl || undefined}
                >
                    <View
                        className={`pt-3 pb-12 ${(title || backTo) ? '' : 'mt-16'}`}
                        style={{
                            paddingBottom: pb || 0,
                        }}
                    >
                        {children}
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    );
}
