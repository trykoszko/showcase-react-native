import {Pressable, ScrollView, Text, View} from 'react-native';
import {AxiosInstance, AxiosResponse} from "axios";
import {useAxios} from "../../hooks/useAxios";
import React, {useEffect, useState} from "react";
import {useQuery} from "react-query";
import Loader from "../loader/Loader";
import {StatusCircle} from "./StatusCircle";
import {apiConfig} from "../../config/variables";
import {useRouter} from "expo-router";

export const StatusCircles = () => {
    const axios: AxiosInstance = useAxios();
    const router = useRouter();
    const [viewedItems, setViewedItems] = React.useState<any[]>([])
    const [circles, setCircles] = useState<any>([]);

    const {data, isFetching, error} = useQuery<unknown, unknown, any[]>(
        ['home_circles'],
        async () => {
            try {
                const res: AxiosResponse = await axios.get('/user/home-rings');

                return res?.data ?? [];
            } catch (e) {
                console.log('home_rings exception', e)
            }
        }
    )

    useEffect(() => {
        if (data) {
            const items = data
                ?.reduce((acc: any[], circle: any) => [...acc, ...circle?.events], [])
                ?.filter((event: any) => event?.event?.is_viewed)
                ?.map((event: any) => parseInt(event?.id))
                ?? [];

            if (items?.length) {
                setViewedItems(items);
            }

            setCircles(
                data
                    ?.sort((a, b) => b?.is_viewed - a?.is_viewed)
            );
        }
    }, [data]);

    useEffect(() => {
        console.log('viewedItems', viewedItems);
    }, [viewedItems]);

    if (error) {
        return <Text>Error: {JSON.stringify(error)}</Text>;
    }

    if (!data) {
        return (
            <View
                className={"flex-1 items-center justify-center"}
            >
                <Loader/>
            </View>
        );
    }

    return (
        <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            className="flex-row mb-6"
        >
            {data
                ?.sort((a: any, b: any) => a?.is_viewed - b?.is_viewed)
                ?.map((circle: any, index: number) => (
                    <Pressable
                        key={index}
                        onPress={() => {
                            setViewedItems(
                                viewedItems?.includes(circle?.events[0]?.event?.id)
                                    ? viewedItems
                                    : [...viewedItems, circle?.events[0]?.event?.id]
                            )
                            router.push(`/event/${circle?.events[0]?.event?.uuid}`);
                        }}
                    >
                        <StatusCircle
                            imageSrc={circle?.avatar?.url ?? `${apiConfig.publicUrl}/assets/user.png`}
                            label={circle?.company_name ?? `${circle?.first_name} ${circle?.last_name}`}
                            hasLeftPadding={index === 0}
                            isSpecial={!viewedItems?.includes(circle?.events[0]?.event?.id)}
                        />
                    </Pressable>
                ))}
        </ScrollView>
    );
};
