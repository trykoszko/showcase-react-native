import {Text, View} from "react-native";
import {Image, ImageBackground} from "expo-image";
import React from "react";

const gradientBackground = require('../../../assets/gradient-bg.png');

type CircleProps = {
    hasLeftPadding: boolean;
    imageSrc: string;
    label: string;
    isSpecial: boolean;
}

export const StatusCircle = ({
                                 imageSrc,
                                 label,
                                 hasLeftPadding,
                                 isSpecial
                             }: CircleProps) => {

    return (
        <View
            className={`flex items-center justify-center ${hasLeftPadding ? 'ml-3' : ''}`}
            style={isSpecial ? {} : {
                opacity: 0.6
            }}
        >
            <ImageBackground
                source={gradientBackground}
                className="rounded-full flex items-center justify-center"
                imageStyle={{
                    borderRadius: 100,
                }}
                contentFit={'cover'}
                style={{
                    width: 68,
                    height: 68
                }}
            >
                <Image
                    source={{uri: imageSrc}}
                    className="rounded-full"
                    style={{
                        width: isSpecial ? 60 : 68,
                        height: isSpecial ? 60 : 68
                    }}
                />
            </ImageBackground>
            <Text className="mt-2 text-xs w-20 text-center">{label}</Text>
        </View>
    )
};
