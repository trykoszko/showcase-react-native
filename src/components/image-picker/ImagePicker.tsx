import React from 'react';
import {Image} from 'expo-image';
import {Text, TouchableOpacity, View} from 'react-native';
import i18n from '../../i18n';
import Ionicons from '@expo/vector-icons/Ionicons';
import * as ImagePicker from 'expo-image-picker';

type Props = {
    value: ImagePicker.ImagePickerResult
    resetField: () => void
    setValue: (value: ImagePicker.ImagePickerResult) => void,
    w: number,
    h: number,
    isRounded?: boolean
}

export const ImagePickerButton = ({value, resetField, setValue, w, h, isRounded = false}: Props) =>
{
    return (
        <>
            {value ? (
                <View
                    className={'flex flex-row items-start relative'}
                >
                    <Image
                        style={{
                            width:  w,
                            height: h
                        }}
                        source={{uri: `${value}`}}
                        alt="Uploaded image"
                        className={`rounded-md ${isRounded ? 'rounded-full' : ''} mb-1 mt-1`}
                    />
                    <TouchableOpacity
                        onPress={() =>
                        {
                            resetField();
                        }}
                        className={'flex flex-row items-center absolute'}
                        style={{
                            top:  0,
                            left: w - 14
                        }}
                    >
                        <View
                            className={'flex items-center justify-center bg-red-500 rounded-full w-6 h-6'}
                        >
                            <Ionicons name={'close'} size={18} color={'white'}/>
                        </View>
                    </TouchableOpacity>
                </View>
            ) : (
                 <TouchableOpacity
                     onPress={async () =>
                     {
                         const permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();

                         if (!permissionResult.granted) {
                             alert('no permissions');
                             return;
                         }

                         const result: ImagePicker.ImagePickerResult = await ImagePicker.launchImageLibraryAsync({
                                                                                                                     mediaTypes:    ImagePicker.MediaTypeOptions.Images,
                                                                                                                     allowsEditing: false,
                                                                                                                     aspect:        [5, 4],
                                                                                                                     quality:       .8
                                                                                                                 });

                         if (!result.canceled) {
                             setValue(result);
                         }
                     }}
                     className={'mt-1 block w-full border border-gray-300 rounded-md px-2 py-3 flex flex-row items-center'}
                 >
                     <Ionicons name={'add'} size={20} color={'grey'}/>
                     <Text
                         className={'text-xs ml-1'}
                     >{i18n.imagePicker.addImage}</Text>
                 </TouchableOpacity>
             )}
        </>
    );
};