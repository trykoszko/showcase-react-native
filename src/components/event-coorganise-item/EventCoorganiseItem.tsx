import Ionicons from '@expo/vector-icons/Ionicons';
import {Alert, Pressable, Text, View} from 'react-native';
import {getDaysLeft} from '../../helpers/date-helpers';
import i18n from '../../i18n';
import FontAwesome from '@expo/vector-icons/FontAwesome';
import {Link} from "expo-router";
import {Image} from "expo-image";
import {Event} from "../../types/Event.type";
import {EventEventElement} from "../../types/EventEventElement.type";
import {EventOffer} from "../../types/EventOffer.type";
import React, {useState} from "react";
import {useApi} from "../../hooks/useApi";

type Props = {
    eventElement: EventEventElement;
    event?: Event;
    isOwner?: boolean;
    isFavourite?: boolean;
    setIsOfferModalVisible: (isVisible: boolean) => void;
    setOfferModalUuid: (uuid: string) => void;
}

export const EventCoorganiseItem = ({
                                        eventElement,
                                        event,
                                        isOwner = false,
                                        isFavourite = false,
                                        setIsOfferModalVisible,
                                        setOfferModalUuid
                                    }: Props) => {
    const {saveCoorganizeItem, unsaveCoorganizeItem} = useApi();
    const acceptedOffer: EventOffer | undefined = eventElement?.offers?.find((offer: EventOffer) => offer.is_accepted);
    const [isSaved, setIsSaved] = useState<boolean>(isFavourite);

    const onLongPress = () => {
        if (isOwner) {
            Alert.alert('Long press');
        }
    }

    const toggleFavorite = async () => {
        if (isSaved) {
            await unsaveCoorganizeItem(eventElement.uuid);
        } else {
            await saveCoorganizeItem(eventElement.uuid);
        }
        setIsSaved(!isSaved);
    }

    const coorganise = () => {
        setIsOfferModalVisible(true);
        setOfferModalUuid(eventElement.uuid);
    }

    if (eventElement?.client_has) {
        return (
            <Pressable
                className="flex flex-col mx-3 px-4 mb-3 py-5 bg-white border border-borderGrey rounded-2xl shadow-xl shadow-gray-200"
                onLongPress={onLongPress}
            >
                <View className="flex flex-row justify-start items-center mb-4">
                    <View
                        className="flex flex-row justify-center items-center relative"
                        style={{
                            width: 48,
                            height: 48
                        }}
                    >
                        {/*<Ionicons name="location-sharp" size={22} color="#672171"/>*/}
                        <Text>{eventElement?.event_element?.icon}</Text>
                        <View
                            className={"w-full h-full rounded-xl absolute top-0 left-0"}
                            style={{
                                backgroundColor: eventElement?.event_element?.color,
                                opacity: .1
                            }}
                        />
                    </View>
                    {eventElement?.event_element && (
                        <View className="ml-3 py-2 px-3 border border-primary rounded-md">
                            <Text className="color-primary">
                                {eventElement?.event_element?.name}
                            </Text>
                        </View>
                    )}
                </View>
                <View className="flex flex-row justify-start items-center">
                    {/*<Ionicons name="location-sharp" size={22} color="#FFEE4A"/>*/}
                    <Text>{eventElement?.client_description}</Text>
                </View>
            </Pressable>
        )
    }

    if (acceptedOffer) {
        return (
            <Pressable
                className="flex flex-col mx-3 px-4 mb-3 py-5 bg-white border border-borderGrey rounded-2xl shadow-xl shadow-gray-200"
                onLongPress={onLongPress}
            >
                <View className="flex flex-row justify-start items-center mb-4">
                    <View
                        className="flex flex-row justify-center items-center relative"
                        style={{
                            width: 48,
                            height: 48
                        }}
                    >
                        {/*<Ionicons name="location-sharp" size={22} color="#672171"/>*/}
                        <Text>{eventElement?.event_element?.icon}</Text>
                        <View
                            className={"w-full h-full rounded-xl absolute top-0 left-0"}
                            style={{
                                backgroundColor: eventElement?.event_element?.color,
                                opacity: .1
                            }}
                        />
                    </View>
                    {eventElement?.event_element && (
                        <View className="ml-3 py-2 px-3 border border-primary rounded-md">
                            <Text className="color-primary">
                                {eventElement?.event_element?.name}
                            </Text>
                        </View>
                    )}
                </View>
                <View className="flex flex-col justify-start items-stretch">
                    <Text>{acceptedOffer?.description}</Text>
                    <View
                        className={"pt-3 mt-4 border-t border-t-gray-200 flex-col items-start w-full"}
                    >
                        <Text
                            className={"text-primary"}
                        >
                            {i18n.eventCoorganiseItem.offeredBy}
                        </Text>
                        <View
                            className={"flex-row items-center justify-start mt-2"}
                        >
                            {acceptedOffer?.provider?.avatar?.url && (
                                <View
                                    className={"mr-3"}
                                >
                                    <Link href={`/user/${acceptedOffer?.provider?.uuid}`}>
                                        <Image
                                            source={{uri: acceptedOffer?.provider?.avatar?.url}}
                                            style={{
                                                width: 36,
                                                height: 36,
                                                borderRadius: 36
                                            }}
                                        />
                                    </Link>
                                </View>
                            )}
                            <Text>
                                {acceptedOffer.provider.name}
                            </Text>
                        </View>
                    </View>
                </View>
            </Pressable>
        )
    }

    return (
        <Pressable
            className="border border-borderGrey rounded-lg p-4 mb-3 mx-3 flex flex-col bg-white shadow-md"
            onLongPress={onLongPress}
        >
            <View className="flex flex-row justify-between items-center">
                <View className="flex flex-row items-center">
                    <View
                        className="flex flex-row justify-center items-center rounded-xl"
                        style={{
                            width: 48,
                            height: 48,
                            backgroundColor: '#efe8f0'
                        }}
                    >
                        <Text>{eventElement?.event_element?.icon}</Text>
                        {/*<FontAwesome5 name="hands-helping" size={16} color="#672171"/>*/}
                    </View>
                    {eventElement?.event_element && (
                        <View className="ml-3 py-2 px-3 border border-primary rounded-md">
                            <Text className="color-primary">
                                {eventElement?.event_element?.name}
                            </Text>
                        </View>
                    )}
                </View>
                <Text className="color-primary">
                    {eventElement?.offer_price} {i18n.helpers.currency}
                </Text>
                {!isOwner && (
                    <Pressable onPress={toggleFavorite}>
                        <Ionicons name={isFavourite ? 'heart' : 'heart-outline'} size={24} color="#952aa9"/>
                    </Pressable>
                )}
            </View>
            {eventElement?.client_description && (
                <Text
                    className={"mt-5"}
                >
                    {eventElement?.client_description}
                </Text>
            )}
            {event?.start_date && (
                <View className="flex flex-row mt-3 py-2">
                    <FontAwesome name="calendar-times-o" size={16} color="#a1a1a9"/>
                    <Text className="text-gray-500 ml-2">
                        {i18n.eventCoorganiseItem.daysLeft} {getDaysLeft(new Date(event?.start_date))} {i18n.eventCoorganiseItem.days}
                    </Text>
                </View>
            )}
            {!isOwner && (
                <View className="mt-3 pt-3 border-t border-borderGrey">
                    <Pressable
                        className={`rounded-md bg-primary`}
                        onPress={coorganise}
                    >
                        <Text
                            className={`text-base font-bold py-3 px-3 text-center text-white`}
                        >
                            {i18n.eventCoorganiseItem.sendOffer}
                        </Text>
                    </Pressable>
                </View>
            )}
        </Pressable>
    );
};
