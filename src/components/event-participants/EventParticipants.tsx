import {Pressable, Text, View} from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import i18n from '../../i18n';
import React, {useState} from 'react';
import {ModalComponent} from '../modal/Modal';
import {ParticipantList} from '../participant-list/ParticipantList';
import {useAuthStore} from "../../store/AuthStore";
import {EventUser} from "../../types/EventUser.type";
import {MyUser} from "../../types/MyUser.type";

type Props = {
    participants: EventUser[],
    slots: number,
    slotsLeft: number,
    isOwner: boolean
}

export const EventParticipants = ({isOwner, participants, slots, slotsLeft}: Props) =>
{
    const [isModalOpen, setIsModalOpen] = useState(false);
    const userData: MyUser | undefined                      = useAuthStore(state => state.userData);

    const friendsCount = participants
        ?.filter((participant: EventUser) =>
            userData
                ?.friends
                ?.includes(participant.id)
        )
        ?.length;

    return (
        <>

            <Pressable
                className="flex flex-col mx-3 px-4 mb-3 py-5 bg-white border border-borderGrey rounded-2xl shadow-xl shadow-gray-200"
                onPress={() => setIsModalOpen(true)}
            >
                <View className="flex flex-row justify-start items-center mb-4">
                    <View
                        className="flex flex-row justify-center items-center rounded-xl"
                        style={{
                            width:           48,
                            height:          48,
                            backgroundColor: '#fffef8'
                        }}
                    >
                        <Ionicons name="person-outline" size={22} color="#6a642b"/>
                    </View>
                    <Text className="ml-3">{i18n.eventParticipants.title}</Text>
                </View>
                <View className="flex flex-row justify-start items-center">
                    <Ionicons name="people" size={22} color="#a1a1a9"/>
                    <Text className="ml-3">{slots - slotsLeft} (<Text
                        className="color-primary">{friendsCount} {i18n.eventParticipants.friends}</Text>)
                        / {slots} {i18n.eventParticipants.slots}</Text>
                </View>
            </Pressable>

            <ModalComponent
                isVisible={isModalOpen}
                setIsVisible={setIsModalOpen}
                title={i18n.eventParticipants.modalTitle}
                content={(
                    <View className="flex-col">
                        {participants?.length && (
                            <ParticipantList participants={participants} setIsModalOpen={setIsModalOpen} isOwner={isOwner}/>
                        )}
                    </View>
                )}
            />
        </>
    );
};
