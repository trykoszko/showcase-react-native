import {Modal, Platform, Text, TouchableOpacity, View} from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import {BlurView} from 'expo-blur';

type Props = {
    isVisible: boolean;
    setIsVisible: (isVisible: boolean) => void;
    title: string;
    content: React.ReactNode;
}

export const ModalComponent = ({isVisible, setIsVisible, title, content}: Props) =>
{
    return (
        <>
            <Modal
                visible={isVisible}
                animationType="slide"
                presentationStyle={'formSheet'}
                style={{
                    backgroundColor: 'white'
                }}
                onRequestClose={() => setIsVisible(false)}
            >
                <View
                    className="flex-1"
                >
                    <BlurView
                        intensity={20}
                        className="h-full w-full"
                    />
                    <View
                        className={`flex-col p-4 bg-white rounded-t-xl absolute top-0 left-0 w-full ${Platform.OS === 'android' ? 'h-full' : ''}`}
                    >
                        <View
                            className="flex-row justify-between items-center mb-4 pb-4 mt-2 border-b border-borderGrey"
                        >
                            <Text>{title}</Text>
                            <TouchableOpacity
                                onPress={() => setIsVisible(false)}
                            >
                                <Ionicons
                                    name="close"
                                    size={24}
                                    color="black"
                                />
                            </TouchableOpacity>
                        </View>
                        <View
                            className="flex-col w-100 h-100"
                        >
                            {content}
                        </View>
                    </View>
                </View>
            </Modal>
        </>
    );
};
