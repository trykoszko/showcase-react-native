import {Image, ImageBackground} from 'expo-image';
import {View} from 'react-native';
import {useGlobalLoaderStore} from '../../store/GlobalLoaderStore';
import Loader from '../loader/Loader';
import {useAuthStore} from "../../store/AuthStore";

const splashImageBackground = require('../../../assets/background.jpg');
const logoImage             = require('../../../assets/logo.png');

export default function GlobalLoader()
{
    const isLoading = useGlobalLoaderStore(state => state.isLoading);
    const userData = useAuthStore(state => state.userData);
    const apiToken = useAuthStore(state => state.apiToken);

    const style = {
        zIndex: 10001
    };

    // hide loader only if user data is already fetched
    if (userData?.first_name || !apiToken || apiToken === '') {
        if (!isLoading) {
            return <></>;
        }
    }

    return (
        <View style={style} className="absolute top-0 left-0 w-full h-full flex flex-row justify-center items-center">
            <ImageBackground
                source={splashImageBackground}
                style={{width: '100%', height: '100%'}}
                contentFit={'cover'}
                className="flex items-center justify-center"
            >
                <Image
                    source={logoImage}
                    style={{
                        width:  400,
                        height: 400,
                    }}
                    contentFit={'contain'}
                />
                <Loader/>
            </ImageBackground>
        </View>
    );
}
