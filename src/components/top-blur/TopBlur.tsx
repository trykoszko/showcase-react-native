import {BlurView} from 'expo-blur';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

export const TopBlur = () =>
{
    const insets = useSafeAreaInsets();

    return <BlurView
        style={{
            position: 'absolute',
            top:      0,
            left:     0,
            width:    '100%',
            height:   insets.top,
            zIndex:   10002,
        }}
        intensity={20}
    />;
};
