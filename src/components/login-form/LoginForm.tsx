import Ionicons from '@expo/vector-icons/Ionicons';
import {yupResolver} from '@hookform/resolvers/yup';
import {useRouter} from 'expo-router';
import React, {useEffect, useState} from 'react';
import {Controller, FieldValues, useForm} from 'react-hook-form';
import {Alert, Pressable, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {useMutation} from 'react-query';
import * as yup from 'yup';
import i18n from '../../i18n';
import {useAuthStore} from '../../store/AuthStore';
import {usePushNotificationStore} from '../../store/PushNotificationStore';
import SocialLoginButtons from './SocialLoginButtons';
import Loader from "../loader/Loader";
import {usePublicAxios} from "../../hooks/usePublicAxios";
import * as WebBrowser from 'expo-web-browser';

const formSchema = yup.object().shape({
    email: yup.string().email().required(),
    password: yup.string().min(8),
    device_token: yup.string().min(8).optional()
});

const LoginForm = () => {
    const axios = usePublicAxios();
    const router = useRouter();

    const setApiToken = useAuthStore(state => state.setApiToken);
    const setRefreshToken = useAuthStore(state => state.setRefreshToken);
    const setEol = useAuthStore(state => state.setEol);

    const expoPushToken = usePushNotificationStore(state => state.expoPushToken);

    const [loading, setLoading] = useState(false);

    const {control, watch, handleSubmit, formState: {errors}, setValue} = useForm({
        resolver: yupResolver(formSchema)
    });

    const apiRequest = useMutation((formData: FieldValues) => axios.post(
        `/auth/login`,
        formData
    ));

    const onFormSubmit = ((formData: FieldValues) => {
        apiRequest.mutate(formData);
    });

    useEffect(() => {
        if (apiRequest.status === 'success') {
            setApiToken(apiRequest?.data?.data?.access_token);
            setRefreshToken(apiRequest?.data?.data?.refresh_token);
            setEol(apiRequest?.data?.data?.eol);
            setLoading(false);
        }
        if (apiRequest.status === 'error') {
            Alert.alert('Błąd serwera', 'Spróbuj później');
            setLoading(false);
        }
    }, [apiRequest.status]);

    useEffect(() => {
        if (expoPushToken) {
            setValue('device_token', expoPushToken);
        }
    }, [expoPushToken]);

    const watchFields = watch(['email', 'password']);
    const isNextButtonDisabled = watchFields.some((field: any) => !field);

    return (
        <View>
            <Text className="text-primary font-bold text-lg mb-4">{i18n.loginForm.heading}</Text>

            <Controller
                control={control}
                render={({field: {onChange, onBlur}}) => (
                    <TextInput
                        onBlur={onBlur}
                        onChangeText={(val) => onChange(val)}
                        className={`border py-3 px-2 rounded ${errors.email ? 'mb-1 border-red-500' : 'mb-6 border-gray-300'}`}
                        placeholder={i18n.loginForm.emailLabel}
                        keyboardType="email-address"
                        autoCapitalize="none"
                        autoComplete="email"
                        autoCorrect={false}
                        autoFocus={true}
                    />
                )}
                name="email"
                rules={{required: true}}
                defaultValue=""
            />
            {errors?.email?.message && (
                <Text className="text-red-500 text-xs italic mb-3">{errors.email.message}</Text>
            )}

            <Controller
                control={control}
                render={({field: {onChange, onBlur}}) => (
                    <TextInput
                        onBlur={onBlur}
                        onChangeText={(val) => onChange(val)}
                        className={`border py-3 px-2 rounded ${errors.password ? 'mb-1 border-red-500' : 'mb-6 border-gray-300'}`}
                        placeholder={i18n.loginForm.passwordLabel}
                        autoCapitalize="none"
                        autoComplete="password"
                        autoCorrect={false}
                        passwordRules="minlength: 8"
                        secureTextEntry={true}
                    />
                )}
                name="password"
                rules={{required: true}}
                defaultValue=""
            />
            {errors?.password?.message && (
                <Text className="text-red-500 text-xs italic mb-2">{errors.password.message}</Text>
            )}

            <View className="flex flex-row justify-end">
                <Pressable
                    onPress={async () => {
                        await WebBrowser.openBrowserAsync('https://app.com/forgot-password');
                    }}
                >
                    <Text className="underline mb-8">{i18n.loginForm.forgotPassword}</Text>
                </Pressable>
            </View>

            {loading ? (
                <View
                    className="flex flex-row items-center justify-center h-10"
                >
                    <Loader/>
                </View>
            ) : (
                <TouchableOpacity
                    onPress={() => {
                        if (!isNextButtonDisabled) {
                            setLoading(true);
                            handleSubmit(onFormSubmit)();
                        }
                    }}
                    className={`bg-primary py-3 px-2 rounded mb-4 ${isNextButtonDisabled ? 'opacity-50' : ''}`}
                >
                    <Text className="font-bold text-white text-center">{i18n.loginForm.submit}</Text>
                </TouchableOpacity>
            )}

            <View className="flex flex-row justify-center mb-4">
                <Text className="text-gray-500">{i18n.loginForm.or}</Text>
            </View>

            <TouchableOpacity
                onPress={() => {
                    router.push('/register');
                }}
                className="border border-gray-300 p-3 rounded mb-4"
            >
                <View className="flex flex-row items-center justify-between">
                    <Ionicons name="person-outline" size={20} color="black"/>
                    <Text className="ml-3">{i18n.loginForm.register}</Text>
                    <View
                        className={'w-8'}
                    />
                </View>
            </TouchableOpacity>

            <SocialLoginButtons/>
        </View>
    );
};

export default LoginForm;
