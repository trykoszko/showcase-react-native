import Ionicons from '@expo/vector-icons/Ionicons';
import appleAuth from '@invertase/react-native-apple-authentication';
import {GoogleSignin} from '@react-native-google-signin/google-signin';
import * as Device from 'expo-device';
import {useEffect} from 'react';
import {FieldValues} from 'react-hook-form';
import {Alert, Text, TouchableOpacity, View} from 'react-native';
import {LoginManager, Profile} from 'react-native-fbsdk-next';
import FBProfile from 'react-native-fbsdk-next/lib/typescript/src/FBProfile';
import {useMutation} from 'react-query';
import i18n from '../../i18n';
import {useAuthStore} from '../../store/AuthStore';
import {useGlobalLoaderStore} from '../../store/GlobalLoaderStore';
import {usePushNotificationStore} from '../../store/PushNotificationStore';
import {usePublicAxios} from "../../hooks/usePublicAxios";

export default function SocialLoginButtons() {
    const axios = usePublicAxios();

    const setApiToken = useAuthStore(state => state.setApiToken);
    const setRefreshToken = useAuthStore(state => state.setRefreshToken);
    const setEol = useAuthStore(state => state.setEol);

    const setIsLoading = useGlobalLoaderStore(state => state.setIsLoading);
    const expoPushToken = usePushNotificationStore(state => state.expoPushToken);

    const apiRequest = useMutation((formData: FieldValues) => axios.post(
        '/auth/social-auth',
        formData
    ));

    useEffect(() => {
        if (apiRequest.status === 'success') {
            setApiToken(apiRequest?.data?.data?.access_token);
            setRefreshToken(apiRequest?.data?.data?.refresh_token);
            setEol(apiRequest?.data?.data?.eol);
            setIsLoading(false);
        }
        if (apiRequest.status === 'error') {
            Alert.alert(`Social auth error: ${apiRequest.status}`, JSON.stringify([apiRequest?.error, apiRequest?.data, apiRequest?.context]));
            setIsLoading(false);
        }
    }, [apiRequest.status]);

    const handleFacebookLogin = async (): Promise<void> => {
        try {
            const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);

            if (!result.isCancelled) {
                const profile: FBProfile | null = await Profile.getCurrentProfile();

                if (!profile) {
                    Alert.alert('Facebook login', 'Something went wrong');
                    return;
                }

                setIsLoading(true);

                apiRequest.mutate({
                    facebook_user_id: profile?.userID,
                    email: profile?.email,
                    first_name: profile?.firstName,
                    last_name: profile?.lastName,
                    image_url: profile?.imageURL,
                    provider: 'facebook',
                    device_token: expoPushToken
                });
            }
        } catch (error) {
            console.log(`Facebook login error: ${apiRequest.status}`, JSON.stringify([apiRequest?.error, apiRequest?.data, apiRequest?.context]));
            await Promise.reject(error);
            setIsLoading(false);
        }
    };

    const handleGoogleLogin = async () => {
        try {
            await GoogleSignin.hasPlayServices({
                showPlayServicesUpdateDialog: true
            });

            const profile = await GoogleSignin.signIn();

            if (!profile.idToken) {
                setIsLoading(false);

                Alert.alert('Google login', 'Something went wrong');
                return;
            }

            setIsLoading(true);

            apiRequest.mutate({
                google_user_id: profile?.user?.id,
                email: profile?.user?.email,
                first_name: profile?.user?.givenName,
                last_name: profile?.user?.familyName,
                image_url: profile?.user?.photo,
                provider: 'google',
                device_token: expoPushToken
            });
        } catch (error) {
            console.log(`Google login error: ${apiRequest.status}`, JSON.stringify([apiRequest?.error, apiRequest?.data, apiRequest?.context]));
            await Promise.reject(error);
            setIsLoading(false);
        }
    };

    const handleAppleLogin = async () => {
        try {
            const profile = await appleAuth.performRequest({
                requestedOperation: appleAuth.Operation.LOGIN,
                requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
            });

            if (!profile.user) {
                Alert.alert('Apple login', 'Something went wrong');
                return;
            }

            setIsLoading(true);

            apiRequest.mutate({
                apple_user_id: profile?.user,
                email: profile?.email,
                first_name: profile?.fullName?.givenName,
                last_name: profile?.fullName?.familyName,
                provider: 'apple',
                device_token: expoPushToken
            });
        } catch (error) {
            console.log(`Apple login error: ${apiRequest.status}`, JSON.stringify([apiRequest?.error, apiRequest?.data, apiRequest?.context]));
            await Promise.reject(error);
            setIsLoading(false);
        }
    };

    return (
        <>
            <TouchableOpacity
                onPress={handleFacebookLogin}
                className="border border-gray-300 p-3 rounded mb-4"
            >
                <View className="flex flex-row items-center justify-between">
                    <Ionicons name="logo-facebook" size={20} color="black"/>
                    <Text className="ml-3">{i18n.socialLoginButtons.facebook}</Text>
                    <View
                        className={'w-8'}
                    />
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={handleGoogleLogin}
                className="border border-gray-300 p-3 rounded mb-4"
            >
                <View className="flex flex-row items-center justify-between">
                    <Ionicons name="logo-google" size={20} color="black"/>
                    <Text className="ml-3">{i18n.socialLoginButtons.google}</Text>
                    <View
                        className={'w-8'}
                    />
                </View>
            </TouchableOpacity>

            {(Device.brand === 'Apple' && appleAuth.isSupported) && (
                <TouchableOpacity
                    onPress={handleAppleLogin}
                    className="border border-gray-300 p-3 rounded mb-4"
                >
                    <View className="flex flex-row items-center justify-between">
                        <Ionicons name="logo-apple" size={20} color="black"/>
                        <Text className="ml-3">{i18n.socialLoginButtons.apple}</Text>
                        <View
                            className={'w-8'}
                        />
                    </View>
                </TouchableOpacity>
            )}

            {/* <TouchableOpacity
             onPress={() => {
             console.log('phone')
             }}
             className="border border-gray-300 p-3 rounded mb-4"
             >
             <View className="flex flex-row items-center">
             <Ionicons name="phone-portrait-outline" size={20} color="black" />
             <Text className="ml-3">{i18n.socialLoginButtons.phone}</Text>
             </View>
             </TouchableOpacity> */}
        </>
    );
}