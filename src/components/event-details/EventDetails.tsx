import {Image, Text, TouchableOpacity, View} from 'react-native';
import {getEventDates} from '../../helpers/date-helpers';
import i18n from '../../i18n';
import {useRouter} from 'expo-router';
import {useAuthStore} from "../../store/AuthStore";

type Props = {
    dateTime: Date,
    duration: number,
    title: string,
    creatorUuid: string,
    creatorProfilePictureUrl: string,
    creatorName: string,
    creatorRole?: string,
    creatorSocialHandle?: string,
    creatorSocialUrl?: string
}

export const EventDetails = ({
                                 dateTime,
                                 duration,
                                 title,
                                 creatorUuid,
                                 creatorProfilePictureUrl,
                                 creatorName,
                                 creatorRole,
                                 creatorSocialHandle,
                                 creatorSocialUrl
                             }: Props) => {
    const router = useRouter();
    const userData = useAuthStore(state => state.userData);

    return (
        <View
            className="flex flex-col mx-3 mb-3 px-4 py-5 mt-10 bg-white border border-borderGrey rounded-2xl shadow-xl shadow-gray-200"
        >
            <View
                className="flex flex-row justify-between items-center mb-1"
            >
                <Text className="text-md text-primary">{getEventDates(dateTime, duration)}</Text>
            </View>
            <Text className="text-2xl text-darkGrey font-bold mb-3">{title}</Text>
            <View
                className="w-full bg-borderGrey mb-4"
                style={{
                    height: 1
                }}
            />
            <View className="flex flex-row justify-start items-center">
                <TouchableOpacity
                    onPress={() => router.push(
                        userData?.uuid === creatorUuid
                        ? '/profile'
                        : `/user/${creatorUuid}`
                    )}
                >
                    <Image
                        source={{uri: creatorProfilePictureUrl}}
                        style={{width: 40, height: 40, borderRadius: 20}}
                        className="mr-3"
                    />
                </TouchableOpacity>
                <View className="flex flex-col justify-start w-full">
                    <Text className="text-sm text-primary">{i18n.eventDetails.creator}</Text>
                    <Text className="text-sm text-darkGrey font-bold">
                        {creatorName}
                    </Text>
                    <View className="flex flex-row justify-start items-center flex-wrap max-w-full">
                        {creatorRole && (
                            <Text className="text-sm text-grey-500">{creatorRole}</Text>
                        )}
                        {(creatorSocialHandle && creatorSocialUrl) && (
                            <>
                                <View
                                    style={{
                                        width: 4,
                                        height: 4,
                                        borderRadius: 2,
                                    }}
                                    className="mx-1 bg-lightGrey"
                                />
                                <TouchableOpacity
                                    onPress={() => router.push(creatorSocialUrl)}
                                >
                                    <Text className="text-sm text-blue-400">{creatorSocialHandle}</Text>
                                </TouchableOpacity>
                            </>
                        )}
                    </View>
                </View>
            </View>
        </View>
    );
};