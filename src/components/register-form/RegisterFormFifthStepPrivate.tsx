import {Alert, Platform, Pressable, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import i18n from '../../i18n';
import Ionicons from '@expo/vector-icons/Ionicons';
import {useRouter} from 'expo-router';
import {Image} from 'expo-image';
import {useGlobalLoaderStore} from "../../store/GlobalLoaderStore";
import {useMutation} from "react-query";
import {FieldValues} from "react-hook-form";
import axios from "axios";
import {apiConfig} from "../../config/variables";
import * as WebBrowser from 'expo-web-browser';

const howToUseImage = require('../../../assets/how-to-use.png');

type Props = {
    control: any,
    errors: any,
    handleSubmit: any,
    setCurrentStep: (step: number) => void
}

export const RegisterFormFifthStepPrivate = ({
                                                 control,
                                                 errors,
                                                 handleSubmit,
                                                 setCurrentStep
                                             }: Props) => {
    const setIsLoading = useGlobalLoaderStore(state => state.setIsLoading);
    const router = useRouter();
    const axiosClient = axios.create({
        baseURL: apiConfig.url
    });

    const apiRequest = useMutation((formData: FieldValues) => axiosClient.post(
            '/auth/register',
            formData,
            {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        )
            .then(() => {
                Alert.alert(
                    i18n.registerFormFifthStepPrivate.congratulations,
                    i18n.registerFormFifthStepPrivate.accountCreated,
                    [
                        {
                            text: 'OK',
                            onPress: () => {
                                setIsLoading(false);
                                router.push('/');
                            }
                        }
                    ]
                );
            })
            .catch((error) => {
                setIsLoading(false);

                Alert.alert(
                    i18n.registerFormFifthStepPrivate.submitError,
                    error?.response?.data?.message?.join(', ') ?? i18n.registerFormFifthStepPrivate.submitErrorText
                );
            })
    );

    const sendRequest = ((formData: FieldValues) => {
        setIsLoading(true);

        const data: FormData = new FormData();

        for (let [key, value] of Object.entries(formData)) {
            if (key === 'birthdate') {
                value = value.toISOString()
            }
            if (key === 'image') {
                if (!value) {
                    continue;
                }

                const uri =
                    Platform.OS === "android"
                        ? value
                        : value?.replace("file://", "")
                const filename = value.split("/").pop()
                const match = /\.(\w+)$/.exec(filename as string)
                const ext = match?.[1]
                const type = match ? `image/${match[1]}` : `image`

                value = {
                    uri: uri,
                    name: `image.${ext}`,
                    type: type
                };
            }
            if (key === 'elements') {
                for (let i = 0; i < value.length; i++) {
                    data.append('elements', value[i].uuid);
                }

                continue;
            }
            if (key === 'city') {
                value = value.uuid;
            }

            data.append(key, value);
        }

        apiRequest.mutate(data);
    });

    return (
        <View
            className={'flex flex-col justify-between items-stretch h-full relative'}
        >
            <View
                className={`flex flex-row justify-between items-center pt-2 pb-6 px-3`}
                style={{
                    height: '8%',
                    zIndex: 100
                }}
            >
                <TouchableOpacity
                    onPress={() => setCurrentStep(4)}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
                <Text className="text-sm font-bold">{i18n.registerFormFifthStepPrivate.title}</Text>
                <View style={{width: 24}}/>
            </View>
            <View
                className={'w-full flex flex-row items-center justify-center h-1'}
            >
                <View
                    className={'w-11/12 border-gray-300 border-b h-1'}
                />
            </View>
            <ScrollView
                scrollEventThrottle={16}
                style={{
                    height: '79%'
                }}
            >
                <View
                    className="flex flex-col justify-start items-center grow p-3"
                >
                    <Text
                        className={'mb-16 mt-2'}
                    >{i18n.registerFormFifthStepPrivate.text}</Text>

                    <Pressable
                        onPress={async () => {
                            await WebBrowser.openBrowserAsync('https://app.com/tutorial');
                        }}
                    >
                        <Image
                            source={howToUseImage}
                            style={{
                                width: 318,
                                height: 270,
                                alignSelf: 'center'
                            }}
                        />
                    </Pressable>

                </View>
            </ScrollView>
            <View
                className="mt-auto flex flex-col justify-between items-center border-gray-300 border-t pt-2 pb-6 px-3"
                style={{
                    height: '13%'
                }}
            >
                <TouchableOpacity
                    onPress={() => {
                        if (Object.keys(errors).length) {
                            Alert.alert(i18n.registerFormFifthStepPrivate.submitError, i18n.registerFormFifthStepPrivate.submitErrorText);
                        } else {
                            handleSubmit(sendRequest)();
                        }
                    }}
                    className={'flex flex-row items-center justify-center bg-primary rounded-md w-full mb-3'}
                >
                    <Text
                        className={`text-base font-bold py-3 px-3 text-center text-white`}>
                        {i18n.registerFormFifthStepPrivate.skip}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};
