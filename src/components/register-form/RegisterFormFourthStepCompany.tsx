import {Dimensions, Pressable, ScrollView, Switch, Text, TextInput, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import i18n from '../../i18n';
import Ionicons from '@expo/vector-icons/Ionicons';
import {ModalComponent} from '../modal/Modal';
import {Controller} from "react-hook-form";
import {EventElement} from "../../types/EventElement.type";
import {EventElementComponent} from "../event-element-component/EventElementComponent";
import Slider from "@react-native-community/slider";
import {useQuery} from "react-query";
import {AxiosResponse} from "axios";
import {Language} from "../../types/Language.type";
import {EventTemplate} from "../../types/EventTemplate.type";
import {usePublicAxios} from "../../hooks/usePublicAxios";

type Props = {
    control: any,
    errors: any,
    setCurrentStep: (step: number) => void,
    getValues: any,
}

export const RegisterFormFourthStepCompany = ({
                                                  control,
                                                  errors,
                                                  setCurrentStep,
                                                  getValues
                                              }: Props) => {
    const axios = usePublicAxios();

    const [isHelpModalOpen, setIsHelpModalOpen] = useState<boolean>(false);
    const [helpModalTitle, setHelpModalTitle] = useState<string>('');
    const [helpModalContent, setHelpModalContent] = useState<string>('');

    const [isHandledEventsModalOpen, setIsHandledEventsModalOpen] = useState<boolean>(false);
    const [isHandledLanguagesModalOpen, setIsHandledLanguagesModalOpen] = useState<boolean>(false);
    const [isPreferredContactsModalOpen, setIsPreferredContactsModalOpen] = useState<boolean>(false);
    const [isAgeRestrictionsModalOpen, setIsAgeRestrictionsModalOpen] = useState<boolean>(false);

    const {data: languages} = useQuery<unknown, unknown, { data: Language[] }>({
        queryKey: ['languages'],
        queryFn: async () => {
            try {
                const res: AxiosResponse = await axios.get(`public/language`)

                return res?.data ?? null;
            } catch (e) {
                console.log('language exception', e)
            }
        }
    });

    const {data: eventTemplates} = useQuery<unknown, unknown, { data: EventTemplate[] }>({
        queryKey: ['event_templates'],
        queryFn: async () => {
            try {
                const res: AxiosResponse = await axios.get(`public/event-template`)

                return res?.data ?? null;
            } catch (e) {
                console.log('event_templates exception', e)
            }
        }
    });

    return (
        <View
            className={'flex flex-col justify-between items-stretch h-full relative'}
        >
            <View
                className={`flex flex-row justify-between items-center pt-2 pb-6 px-3`}
                style={{
                    height: '8%',
                    zIndex: 100
                }}
            >
                <TouchableOpacity
                    onPress={() => setCurrentStep(3)}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
                <Text className="text-sm font-bold">{i18n.registerFormFourthStepCompany.title}</Text>
                <View style={{width: 24}}/>
            </View>
            <View
                className={'w-full flex flex-row items-center justify-center h-1'}
            >
                <View
                    className={'w-11/12 border-gray-300 border-b h-1'}
                />
            </View>
            <ScrollView
                scrollEventThrottle={16}
                style={{
                    height: '79%'
                }}
            >
                <View className="flex flex-col justify-start grow p-3">
                    <Text
                        className={'mb-5'}
                    >{i18n.registerFormFourthStepCompany.text}</Text>

                    {getValues('elements')?.length ? (
                        <View className={"mb-5"}>
                            {getValues('elements')?.map((element: EventElement, index: number) => (
                                <View key={index}>
                                    <EventElementComponent element={element}/>
                                </View>
                            ))}
                        </View>
                    ) : <></>}

                    <Controller
                        name={'kind'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'kind' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs'}
                                >{i18n.registerFormFourthStepCompany.kind}</Text>
                                <TextInput
                                    className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                    value={value}
                                    placeholder={i18n.registerFormFourthStepCompany.kind}
                                    onChange={(e) => onChange(e.nativeEvent.text)}
                                    autoCorrect={false}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'people_limit'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className="flex flex-col items-start justify-between mt-3"
                                style={'people_limit' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                >{i18n.registerFormFourthStepCompany.peopleLimit}</Text>

                                <View className={'flex flex-col items-between mt-2 pb-4 border-b border-b-gray-200'}>
                                    {(value && value > 0) ? (
                                        <Text
                                            className={'self-end'}
                                        >
                                            {value === 1 ? '1 osoba' : `1 - ${value} osób`}
                                        </Text>
                                    ) : (
                                        <Text
                                            className={'self-end'}
                                        >
                                            Nieograniczona ilość
                                        </Text>
                                    )}
                                    <Slider
                                        style={{width: Dimensions.get('window').width - 30, height: 20}}
                                        minimumValue={0}
                                        maximumValue={200}
                                        step={1}
                                        minimumTrackTintColor="#952aa9"
                                        maximumTrackTintColor="#ececec"
                                        value={value}
                                        onValueChange={value => {
                                            onChange(value);
                                        }}
                                    />
                                </View>
                            </View>
                        )}
                    />

                    <Controller
                        name={'has_staff'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className="flex flex-row items-center justify-between mt-4 pb-4 border-b border-b-gray-200"
                                style={'has_staff' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text>{i18n.registerFormFourthStepCompany.hasStaff}</Text>
                                <TouchableOpacity
                                    className={'ml-auto mr-3'}
                                    onPress={() => {
                                        setHelpModalTitle(i18n.registerFormFourthStepCompany.hasStaff);
                                        setHelpModalContent(i18n.registerFormFourthStepCompany.hasStaffHelp);
                                        setIsHelpModalOpen(true);
                                    }}
                                >
                                    <Ionicons name={'help-circle-outline'} size={20} color={'black'}/>
                                </TouchableOpacity>
                                <Switch
                                    value={value}
                                    onValueChange={(value) => {
                                        onChange(value);
                                    }}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'can_travel'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className="flex flex-row items-center justify-between mt-4 pb-4 border-b border-b-gray-200"
                                style={'can_travel' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text>{i18n.registerFormFourthStepCompany.canTravel}</Text>
                                <TouchableOpacity
                                    className={'ml-auto mr-3'}
                                    onPress={() => {
                                        setHelpModalTitle(i18n.registerFormFourthStepCompany.canTravel);
                                        setHelpModalContent(i18n.registerFormFourthStepCompany.canTravelHelp);
                                        setIsHelpModalOpen(true);
                                    }}
                                >
                                    <Ionicons name={'help-circle-outline'} size={20} color={'black'}/>
                                </TouchableOpacity>
                                <Switch
                                    value={value}
                                    onValueChange={(value) => {
                                        onChange(value);
                                    }}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'can_invoice'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className="flex flex-row items-center justify-between mt-4 pb-4 border-b border-b-gray-200"
                                style={'can_invoice' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text>{i18n.registerFormFourthStepCompany.canInvoice}</Text>
                                <TouchableOpacity
                                    className={'ml-auto mr-3'}
                                    onPress={() => {
                                        setHelpModalTitle(i18n.registerFormFourthStepCompany.canInvoice);
                                        setHelpModalContent(i18n.registerFormFourthStepCompany.canInvoiceHelp);
                                        setIsHelpModalOpen(true);
                                    }}
                                >
                                    <Ionicons name={'help-circle-outline'} size={20} color={'black'}/>
                                </TouchableOpacity>
                                <Switch
                                    value={value}
                                    onValueChange={(value) => {
                                        onChange(value);
                                    }}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'handled_events'}
                        control={control}
                        defaultValue={[]}
                        render={({field: {onChange, value}}) => (
                            <>
                                <View
                                    className={'flex flex-col items-stretch mt-3'}
                                    style={'handled_events' in errors ? {
                                        padding: 3,
                                        borderColor: 'red',
                                        borderWidth: 1
                                    } : {}}
                                >
                                    <Text
                                        className={'text-xs'}
                                    >{i18n.registerFormFourthStepCompany.handledEvents}</Text>
                                    <Pressable
                                        onPress={() => setIsHandledEventsModalOpen(true)}
                                        className={'mt-1 block w-full border border-gray-300 rounded-md px-2 py-3 flex flex-row items-center justify-between'}
                                    >
                                        <Text>{value?.length ? value?.map((template: EventTemplate) => template?.name).join(', ') : i18n.registerFormFourthStepCompany.handledEvents}</Text>
                                        <Ionicons name={'chevron-down'} size={18} color={'black'}/>
                                    </Pressable>
                                </View>
                                <ModalComponent
                                    isVisible={isHandledEventsModalOpen}
                                    setIsVisible={setIsHandledEventsModalOpen}
                                    title={i18n.registerFormFourthStepCompany.handledEvents}
                                    content={(<>
                                        <View
                                            className={'flex flex-col'}
                                            style={{
                                                maxHeight: Dimensions.get('window').height - 200
                                            }}
                                        >
                                            <ScrollView
                                                horizontal={false}
                                                className={'flex flex-col px-3'}
                                            >
                                                {eventTemplates?.data?.length && eventTemplates?.data?.map((eventTemplate: EventTemplate, index: number) => (
                                                    <Pressable
                                                        key={index}
                                                        onPress={() => {
                                                            const items = value?.map((item: EventTemplate) => item.uuid).includes(eventTemplate.uuid)
                                                                ? value.filter((item: EventTemplate) => item.uuid !== eventTemplate.uuid)
                                                                : [...value, eventTemplate];

                                                            onChange(items);
                                                        }}
                                                        className={'flex flex-row items-center justify-between mb-2'}
                                                    >
                                                        <Text
                                                            className={'py-2'}
                                                        >{eventTemplate.name}</Text>
                                                        <View
                                                            className={'w-4 h-4 border flex items-center justify-center'}
                                                        >
                                                            {value?.map((item: EventTemplate) => item.uuid).includes(eventTemplate.uuid) && (
                                                                <View className={'w-2 h-2 bg-black'}/>
                                                            )}
                                                        </View>
                                                    </Pressable>
                                                ))}
                                            </ScrollView>
                                        </View>
                                    </>)}
                                />
                            </>
                        )}
                    />

                    <Controller
                        name={'handled_languages'}
                        control={control}
                        defaultValue={[]}
                        render={({field: {onChange, value}}) => (
                            <>
                                <View
                                    className={'flex flex-col items-stretch mt-3'}
                                    style={'handled_languages' in errors ? {
                                        padding: 3,
                                        borderColor: 'red',
                                        borderWidth: 1
                                    } : {}}
                                >
                                    <Text
                                        className={'text-xs'}
                                    >{i18n.registerFormFourthStepCompany.handledLanguages}</Text>
                                    <Pressable
                                        onPress={() => setIsHandledLanguagesModalOpen(true)}
                                        className={'mt-1 block w-full border border-gray-300 rounded-md px-2 py-3 flex flex-row items-center justify-between'}
                                    >
                                        <Text>{value?.length ? value?.map((language: Language) => language?.native_name).join(', ') : i18n.registerFormFourthStepCompany.handledLanguages}</Text>
                                        <Ionicons name={'chevron-down'} size={18} color={'black'}/>
                                    </Pressable>
                                </View>
                                <ModalComponent
                                    isVisible={isHandledLanguagesModalOpen}
                                    setIsVisible={setIsHandledLanguagesModalOpen}
                                    title={i18n.registerFormFourthStepCompany.handledLanguages}
                                    content={(<>
                                        <View
                                            className={'flex flex-col'}
                                            style={{
                                                maxHeight: Dimensions.get('window').height - 200
                                            }}
                                        >
                                            <ScrollView
                                                horizontal={false}
                                                className={'flex flex-col px-3'}
                                            >
                                                {languages?.data?.length && languages?.data?.map((language: Language, index: number) => (
                                                    <Pressable
                                                        key={index}
                                                        onPress={() => {
                                                            const items = value?.map((item: Language) => item.uuid).includes(language.uuid)
                                                                ? value.filter((item: Language) => item.uuid !== language.uuid)
                                                                : [...value, language];

                                                            onChange(items);
                                                        }}
                                                        className={'flex flex-row items-center justify-between mb-2'}
                                                    >
                                                        <Text
                                                            className={'py-2'}
                                                        >{language.native_name}</Text>
                                                        <View
                                                            className={'w-4 h-4 border flex items-center justify-center'}
                                                        >
                                                            {value?.map((item: Language) => item.uuid).includes(language.uuid) && (
                                                                <View className={'w-2 h-2 bg-black'}/>
                                                            )}
                                                        </View>
                                                    </Pressable>
                                                ))}
                                            </ScrollView>
                                        </View>
                                    </>)}
                                />
                            </>
                        )}
                    />

                    <Controller
                        name={'age_restriction'}
                        control={control}
                        defaultValue={i18n.registerFormFourthStepCompany.ageRestrictions[0]}
                        render={({field: {onChange, value}}) => (
                            <>
                                <View
                                    className={'flex flex-col items-stretch mt-3'}
                                    style={'age_restriction' in errors ? {
                                        padding: 3,
                                        borderColor: 'red',
                                        borderWidth: 1
                                    } : {}}
                                >
                                    <Text
                                        className={'text-xs'}
                                    >{i18n.registerFormFourthStepCompany.ageRestriction}</Text>
                                    <Pressable
                                        onPress={() => setIsAgeRestrictionsModalOpen(true)}
                                        className={'mt-1 block w-full border border-gray-300 rounded-md px-2 py-3 flex flex-row items-center justify-between'}
                                    >
                                        <Text>{value?.length ? value : i18n.registerFormFourthStepCompany.ageRestriction}</Text>
                                        <Ionicons name={'chevron-down'} size={18} color={'black'}/>
                                    </Pressable>
                                </View>
                                <ModalComponent
                                    isVisible={isAgeRestrictionsModalOpen}
                                    setIsVisible={setIsAgeRestrictionsModalOpen}
                                    title={i18n.registerFormFourthStepCompany.ageRestriction}
                                    content={(<>
                                        <View
                                            className={'flex flex-col'}
                                            style={{
                                                maxHeight: Dimensions.get('window').height - 200
                                            }}
                                        >
                                            <ScrollView
                                                horizontal={false}
                                                className={'flex flex-col px-3'}
                                            >
                                                {i18n.registerFormFourthStepCompany.ageRestrictions.map((ageRestriction: string, index: number) => (
                                                    <Pressable
                                                        key={index}
                                                        onPress={() => {
                                                            onChange(ageRestriction);
                                                            setIsAgeRestrictionsModalOpen(false);
                                                        }}
                                                        className={'flex flex-row items-center justify-between mb-2'}
                                                    >
                                                        <Text
                                                            className={'py-2'}
                                                        >{ageRestriction}</Text>
                                                        <View
                                                            className={'w-4 h-4 border rounded-full flex items-center justify-center'}
                                                        >
                                                            {value === ageRestriction && (
                                                                <View className={'w-2 h-2 rounded-full bg-black'}/>
                                                            )}
                                                        </View>
                                                    </Pressable>
                                                ))}
                                            </ScrollView>
                                        </View>
                                    </>)}
                                />
                            </>
                        )}
                    />

                    <Controller
                        name={'preferred_contacts'}
                        control={control}
                        defaultValue={[]}
                        render={({field: {onChange, value}}) => (
                            <>
                                <View
                                    className={'flex flex-col items-stretch mt-3'}
                                    style={'preferred_contacts' in errors ? {
                                        padding: 3,
                                        borderColor: 'red',
                                        borderWidth: 1
                                    } : {}}
                                >
                                    <Text
                                        className={'text-xs'}
                                    >{i18n.registerFormFourthStepCompany.preferredContact}</Text>
                                    <Pressable
                                        onPress={() => setIsPreferredContactsModalOpen(true)}
                                        className={'mt-1 block w-full border border-gray-300 rounded-md px-2 py-3 flex flex-row items-center justify-between'}
                                    >
                                        <Text>{value?.length ? value?.join(', ') : i18n.registerFormFourthStepCompany.preferredContact}</Text>
                                        <Ionicons name={'chevron-down'} size={18} color={'black'}/>
                                    </Pressable>
                                </View>
                                <ModalComponent
                                    isVisible={isPreferredContactsModalOpen}
                                    setIsVisible={setIsPreferredContactsModalOpen}
                                    title={i18n.registerFormFourthStepCompany.preferredContact}
                                    content={(<>
                                        <View
                                            className={'flex flex-col'}
                                            style={{
                                                maxHeight: Dimensions.get('window').height - 200
                                            }}
                                        >
                                            <ScrollView
                                                horizontal={false}
                                                className={'flex flex-col px-3'}
                                            >
                                                {i18n.registerFormFourthStepCompany.preferredContacts.map((element: string, index: number) => (
                                                    <Pressable
                                                        key={index}
                                                        onPress={() => {
                                                            const elementItems = value.includes(element)
                                                                ? value.filter((eventElement: string) => eventElement !== element)
                                                                : [...value, element];

                                                            onChange(elementItems);
                                                        }}
                                                        className={'flex flex-row items-center justify-between mb-2'}
                                                    >
                                                        <Text
                                                            className={'py-2'}
                                                        >{element}</Text>
                                                        <View
                                                            className={'w-4 h-4 border flex items-center justify-center'}
                                                        >
                                                            {value.includes(element) && (
                                                                <View className={'w-2 h-2 bg-black'}/>
                                                            )}
                                                        </View>
                                                    </Pressable>
                                                ))}
                                            </ScrollView>
                                        </View>
                                    </>)}
                                />
                            </>
                        )}
                    />

                    <Controller
                        name={'other_info'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'other_info' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs'}
                                >{i18n.registerFormFourthStepCompany.otherInfo}</Text>
                                <TextInput
                                    className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                    value={value}
                                    placeholder={i18n.registerFormFourthStepCompany.otherInfo}
                                    onChange={(e) => onChange(e.nativeEvent.text)}
                                    autoCorrect={false}
                                    multiline={true}
                                    numberOfLines={6}
                                    style={{
                                        textAlignVertical: 'top',
                                        paddingTop: 8,
                                        paddingBottom: 8,
                                        minHeight: 100
                                    }}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'service_time'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'service_time' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs'}
                                >{i18n.registerFormFourthStepCompany.serviceTime}</Text>
                                <TextInput
                                    className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                    value={value}
                                    placeholder={i18n.registerFormFourthStepCompany.serviceTime}
                                    onChange={(e) => onChange(e.nativeEvent.text)}
                                    autoCorrect={false}
                                    multiline={true}
                                    numberOfLines={6}
                                    style={{
                                        textAlignVertical: 'top',
                                        paddingTop: 8,
                                        paddingBottom: 8,
                                        minHeight: 100
                                    }}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'materials'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'materials' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs'}
                                >{i18n.registerFormFourthStepCompany.materials}</Text>
                                <TextInput
                                    className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                    value={value}
                                    placeholder={i18n.registerFormFourthStepCompany.materials}
                                    onChange={(e) => onChange(e.nativeEvent.text)}
                                    autoCorrect={false}
                                    multiline={true}
                                    numberOfLines={6}
                                    style={{
                                        textAlignVertical: 'top',
                                        paddingTop: 8,
                                        paddingBottom: 8,
                                        minHeight: 100
                                    }}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'aim'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'aim' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs'}
                                >{i18n.registerFormFourthStepCompany.aim}</Text>
                                <TextInput
                                    className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                    value={value}
                                    placeholder={i18n.registerFormFourthStepCompany.aim}
                                    onChange={(e) => onChange(e.nativeEvent.text)}
                                    autoCorrect={false}
                                    multiline={true}
                                    numberOfLines={6}
                                    style={{
                                        textAlignVertical: 'top',
                                        paddingTop: 8,
                                        paddingBottom: 8,
                                        minHeight: 100
                                    }}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'style'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'style' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs'}
                                >{i18n.registerFormFourthStepCompany.style}</Text>
                                <TextInput
                                    className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                    value={value}
                                    placeholder={i18n.registerFormFourthStepCompany.style}
                                    onChange={(e) => onChange(e.nativeEvent.text)}
                                    autoCorrect={false}
                                    multiline={true}
                                    numberOfLines={6}
                                    style={{
                                        textAlignVertical: 'top',
                                        paddingTop: 8,
                                        paddingBottom: 8,
                                        minHeight: 100
                                    }}
                                />
                            </View>
                        )}
                    />

                </View>
            </ScrollView>
            <View
                className="mt-auto flex flex-col justify-between items-center border-gray-300 border-t pt-2 pb-6 px-3"
                style={{
                    height: '13%'
                }}
            >
                <TouchableOpacity
                    onPress={() => setCurrentStep(5)}
                    className={'flex flex-row items-center justify-center bg-primary rounded-md w-full mb-3'}
                >
                    <Text
                        className={`text-base font-bold py-3 px-3 text-center text-white`}>
                        Dalej
                    </Text>
                </TouchableOpacity>
            </View>

            <ModalComponent
                isVisible={isHelpModalOpen}
                setIsVisible={setIsHelpModalOpen}
                title={helpModalTitle}
                content={(
                    <View className="flex-col">
                        <Text>
                            {helpModalContent}
                        </Text>
                    </View>
                )}
            />
        </View>
    );
};