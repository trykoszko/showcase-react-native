import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import i18n from '../../i18n';
import Ionicons from '@expo/vector-icons/Ionicons';
import {shareApp} from '../../helpers/share';

type Props = {
    setCurrentStep: (step: number) => void
}

export const RegisterFormFifthStepCompany = ({
                                                 setCurrentStep
                                             }: Props) => {
    return (
        <View
            className={'flex flex-col justify-between items-stretch h-full relative'}
        >
            <View
                className={`flex flex-row justify-between items-center pt-2 pb-6 px-3`}
                style={{
                    height: '8%',
                    zIndex: 100
                }}
            >
                <TouchableOpacity
                    onPress={() => setCurrentStep(4)}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
                <Text className="text-sm font-bold">{i18n.registerFormFifthStepCompany.title}</Text>
                <View style={{width: 24}}/>
            </View>
            <View
                className={'w-full flex flex-row items-center justify-center h-1'}
            >
                <View
                    className={'w-11/12 border-gray-300 border-b h-1'}
                />
            </View>
            <ScrollView
                scrollEventThrottle={16}
                style={{
                    height: '79%'
                }}
            >
                <View
                    className="flex flex-col justify-start grow p-3"
                >
                    <Text
                        className={'mb-10'}
                    >{i18n.registerFormFifthStepCompany.text}</Text>

                    <TouchableOpacity
                        onPress={() => {
                            shareApp();
                        }}
                        className="border border-gray-300 p-3 rounded mb-4"
                    >
                        <View className="flex flex-row items-center justify-start">
                            <Ionicons name="people" size={20} color="black"/>
                            <Text className="ml-3">{i18n.registerFormFifthStepCompany.inviteFriends}</Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </ScrollView>
            <View
                className="mt-auto flex flex-col justify-between items-center border-gray-300 border-t pt-2 pb-6 px-3"
                style={{
                    height: '13%'
                }}
            >
                <TouchableOpacity
                    onPress={() => {
                        setCurrentStep(6);
                    }}
                    className={'flex flex-row items-center justify-center bg-primary rounded-md w-full mb-3'}
                >
                    <Text
                        className={`text-base font-bold py-3 px-3 text-center text-white`}>
                        {i18n.registerFormFifthStepCompany.skip}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};