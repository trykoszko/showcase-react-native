import {
    Dimensions,
    KeyboardAvoidingView,
    Platform,
    Pressable,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View
} from 'react-native';
import React, {useState} from 'react';
import i18n from '../../i18n';
import Ionicons from '@expo/vector-icons/Ionicons';
import {Link, router} from 'expo-router';
import {ModalComponent} from '../modal/Modal';
import {useQuery} from "react-query";
import {AxiosResponse} from "axios";
import {City} from "../../types/City.type";
import {Controller} from "react-hook-form";
import RNDateTimePicker from "@react-native-community/datetimepicker";
import {getFormattedDateString} from "../../helpers/date-helpers";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {ImagePickerButton} from "../image-picker/ImagePicker";
import {usePublicAxios} from "../../hooks/usePublicAxios";

type Props = {
    control: any,
    errors: any,
    watch: any,
    setCurrentStep: (step: number) => void
}

export const RegisterFormSecondStepPrivate = ({
                                                  control,
                                                  watch,
                                                  errors,
                                                  setCurrentStep
                                              }: Props) => {
    const axios = usePublicAxios();
    const [isLocationModalOpen, setIsLocationModalOpen] = useState<boolean>(false);
    const [isPasswordHidden, setIsPasswordHidden] = useState<boolean>(true);
    const [datepickerVisible, setDatepickerVisible] = useState<boolean>(false)

    const maximumBirthdate: Date = new Date((new Date()).setFullYear((new Date()).getFullYear() - 18));

    const {data: cities, error, isFetching} = useQuery<unknown, unknown, { data: City[] }>({
        queryKey: ['cities'],
        queryFn: async () => {
            try {
                const res: AxiosResponse = await axios.get(`public/city`)

                return res?.data ?? null;
            } catch (e) {
                console.log('cities exception', e)
            }
        }
    });

    const {height} = Dimensions.get('window');

    const watchFields = watch(['first_name', 'last_name', 'city', 'birthdate', 'email', 'password']);
    const isNextButtonDisabled = watchFields.some((field: any) => !field);

    return (
        <View
            className={'flex flex-col justify-between items-stretch h-full relative'}
        >
            <View
                className={`flex flex-row justify-between items-center pt-2 pb-6 px-3`}
                style={{
                    height: height * 0.08,
                    zIndex: 100
                }}
            >
                <TouchableOpacity
                    onPress={() => setCurrentStep(1)}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
                <Text className="text-sm font-bold">{i18n.registerFormSecondStepPrivate.title}</Text>
                <View style={{width: 24}}/>
            </View>
            <View
                className={'w-full flex flex-row items-center justify-center h-1'}
            >
                <View
                    className={'w-11/12 border-gray-300 border-b h-1'}
                />
            </View>
            <View
                className={"flex flex-col justify-start grow"}
                style={{
                    height: '92%'
                }}
            >
                <KeyboardAvoidingView
                    behavior={'padding'}
                    keyboardVerticalOffset={height * 0.13}
                    style={{
                        flex: 1
                    }}
                >
                    <ScrollView
                        scrollEventThrottle={16}
                        horizontal={false}
                        style={{
                            flex: 1
                        }}
                    >
                        <View className="flex flex-col justify-start grow p-3">
                            <Text
                                className={'mb-5'}
                            >{i18n.registerFormSecondStepPrivate.text}</Text>

                            <Controller
                                name={'first_name'}
                                control={control}
                                render={({field: {onChange, value}}) => (
                                    <View
                                        className={'flex flex-col items-stretch'}
                                        style={'first_name' in errors ? {
                                            padding: 3,
                                            borderColor: 'red',
                                            borderWidth: 1
                                        } : {}}
                                    >
                                        <Text
                                            className={'text-xs'}
                                        >{i18n.registerFormSecondStepPrivate.name}</Text>
                                        <TextInput
                                            className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                            value={value}
                                            placeholder={i18n.registerFormSecondStepPrivate.name}
                                            onChange={(e) => onChange(e.nativeEvent.text)}
                                            autoCorrect={false}
                                        />
                                    </View>
                                )}
                            />

                            <Controller
                                name={'last_name'}
                                control={control}
                                render={({field: {onChange, value}}) => (
                                    <View
                                        className={'flex flex-col items-stretch mt-3'}
                                        style={'last_name' in errors ? {
                                            padding: 3,
                                            borderColor: 'red',
                                            borderWidth: 1
                                        } : {}}
                                    >
                                        <Text
                                            className={'text-xs'}
                                        >{i18n.registerFormSecondStepPrivate.lastName}</Text>
                                        <TextInput
                                            className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                            value={value}
                                            placeholder={i18n.registerFormSecondStepPrivate.lastName}
                                            onChange={(e) => onChange(e.nativeEvent.text)}
                                            autoCorrect={false}
                                        />
                                    </View>
                                )}
                            />

                            <Controller
                                name={'city'}
                                control={control}
                                render={({field: {onChange, value}}) => (
                                    <>
                                        <View
                                            className={'flex flex-col items-stretch mt-3'}
                                            style={'city' in errors ? {
                                                padding: 3,
                                                borderColor: 'red',
                                                borderWidth: 1
                                            } : {}}
                                        >
                                            <Text
                                                className={'text-xs'}
                                            >{i18n.registerFormSecondStepPrivate.location}</Text>
                                            <Pressable
                                                onPress={() => setIsLocationModalOpen(true)}
                                                className={'mt-1 block w-full border border-gray-300 rounded-md px-2 py-3 flex flex-row items-center justify-between'}
                                            >
                                                <Text>{value
                                                    ? (
                                                        cities?.data?.find(city => city.uuid === value?.uuid)?.name
                                                    )
                                                    : i18n.registerFormSecondStepPrivate.location}</Text>
                                                <Ionicons name={'chevron-down'} size={18} color={'black'}/>
                                            </Pressable>
                                        </View>
                                        <ModalComponent
                                            isVisible={isLocationModalOpen}
                                            setIsVisible={setIsLocationModalOpen}
                                            title={i18n.registerFormSecondStepPrivate.pickLocation}
                                            content={(<View className={"px-1"}>
                                                {cities?.data && cities?.data?.map((city: City, index: number) => (
                                                    <TouchableOpacity
                                                        key={index}
                                                        onPress={() => {
                                                            onChange(city);
                                                            setIsLocationModalOpen(false);
                                                        }}
                                                        className={'flex flex-row items-center justify-between py-2'}
                                                    >
                                                        <Text
                                                            className={'py-2'}
                                                        >{city.name}, {city.country.native_name}</Text>
                                                        <View
                                                            className={'w-4 h-4 border rounded-full flex items-center justify-center'}
                                                        >
                                                            {value?.uuid === city.uuid && (
                                                                <View className={'w-2 h-2 rounded-full bg-black'}/>
                                                            )}
                                                        </View>
                                                    </TouchableOpacity>
                                                ))}
                                            </View>)}
                                        />
                                    </>
                                )}
                            />

                            <Controller
                                name={'birthdate'}
                                control={control}
                                render={({field: {onChange, value}}) => (
                                    <View
                                        className={'flex flex-col items-stretch mt-3'}
                                        style={'birthdate' in errors ? {
                                            padding: 3,
                                            borderColor: 'red',
                                            borderWidth: 1
                                        } : {}}
                                    >
                                        <Text
                                            className={'text-xs mb-1'}
                                        >{i18n.registerFormSecondStepPrivate.birthdate}</Text>
                                        {Platform.OS === 'ios' ? (
                                            <RNDateTimePicker
                                                value={value ? new Date(value) : new Date()}
                                                maximumDate={maximumBirthdate}
                                                mode="date"
                                                display="default"
                                                onChange={(e, date) => {
                                                    if (date) {
                                                        onChange(date);
                                                    }
                                                }}
                                            />
                                        ) : (
                                            <>
                                                <View
                                                    className={"flex flex-row items-center justify-stretch"}
                                                >
                                                    <TouchableOpacity
                                                        onPress={() => setDatepickerVisible(true)}
                                                        className={'border border-gray-300 rounded-md px-3 py-3 mr-3 w-full'}
                                                    >
                                                        <Text>{getFormattedDateString(value ? new Date(value) : new Date())}</Text>
                                                    </TouchableOpacity>
                                                </View>

                                                <DateTimePickerModal
                                                    isVisible={datepickerVisible}
                                                    mode="date"
                                                    locale="pl"
                                                    is24Hour={true}
                                                    date={value ? new Date(value) : new Date()}
                                                    maximumDate={maximumBirthdate}
                                                    onConfirm={(val) => {
                                                        if (val) {
                                                            onChange(val);
                                                        }
                                                        setDatepickerVisible(false)
                                                    }}
                                                    onCancel={() => {
                                                        setDatepickerVisible(false)
                                                    }}
                                                />
                                            </>
                                        )}
                                    </View>
                                )}
                            />

                            <Controller
                                name={'email'}
                                control={control}
                                render={({field: {onChange, value}}) => (
                                    <View
                                        className={'flex flex-col items-stretch mt-3'}
                                        style={'email' in errors ? {
                                            padding: 3,
                                            borderColor: 'red',
                                            borderWidth: 1
                                        } : {}}
                                    >
                                        <Text
                                            className={'text-xs'}
                                        >{i18n.registerFormSecondStepPrivate.email}</Text>
                                        <TextInput
                                            className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                            value={value}
                                            placeholder={i18n.registerFormSecondStepPrivate.email}
                                            onChange={(e) => onChange(e.nativeEvent.text)}
                                            autoCapitalize={"none"}
                                            keyboardType={"email-address"}
                                        />
                                    </View>
                                )}
                            />

                            <Controller
                                name={'password'}
                                control={control}
                                render={({field: {onChange, value}}) => (
                                    <View
                                        className={'flex flex-col items-stretch mt-3'}
                                        style={'password' in errors ? {
                                            padding: 3,
                                            borderColor: 'red',
                                            borderWidth: 1
                                        } : {}}
                                    >
                                        <Text
                                            className={'text-xs'}
                                        >{i18n.registerFormSecondStepPrivate.password}</Text>
                                        <View
                                            className={'mt-1 block w-full border border-gray-300 rounded-md px-2 py-3 flex flex-row items-center justify-between'}
                                        >
                                            <TextInput
                                                secureTextEntry={isPasswordHidden}
                                                value={value}
                                                autoCapitalize={'none'}
                                                placeholder={i18n.registerFormSecondStepPrivate.password}
                                                onChange={(e) => onChange(e.nativeEvent.text)}
                                                className={'w-9/12'}
                                            />
                                            <Pressable
                                                onPress={() => setIsPasswordHidden(!isPasswordHidden)}
                                                className={'w-3/12'}
                                            >
                                                <Text
                                                    className={'text-xs underline text-right'}
                                                >{isPasswordHidden
                                                    ? i18n.registerFormSecondStepPrivate.showPassword
                                                    : i18n.registerFormSecondStepPrivate.hidePassword}</Text>
                                            </Pressable>
                                        </View>
                                    </View>
                                )}
                            />

                            <Controller
                                name={'image'}
                                control={control}
                                render={({field: {onChange, value}}) => (
                                    <View
                                        className={'flex flex-col items-stretch mt-3'}
                                        style={'image' in errors ? {
                                            padding: 3,
                                            borderColor: 'red',
                                            borderWidth: 1
                                        } : {}}
                                    >
                                        <Text
                                            className={'text-xs mb-1'}
                                        >{i18n.registerFormSecondStepPrivate.image}</Text>
                                        <ImagePickerButton
                                            value={value}
                                            resetField={() => onChange(null)}
                                            setValue={(value) => onChange(value?.assets ? value?.assets[0]?.uri : null)}
                                            w={64}
                                            h={64}
                                            isRounded={true}
                                        />
                                    </View>
                                )}
                            />

                            <Text
                                className={'mt-3 mb-5'}
                            >
                                {i18n.registerFormSecondStepPrivate.bottomTextText}
                                <Link href={i18n.registerFormSecondStepPrivate.bottomTextTermsLink}>
                                    <Text
                                        className={'underline'}
                                    >{i18n.registerFormSecondStepPrivate.bottomTextTermsText}</Text>
                                </Link>, &nbsp;
                                <Link href={i18n.registerFormSecondStepPrivate.bottomTextPaymentLink}>
                                    <Text
                                        className={'underline'}
                                    >{i18n.registerFormSecondStepPrivate.bottomTextPaymentText}</Text>
                                </Link>, &nbsp;
                                <Link href={i18n.registerFormSecondStepPrivate.bottomTextPrivacyLink}>
                                    <Text
                                        className={'underline'}
                                    >{i18n.registerFormSecondStepPrivate.bottomTextPrivacyText}</Text>
                                </Link>
                            </Text>

                        </View>
                    </ScrollView>
                    <View
                        className="flex flex-col justify-between items-center border-gray-300 border-t pt-2 pb-6 px-3"
                        style={{
                            height: height * 0.16
                        }}
                    >
                        <Pressable
                            onPress={() => {
                                if (isNextButtonDisabled) {
                                    return
                                }
                                setCurrentStep(3)
                            }}
                            className={`flex flex-row items-center justify-center rounded-md w-full ${isNextButtonDisabled ? 'bg-gray-500' : 'bg-primary'}`}
                        >
                            <Text
                                className={`text-base font-bold py-3 px-3 text-center text-white`}>
                                {i18n.registerFormSecondStepPrivate.join}
                            </Text>
                        </Pressable>
                        <Pressable
                            onPress={() => router.push('/authentication')}
                            className={'flex flex-row items-center justify-center rounded-md w-full mb-3'}
                        >
                            <Text
                                className={`text-base py-1 px-3 text-center`}>
                                Masz już konto? <Text
                                className={'underline'}
                            >{i18n.registerFormSecondStepPrivate.logIn}</Text>
                            </Text>
                        </Pressable>
                    </View>
                </KeyboardAvoidingView>
            </View>
        </View>
    );
};
