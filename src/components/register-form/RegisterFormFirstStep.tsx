import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import React from 'react';
import i18n from '../../i18n';
import {router} from 'expo-router';
import FontAwesome from '@expo/vector-icons/FontAwesome';

type Props = {
    setFormType: (type: string) => void,
    setCurrentStep: (step: number) => void
}

export const RegisterFormFirstStep = ({setFormType, setCurrentStep}: Props) =>
{
    return (
        <View
            className={'flex flex-col justify-between items-stretch h-full relative'}
        >
            <View
                className={`flex flex-row justify-between items-center pt-2 pb-6 px-3`}
                style={{
                    height: '8%',
                    zIndex: 100
                }}
            >
                <TouchableOpacity
                    onPress={() => router.back()}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
                <Text className="text-sm font-bold">{i18n.registerFormFirstStep.title}</Text>
                <View style={{width: 24}}/>
            </View>
            <View
                className={'w-full flex flex-row items-center justify-center h-1'}
            >
                <View
                    className={'w-11/12 border-gray-300 border-b h-1'}
                />
            </View>
            <ScrollView
                scrollEventThrottle={16}
                className={'p-3'}
                style={{
                    height: '92%'
                }}
            >
                <View
                    className={'w-full h-full flex flex-col justify-center'}
                >
                    <Text
                        className={'mb-10'}
                    >{i18n.registerFormFirstStep.text}</Text>

                    <TouchableOpacity
                        onPress={() =>
                        {
                            setFormType('private');
                            setCurrentStep(2);
                        }}
                        className={'mt-3'}
                    >
                        <View
                            className={`border rounded-lg flex flex-row items-center justify-between p-4 border-gray-200`}
                        >
                            <View className={'flex flex-row items-center'}>
                                <Ionicons name={'person-outline'} size={20} color={'black'}/>
                                <Text className={'ml-3'}>{i18n.registerFormFirstStep.private}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() =>
                        {
                            setFormType('company');
                            setCurrentStep(2);
                        }}
                        className={'mt-3'}
                    >
                        <View
                            className={`border rounded-lg flex flex-row items-center justify-between p-4 border-gray-200`}
                        >
                            <View className={'flex flex-row items-center'}>
                                <FontAwesome name={'building-o'} size={20} color={'black'}/>
                                <Text className={'ml-3'}>{i18n.registerFormFirstStep.company}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>

                </View>
            </ScrollView>
        </View>
    );
};
