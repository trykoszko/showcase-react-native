import {Alert, Dimensions, Pressable, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import * as Device from 'expo-device';
import React, {useState} from 'react';
import i18n from '../../i18n';
import Ionicons from '@expo/vector-icons/Ionicons';
import {ModalComponent} from '../modal/Modal';
import {Image} from 'expo-image';
import {EventElement} from "../../types/EventElement.type";
import {EventElementComponent} from "../event-element-component/EventElementComponent";
import {Controller} from "react-hook-form";
import {useQuery} from "react-query";
import {AxiosResponse} from "axios";
import {usePublicAxios} from "../../hooks/usePublicAxios";
import appleAuth from "@invertase/react-native-apple-authentication";

const coorganizeImage = require('../../../assets/coorganize.png');

type Props = {
    control: any,
    errors: any,
    watch: any,
    setCurrentStep: (step: number) => void
}

export const RegisterFormThirdStepCompany = ({
                                                 control,
                                                 errors,
                                                 watch,
                                                 setCurrentStep
                                             }: Props) => {
    const axios = usePublicAxios();
    const [isAttributesModalOpen, setIsAttributesModalOpen] = useState<boolean>(false);

    const {data: elements, error, isFetching} = useQuery<unknown, unknown, { data: EventElement[] }>({
        queryKey: ['event_elements'],
        queryFn: async () => {
            try {
                const res: AxiosResponse = await axios.get(`public/event-element?limit=100`)

                return res?.data ?? null;
            } catch (e) {
                console.log('event_elements exception', e)
            }
        }
    });

    const watchFields = watch(['elements']);
    const isNextButtonDisabled = watchFields.some((field: any) => !field?.length);

    return (
        <View
            className={'flex flex-col justify-between items-stretch h-full relative'}
        >
            <View
                className={`flex flex-row justify-between items-center pt-2 pb-6 px-3`}
                style={{
                    height: '8%',
                    zIndex: 100
                }}
            >
                <TouchableOpacity
                    onPress={() => setCurrentStep(2)}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
                <Text className="text-sm font-bold">{i18n.registerFormThirdStepCompany.title}</Text>
                <View style={{width: 24}}/>
            </View>
            <View
                className={'w-full flex flex-row items-center justify-center h-1'}
            >
                <View
                    className={'w-11/12 border-gray-300 border-b h-1'}
                />
            </View>
            <ScrollView
                scrollEventThrottle={16}
                style={{
                    height: '79%'
                }}
            >
                <View className="flex flex-col justify-start grow p-3">
                    <Text
                        className={'mb-5'}
                    >{i18n.registerFormThirdStepCompany.text}</Text>

                    <View
                        className={'py-6'}
                    >
                        <Image
                            source={coorganizeImage}
                            style={{
                                width: 106,
                                height: 123,
                                alignSelf: 'center'
                            }}
                        />
                    </View>

                    <Controller
                        name={'elements'}
                        control={control}
                        defaultValue={[]}
                        render={({field: {onChange, value}}) => (
                            <>
                                <View
                                    className={'flex flex-col items-stretch mt-3'}
                                    style={'elements' in errors ? {
                                        padding: 3,
                                        borderColor: 'red',
                                        borderWidth: 1
                                    } : {}}
                                >
                                    <Text
                                        className={'text-xs'}
                                    >{i18n.registerFormThirdStepCompany.chooseAttributes}</Text>
                                    <Pressable
                                        onPress={() => setIsAttributesModalOpen(true)}
                                        className={'mt-1 block w-full border border-gray-300 rounded-md px-2 py-3 flex flex-row items-center justify-between'}
                                    >
                                        <Text>{i18n.registerFormThirdStepCompany.addAttributes}</Text>
                                        <Ionicons name={'chevron-down'} size={18} color={'black'}/>
                                    </Pressable>
                                    {value.length > 0 && (
                                        <View
                                            className={'flex flex-row flex-wrap mt-3'}
                                        >
                                            {value.map((eventElement: EventElement, index: number) => (
                                                <Pressable
                                                    key={index}
                                                    className={'bg-primary rounded-full px-3 py-2 mr-2 mb-2 flex-row'}
                                                    onPress={() => {
                                                        const eventElementItems = value.filter((element: EventElement) => element !== eventElement);

                                                        onChange(eventElementItems);
                                                    }}
                                                >
                                                    <Text
                                                        className={'text-white mr-2'}
                                                    >{eventElement.name}</Text>
                                                    <Ionicons name={'close'} size={16} color={'white'}/>
                                                </Pressable>
                                            ))}
                                        </View>
                                    )}
                                </View>
                                <ModalComponent
                                    isVisible={isAttributesModalOpen}
                                    setIsVisible={setIsAttributesModalOpen}
                                    title={i18n.registerFormThirdStepCompany.chooseAttributes}
                                    content={(<>
                                        <ScrollView
                                            horizontal={false}
                                            style={{
                                                maxHeight: Dimensions.get('window').height - 200
                                            }}
                                        >
                                            {elements?.data && elements?.data?.map((element: EventElement, index: number) => (
                                                <Pressable
                                                    key={index}
                                                    onPress={() => {
                                                        const elementItems = value.map((item: EventElement) => item.uuid).includes(element.uuid)
                                                            ? value.filter((eventElement: EventElement) => eventElement.uuid !== element.uuid)
                                                            : [...value, element];

                                                        onChange(elementItems);
                                                    }}
                                                    className={'flex flex-row items-center justify-between mb-2'}
                                                >
                                                    <View>
                                                        <EventElementComponent element={element}
                                                                               selected={value.map((item: EventElement) => item.uuid).includes(element.uuid)}/>
                                                    </View>
                                                </Pressable>
                                            ))}
                                        </ScrollView>
                                    </>)}
                                />
                            </>
                        )}
                    />

                    <View
                        className={'flex flex-col items-stretch mt-5'}
                    >
                        <Text
                            className={'text-md mb-2'}
                        >{i18n.registerFormThirdStepCompany.yourPortfolio}</Text>

                        <TouchableOpacity
                            onPress={() => {
                                Alert.alert('Clicked');
                            }}
                            className="border border-gray-300 p-3 rounded mb-4"
                        >
                            <View className="flex flex-row items-center justify-between">
                                <Ionicons name="logo-facebook" size={20} color="black"/>
                                <Text className="ml-3">{i18n.registerFormThirdStepCompany.facebook}</Text>
                                <View
                                    className={'w-8'}
                                />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                Alert.alert('Clicked');
                            }}
                            className="border border-gray-300 p-3 rounded mb-4"
                        >
                            <View className="flex flex-row items-center justify-between">
                                <Ionicons name="logo-google" size={20} color="black"/>
                                <Text className="ml-3">{i18n.registerFormThirdStepCompany.google}</Text>
                                <View
                                    className={'w-8'}
                                />
                            </View>
                        </TouchableOpacity>

                        {(Device.brand === 'Apple' && appleAuth.isSupported) && (
                            <TouchableOpacity
                                onPress={() => {
                                    Alert.alert('Clicked');
                                }}
                                className="border border-gray-300 p-3 rounded mb-4"
                            >
                                <View className="flex flex-row items-center justify-between">
                                    <Ionicons name="logo-apple" size={20} color="black"/>
                                    <Text className="ml-3">{i18n.registerFormThirdStepCompany.apple}</Text>
                                    <View
                                        className={'w-8'}
                                    />
                                </View>
                            </TouchableOpacity>
                        )}

                        <TouchableOpacity
                            onPress={() => {
                                Alert.alert('Clicked');
                            }}
                            className="border border-gray-300 p-3 rounded mb-4"
                        >
                            <View className="flex flex-row items-center justify-between">
                                <Ionicons name="logo-linkedin" size={20} color="black"/>
                                <Text className="ml-3">{i18n.registerFormThirdStepCompany.google}</Text>
                                <View
                                    className={'w-8'}
                                />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity
                            onPress={() => {
                                Alert.alert('Clicked');
                            }}
                            className="border border-gray-300 p-3 rounded mb-4"
                        >
                            <View className="flex flex-row items-center justify-between">
                                <View
                                    className={'w-4'}
                                />
                                <Text className="ml-3">{i18n.registerFormThirdStepCompany.addOther}</Text>
                                <View
                                    className={'w-8'}
                                />
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
            </ScrollView>
            <View
                className="mt-auto flex flex-col justify-between items-center border-gray-300 border-t pt-2 pb-6 px-3"
                style={{
                    height: '13%'
                }}
            >
                <TouchableOpacity
                    onPress={() => setCurrentStep(4)}
                    className={'flex flex-row items-center justify-center bg-primary rounded-md w-full mb-3'}
                >
                    <Text
                        className={`text-base font-bold py-3 px-3 text-center text-white`}>
                        {isNextButtonDisabled ? i18n.registerFormThirdStepCompany.skip : i18n.registerFormThirdStepCompany.next}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};