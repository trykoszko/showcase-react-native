import {Pressable, ScrollView, Text, TextInput, TouchableOpacity, View} from 'react-native';
import React, {useState} from 'react';
import i18n from '../../i18n';
import Ionicons from '@expo/vector-icons/Ionicons';
import {ModalComponent} from '../modal/Modal';
import {Link, router} from 'expo-router';
import {useQuery} from "react-query";
import {City} from "../../types/City.type";
import {AxiosResponse} from "axios";
import {Controller} from "react-hook-form";
import {ImagePickerButton} from "../image-picker/ImagePicker";
import {usePublicAxios} from "../../hooks/usePublicAxios";

type Props = {
    control: any,
    errors: any,
    watch: any,
    setCurrentStep: (step: number) => void
}

export const RegisterFormSecondStepCompany = ({
                                                  control,
                                                  errors,
                                                  watch,
                                                  setCurrentStep
                                              }: Props) => {
    const axios = usePublicAxios();
    const [isLocationModalOpen, setIsLocationModalOpen] = useState<boolean>(false);
    const [isPasswordHidden, setIsPasswordHidden] = useState<boolean>(true);

    const {data: cities} = useQuery<unknown, unknown, { data: City[] }>({
        queryKey: ['cities'],
        queryFn: async () => {
            try {
                const res: AxiosResponse = await axios.get(`public/city`)

                return res?.data ?? null;
            } catch (e) {
                console.log('cities exception', e)
            }
        }
    });

    const watchFields = watch(['company_name', 'company_description', 'city', 'email', 'password']);
    const isNextButtonDisabled = watchFields.some((field: any) => !field);

    return (
        <View
            className={'flex flex-col justify-between items-stretch h-full relative'}
        >
            <View
                className={`flex flex-row justify-between items-center pt-2 pb-6 px-3`}
                style={{
                    height: '8%',
                    zIndex: 100
                }}
            >
                <TouchableOpacity
                    onPress={() => setCurrentStep(1)}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
                <Text className="text-sm font-bold">{i18n.registerFormSecondStepCompany.title}</Text>
                <View style={{width: 24}}/>
            </View>
            <View
                className={'w-full flex flex-row items-center justify-center h-1'}
            >
                <View
                    className={'w-11/12 border-gray-300 border-b h-1'}
                />
            </View>
            <ScrollView
                scrollEventThrottle={16}
                style={{
                    height: '73%'
                }}
            >
                <View className="flex flex-col justify-start grow p-3">
                    <Text
                        className={'mb-5'}
                    >{i18n.registerFormSecondStepCompany.text}</Text>

                    <Controller
                        name={'company_name'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'company_name' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs'}
                                >{i18n.registerFormSecondStepCompany.companyName}</Text>
                                <TextInput
                                    className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                    value={value}
                                    placeholder={i18n.registerFormSecondStepCompany.companyName}
                                    onChange={(e) => onChange(e.nativeEvent.text)}
                                    autoCorrect={false}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'company_description'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'company_description' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs'}
                                >{i18n.registerFormSecondStepCompany.description}</Text>
                                <TextInput
                                    className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                    value={value}
                                    placeholder={i18n.registerFormSecondStepCompany.description}
                                    onChange={(e) => onChange(e.nativeEvent.text)}
                                    autoCorrect={false}
                                    multiline={true}
                                    numberOfLines={6}
                                    style={{
                                        textAlignVertical: 'top',
                                        paddingTop: 8,
                                        paddingBottom: 8,
                                        minHeight: 100
                                    }}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'city'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <>
                                <View
                                    className={'flex flex-col items-stretch mt-3'}
                                    style={'city' in errors ? {
                                        padding: 3,
                                        borderColor: 'red',
                                        borderWidth: 1
                                    } : {}}
                                >
                                    <Text
                                        className={'text-xs'}
                                    >{i18n.registerFormSecondStepCompany.location}</Text>
                                    <Pressable
                                        onPress={() => setIsLocationModalOpen(true)}
                                        className={'mt-1 block w-full border border-gray-300 rounded-md px-2 py-3 flex flex-row items-center justify-between'}
                                    >
                                        <Text>{value
                                            ? (
                                                cities?.data?.find(city => city.uuid === value?.uuid)?.name
                                            )
                                            : i18n.registerFormSecondStepCompany.location}</Text>
                                        <Ionicons name={'chevron-down'} size={18} color={'black'}/>
                                    </Pressable>
                                </View>
                                <ModalComponent
                                    isVisible={isLocationModalOpen}
                                    setIsVisible={setIsLocationModalOpen}
                                    title={i18n.registerFormSecondStepCompany.pickLocation}
                                    content={(<View className={"px-1"}>
                                        {cities?.data && cities?.data?.map((city: City, index: number) => (
                                            <TouchableOpacity
                                                key={index}
                                                onPress={() => {
                                                    onChange(city);
                                                    setIsLocationModalOpen(false);
                                                }}
                                                className={'flex flex-row items-center justify-between py-2'}
                                            >
                                                <Text
                                                    className={'py-2'}
                                                >{city.name}, {city.country.native_name}</Text>
                                                <View
                                                    className={'w-4 h-4 border rounded-full flex items-center justify-center'}
                                                >
                                                    {value?.uuid === city.uuid && (
                                                        <View className={'w-2 h-2 rounded-full bg-black'}/>
                                                    )}
                                                </View>
                                            </TouchableOpacity>
                                        ))}
                                    </View>)}
                                />
                            </>
                        )}
                    />

                    <Controller
                        name={'email'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'email' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs'}
                                >{i18n.registerFormSecondStepCompany.email}</Text>
                                <TextInput
                                    className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                    value={value}
                                    placeholder={i18n.registerFormSecondStepCompany.email}
                                    onChange={(e) => onChange(e.nativeEvent.text)}
                                    autoCapitalize={"none"}
                                    keyboardType={"email-address"}
                                />
                            </View>
                        )}
                    />

                    <Controller
                        name={'password'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'password' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs'}
                                >{i18n.registerFormSecondStepCompany.password}</Text>
                                <View
                                    className={'mt-1 block w-full border border-gray-300 rounded-md px-2 py-3 flex flex-row items-center justify-between'}
                                >
                                    <TextInput
                                        secureTextEntry={isPasswordHidden}
                                        value={value}
                                        autoCapitalize={'none'}
                                        placeholder={i18n.registerFormSecondStepCompany.password}
                                        onChange={(e) => onChange(e.nativeEvent.text)}
                                        className={'w-9/12'}
                                    />
                                    <Pressable
                                        onPress={() => setIsPasswordHidden(!isPasswordHidden)}
                                        className={'w-3/12'}
                                    >
                                        <Text
                                            className={'text-xs underline text-right'}
                                        >{isPasswordHidden
                                            ? i18n.registerFormSecondStepCompany.showPassword
                                            : i18n.registerFormSecondStepCompany.hidePassword}</Text>
                                    </Pressable>
                                </View>
                            </View>
                        )}
                    />

                    <Controller
                        name={'image'}
                        control={control}
                        render={({field: {onChange, value}}) => (
                            <View
                                className={'flex flex-col items-stretch mt-3'}
                                style={'image' in errors ? {
                                    padding: 3,
                                    borderColor: 'red',
                                    borderWidth: 1
                                } : {}}
                            >
                                <Text
                                    className={'text-xs mb-1'}
                                >{i18n.registerFormSecondStepCompany.image}</Text>
                                <ImagePickerButton
                                    value={value}
                                    resetField={() => onChange(null)}
                                    setValue={(value) => onChange(value?.assets ? value?.assets[0]?.uri : null)}
                                    w={64}
                                    h={64}
                                    isRounded={true}
                                />
                            </View>
                        )}
                    />

                    <Text
                        className={'mt-3 mb-5'}
                    >
                        {i18n.registerFormSecondStepCompany.bottomTextText}
                        <Link href={i18n.registerFormSecondStepCompany.bottomTextTermsLink}>
                            <Text
                                className={'underline'}
                            >{i18n.registerFormSecondStepCompany.bottomTextTermsText}</Text>
                        </Link>, &nbsp;
                        <Link href={i18n.registerFormSecondStepCompany.bottomTextPaymentLink}>
                            <Text
                                className={'underline'}
                            >{i18n.registerFormSecondStepCompany.bottomTextPaymentText}</Text>
                        </Link>, &nbsp;
                        <Link href={i18n.registerFormSecondStepCompany.bottomTextPrivacyLink}>
                            <Text
                                className={'underline'}
                            >{i18n.registerFormSecondStepCompany.bottomTextPrivacyText}</Text>
                        </Link>
                    </Text>

                </View>
            </ScrollView>
            <View
                className="mt-auto flex flex-col justify-between items-center border-gray-300 border-t pt-2 pb-6 px-3"
                style={{
                    height: '19%'
                }}
            >
                <Pressable
                    onPress={() => {
                        if (isNextButtonDisabled) {
                            return
                        }
                        setCurrentStep(3)
                    }}
                    className={`flex flex-row items-center justify-center rounded-md w-full mb-3 ${isNextButtonDisabled ? 'bg-gray-500' : 'bg-primary'}`}
                >
                    <Text
                        className={`text-base font-bold py-3 px-3 text-center text-white`}>
                        {i18n.registerFormSecondStepCompany.join}
                    </Text>
                </Pressable>
                <Pressable
                    onPress={() => router.push('/authentication')}
                    className={'flex flex-row items-center justify-center rounded-md w-full mb-3'}
                >
                    <Text
                        className={`text-base py-3 px-3 text-center`}>
                        Masz już konto? <Text
                        className={'underline'}
                    >{i18n.registerFormSecondStepCompany.logIn}</Text>
                    </Text>
                </Pressable>
            </View>
        </View>
    );
};