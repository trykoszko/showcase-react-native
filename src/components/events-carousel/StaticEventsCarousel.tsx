import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {ImageBackground} from 'expo-image';
import Ionicons from '@expo/vector-icons/Ionicons';
import i18n from '../../i18n';
import {differenceInDays, getEventDates, isLive} from '../../helpers/date-helpers';
import {useRouter} from 'expo-router';
import {share} from '../../helpers/share';
import {apiConfig} from "../../config/variables";
import {Router} from "expo-router/build/types";
import {Event} from '../../types/Event.type';

const blackAlphaGradient = require('../../../assets/black-alpha-gradient.png');

type Props = {
    events: (Event | undefined)[],
    title?: string;
}

export const StaticEventsCarousel = ({events, title = i18n.staticEventsCarousel.title}: Props) => {
    const router: Router = useRouter();

    return (
        <View>
            <View className="flex flex-row justify-between items-center mb-3 px-3">
                <Text className="text-2xl font-bold">{title}</Text>
            </View>
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                className="mb-4"
            >
                {events?.map((event: Event | undefined, index: number) => {
                    if (!event) {
                        return <></>;
                    }

                    return (
                        <TouchableOpacity
                            key={index}
                            onPress={() =>
                                router.push({
                                    pathname: '/event/[event]',
                                    params: {
                                        event: event.uuid as string
                                    }
                                })
                            }
                            className="ml-3"
                        >
                            <View
                                style={{
                                    width: 240,
                                    borderBottomLeftRadius: 4,
                                    borderBottomRightRadius: 4
                                }}
                                className={`rounded-sm border border-borderGrey ${index == events?.length - 1 ? 'mr-3' : ''} bg-white`}
                            >
                                <ImageBackground
                                    source={{uri: event?.image?.url ?? `${apiConfig.publicUrl}/assets/event-bg.jpg`}}
                                    style={{width: '100%', height: 180}}
                                    imageStyle={{
                                        borderTopLeftRadius: 4,
                                        borderTopRightRadius: 4,
                                        objectFit: 'cover'
                                    }}
                                    className="flex flex-col justify-between items-between"
                                >
                                    <View className="flex flex-row justify-between items-center p-3">
                                        {event?.event_template && (
                                            <View className="bg-white rounded-md">
                                                <Text
                                                    className="px-2 py-1 text-sm text-darkGrey"
                                                >{event.event_template.name}</Text>
                                            </View>
                                        )}
                                        <TouchableOpacity
                                            onPress={() => share(event.uuid)}
                                            className="bg-white rounded-full p-2"
                                        >
                                            <Ionicons name="share-social" size={18} color="black"/>
                                        </TouchableOpacity>
                                    </View>
                                    <ImageBackground
                                        source={blackAlphaGradient}
                                        style={{width: '100%', height: 100}}
                                        imageStyle={{justifyContent: 'flex-start'}}
                                        contentFit={'cover'}
                                        className="flex flex-col justify-end"
                                    >
                                        <View
                                            className="flex flex-row justify-start items-center px-3 py-3">
                                            {(event?.duration && isLive(event.start_date, event.duration)) ? (
                                                <>
                                                    <View
                                                        style={{
                                                            width: 4,
                                                            height: 4,
                                                            backgroundColor: 'red'
                                                        }}
                                                    />
                                                    <Text
                                                        className="ml-2 text-white">{i18n.staticEventsCarousel.live}</Text>
                                                </>
                                            ) : (
                                                <Text
                                                    className="text-white">{i18n.staticEventsCarousel.startsIn(differenceInDays(event.start_date, new Date()))}</Text>
                                            )}
                                        </View>
                                    </ImageBackground>
                                </ImageBackground>
                                <View className="flex flex-col px-3 py-4">
                                    {event?.duration && (
                                        <Text
                                            className="text-xs mb-1 text-primary">{getEventDates(
                                            event.start_date,
                                            event.duration
                                        )}</Text>
                                    )}
                                    <Text
                                        className="text-sm font-bold mb-2 text-lg text-darkGrey font-bold"
                                    >{event.title}</Text>
                                    {event?.city?.name && (
                                        <View className="flex flex-row items-center">
                                            <Ionicons name="location-sharp" size={16} color="#FFEE4A"/>
                                            <Text
                                                className="text-xs text-gray-500 ml-1">{event.city.name}</Text>
                                        </View>
                                    )}
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                })}
            </ScrollView>
        </View>
    );
};
