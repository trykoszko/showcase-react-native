import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {useState} from 'react';
import Loader from '../loader/Loader';
import {ImageBackground} from 'expo-image';
import Ionicons from '@expo/vector-icons/Ionicons';
import i18n from '../../i18n';
import {differenceInDays, getEventDates, isLive} from '../../helpers/date-helpers';
import {useRouter} from 'expo-router';
import {EventFilter, useEventFilters} from '../../constants/eventFilters';
import {share} from '../../helpers/share';
import {useQuery} from "react-query";
import {apiConfig} from "../../config/variables";
import {AxiosInstance, AxiosResponse} from "axios";
import {Router} from "expo-router/build/types";
import {usePublicAxios} from "../../hooks/usePublicAxios";

const blackAlphaGradient = require('../../../assets/black-alpha-gradient.png');

type Props = {
    limit?: number;
    hasFilters?: boolean;
    title?: string;
}

export const EventsCarouselPublic = ({limit = 20, hasFilters = true, title = i18n.eventsCarouselPublic.title}: Props) => {
    const router: Router = useRouter();
    const filters: EventFilter[] = useEventFilters(true);
    const [activeFilter, setActiveFilter] = useState<EventFilter>(filters[0]);
    const axios: AxiosInstance = usePublicAxios();
    const [isError, setIsError] = useState<boolean>(false);

    const {status, data, isFetching, error} = useQuery<unknown, unknown, { data: Event[], meta: any }>(
        ['events_carousel_public', activeFilter.key],
        async () => {
            try {
                setIsError(false);
                const res: AxiosResponse = await axios.get(`public/event?limit=6&filter=${activeFilter.key}`);

                return res?.data ?? [];
            } catch (e) {
                setIsError(true);
                console.log('events_carousel_public exception', e)
            }
        }
    )

    return (
        <View>
            {hasFilters && (
                <ScrollView
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    className="mb-4"
                >
                    <View
                        className={'flex flex-row items-center ml-3'}
                    >
                        <Ionicons name={'funnel'} size={20} color={'grey'}/>
                        {filters.map((filter, index) => (
                            <TouchableOpacity
                                key={index}
                                onPress={() => setActiveFilter(filter)}
                                className={`${activeFilter.key === filter.key ? 'bg-primary' : 'bg-gray-200'} rounded-full px-4 py-2 ${index === filters.length - 1 ? 'mr-3' : 'mr-2'} ${index === 0 ? 'ml-3' : ''}`}
                            >
                                <Text
                                    className={`${activeFilter.key === filter.key ? 'text-white' : 'text-gray-700'} text-xs`}
                                >{filter.label}</Text>
                            </TouchableOpacity>
                        ))}
                    </View>
                </ScrollView>
            )}
            <View className="flex flex-row justify-between items-center mb-3 px-3">
                <Text className="text-2xl font-bold">{title}</Text>
            </View>
            {isFetching ? (
                <View
                    className="m-4 p-4 flex-row items-center justify-center"
                    style={{
                        height: 310
                    }}
                >
                    <Loader/>
                </View>
            ) : (
                ((status === 'error' || isError)
                        ? (
                            <View
                                className={"flex flex-col items-center justify-center"}
                                style={{
                                    height: 315
                                }}
                            >
                                <Ionicons name={'alert-circle-outline'} size={32} color={'grey'}/>
                                <Text
                                    className={"text-center text-md text-darkGrey pt-2 pb-4"}
                                >
                                    Błąd pobierania wydarzeń
                                </Text>
                            </View>
                        )
                        : (
                            (data?.data?.length ? (
                                <ScrollView
                                    horizontal={true}
                                    showsHorizontalScrollIndicator={false}
                                    className="mb-4"
                                >
                                    {data?.data?.map((event: any, index: number) => (
                                        <TouchableOpacity
                                            key={index}
                                            onPress={() =>
                                                router.push({
                                                    pathname: '/locked-event'
                                                })
                                            }
                                            className="ml-3"
                                        >
                                            <View
                                                style={{
                                                    width: 240,
                                                    borderBottomLeftRadius: 4,
                                                    borderBottomRightRadius: 4
                                                }}
                                                className={`rounded-sm border border-borderGrey ${index === data?.data?.length - 1 ? 'mr-3' : ''} bg-white`}
                                            >
                                                <ImageBackground
                                                    source={{uri: event?.image?.url ?? `${apiConfig.publicUrl}/assets/event-bg.jpg`}}
                                                    style={{width: '100%', height: 180}}
                                                    imageStyle={{
                                                        borderTopLeftRadius: 4,
                                                        borderTopRightRadius: 4,
                                                        objectFit: 'cover'
                                                    }}
                                                    className="flex flex-col justify-between items-between"
                                                >
                                                    <View className="flex flex-row justify-between items-center p-3">
                                                        {event?.event_template && (
                                                            <View className="bg-white rounded-md">
                                                                <Text
                                                                    className="px-2 py-1 text-sm text-darkGrey"
                                                                >{event.event_template.name}</Text>
                                                            </View>
                                                        )}
                                                        <TouchableOpacity
                                                            onPress={() => share(event.uuid)}
                                                            className="bg-white rounded-full p-2"
                                                        >
                                                            <Ionicons name="share-social" size={18} color="black"/>
                                                        </TouchableOpacity>
                                                    </View>
                                                    <ImageBackground
                                                        source={blackAlphaGradient}
                                                        style={{width: '100%', height: 100}}
                                                        imageStyle={{justifyContent: 'flex-start'}}
                                                        contentFit={'cover'}
                                                        className="flex flex-col justify-end"
                                                    >
                                                        <View
                                                            className="flex flex-row justify-start items-center px-3 py-3">
                                                            {isLive(event.start_date, event.duration) ? (
                                                                <>
                                                                    <View
                                                                        style={{
                                                                            width: 4,
                                                                            height: 4,
                                                                            backgroundColor: 'red'
                                                                        }}
                                                                    />
                                                                    <Text
                                                                        className="ml-2 text-white">{i18n.eventsCarouselPublic.live}</Text>
                                                                </>
                                                            ) : (
                                                                <Text
                                                                    className="text-white">{i18n.eventsCarouselPublic.startsIn(differenceInDays(event.start_date, new Date()))}</Text>
                                                            )}
                                                        </View>
                                                    </ImageBackground>
                                                </ImageBackground>
                                                <View className="flex flex-col px-3 py-4">
                                                    <Text
                                                        className="text-xs mb-1 text-primary">{getEventDates(
                                                        event.start_date,
                                                        event.duration
                                                    )}</Text>
                                                    <Text
                                                        className="text-sm font-bold mb-2 text-lg text-darkGrey font-bold">{event.title}</Text>
                                                    {event?.city?.name && (
                                                        <View className="flex flex-row items-center">
                                                            <Ionicons name="location-sharp" size={16} color="#FFEE4A"/>
                                                            <Text
                                                                className="text-xs text-gray-500 ml-1">{event.city.name}</Text>
                                                        </View>
                                                    )}
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    ))}
                                </ScrollView>
                            ) : (
                                <View
                                    className={"flex flex-col items-center justify-center"}
                                    style={{
                                        height: 315
                                    }}
                                >
                                    <Ionicons name={'funnel-outline'} size={32} color={'grey'}/>
                                    <Text
                                        className={"text-center text-md text-darkGrey pt-2 pb-4"}
                                    >
                                        Nie znaleziono wydarzeń pasujących do filtra
                                    </Text>
                                </View>
                            ))
                        )
                )
            )}
        </View>
    );
};
