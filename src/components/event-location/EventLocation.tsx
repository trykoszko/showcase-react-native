import {Text, View} from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import i18n from '../../i18n';

type Props = {
    locationName: string,
    distance?: number
}

export const EventLocation = ({locationName, distance}: Props) => {
    return (
        <View
            className="flex flex-col mx-3 px-4 mb-3 py-5 bg-white border border-borderGrey rounded-2xl shadow-xl shadow-gray-200"
        >
            <View className="flex flex-row justify-start items-center mb-4">
                <View
                    className="flex flex-row justify-center items-center rounded-xl"
                    style={{
                        width: 48,
                        height: 48,
                        backgroundColor: '#efedfc'
                    }}
                >
                    <Ionicons name="location-sharp" size={22} color="#672171"/>
                </View>
                <Text className="ml-3">{i18n.eventLocation.title}</Text>
            </View>
            <View className="flex flex-row justify-start items-center">
                <Ionicons name="location-sharp" size={22} color="#FFEE4A"/>
                <Text className="ml-3">{locationName} {distance && i18n.eventLocation.distance(distance)}</Text>
            </View>
        </View>
    );
};
