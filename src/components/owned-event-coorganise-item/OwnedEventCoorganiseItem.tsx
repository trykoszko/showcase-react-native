import Ionicons from '@expo/vector-icons/Ionicons';
import {Alert, Pressable, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {getDaysLeft} from '../../helpers/date-helpers';
import i18n from '../../i18n';
import FontAwesome5 from '@expo/vector-icons/FontAwesome5';
import FontAwesome from '@expo/vector-icons/FontAwesome';
import {EventEventElement} from "../../types/EventEventElement.type";

type Props = {
    item: EventEventElement;
}

export const OwnedEventCoorganiseItem = ({item}: Props) =>
{
    const isFavourite = Math.random() > 0.5;

    const removeCoorganiseItem = () =>
    {
        Alert.alert('Remove coorganise item');
    };

    return (
        <Pressable
            className="border border-borderGrey rounded-lg p-4 mb-3 mx-3 flex flex-col bg-white shadow-md"
            onLongPress={() =>
            {
                Alert.alert(
                    'Usunąć ten element współorganizacji wydarzenia?',
                    undefined,
                    [
                        {
                            text:  i18n.ownedEventCoorganizeItem.cancel,
                            style: 'cancel'
                        },
                        {
                            text:    i18n.ownedEventCoorganizeItem.remove,
                            onPress: () => removeCoorganiseItem(),
                            style:   'destructive'
                        }
                    ],
                );
            }}
        >
            <View className="flex flex-row justify-between items-center">
                <View className="flex flex-row items-center">
                    <View
                        className="flex flex-row justify-center items-center rounded-xl"
                        style={{
                            width:           48,
                            height:          48,
                            backgroundColor: '#efe8f0'
                        }}
                    >
                        <FontAwesome5 name="hands-helping" size={16} color="#672171"/>
                    </View>
                    <View className="ml-3 py-2 px-3 border border-primary rounded-md">
                        <Text className="color-primary">
                            {item.event_element.name}
                        </Text>
                    </View>
                </View>
                <Text className="color-primary">
                    {item.offer_price} {i18n.helpers.currency}
                </Text>
                <TouchableOpacity onPress={() =>
                {
                    Alert.alert('Add to favourites');
                }}>
                    <Ionicons name={isFavourite ? 'heart' : 'heart-outline'} size={24} color="#952aa9"/>
                </TouchableOpacity>
            </View>
            <View className="flex flex-row mt-3 py-2">
                <FontAwesome name="calendar-times-o" size={16} color="#a1a1a9"/>
                <Text className="text-gray-500 ml-2">
                    {i18n.ownedEventCoorganizeItem.daysLeft(getDaysLeft(item.event.start_date))}
                </Text>
            </View>
        </Pressable>
    );
};
