import { Animated, Easing } from "react-native"

const spinnerImage = require('../../../assets/spinner.png')

export default function Loader() {
    let rotateValueHolder = new Animated.Value(0);

    const startImageRotateFunction = () => {
        rotateValueHolder.setValue(0);
        Animated.timing(rotateValueHolder, {
            toValue: 1,
            duration: 1500,
            easing: Easing.linear,
            useNativeDriver: false,
        }).start(() => startImageRotateFunction())
    }

    const RotateData = rotateValueHolder.interpolate({
        inputRange: [0, 1],
        outputRange: ['0deg', '360deg'],
    });

    startImageRotateFunction()

    return <Animated.Image
        source={spinnerImage}
        style={{ width: 36, height: 36, transform: [{ rotate: RotateData }], marginTop: -30 }}
    />
}
