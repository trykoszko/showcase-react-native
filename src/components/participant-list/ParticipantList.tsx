import {Alert, Text, TouchableOpacity, View} from 'react-native';
import {Image} from 'expo-image';
import FontAwesome5 from '@expo/vector-icons/FontAwesome5';
import i18n from '../../i18n';
import {router} from 'expo-router';
import {getFormattedDate} from "../../helpers/date-helpers";
import {useAuthStore} from "../../store/AuthStore";
import {EventUser} from "../../types/EventUser.type";
import {apiConfig} from "../../config/variables";
import {MyUser} from "../../types/MyUser.type";
import {EventUserRoleEnum} from "../../enums/EventUserRole.enum";
import React from "react";

type Props = {
    participants: EventUser[],
    setIsModalOpen: (isModalOpen: boolean) => void,
    isOwner: boolean
}

export const ParticipantList = ({participants, setIsModalOpen, isOwner}: Props) => {
    const userData: MyUser | undefined = useAuthStore(state => state.userData);

    const showParticipantModal = (participant: EventUser) => {
        const isYou: boolean = participant.uuid === userData?.uuid;

        if (!isOwner || isYou) {
            return
        }

        Alert.alert(
            participant.name,
            undefined,
            [
                {
                    text: i18n.participantList.cancel,
                    style: 'cancel'
                },
                {
                    text: i18n.participantList.remove,
                    onPress: () => {
                        // @TODO:
                    },
                    style: 'destructive'
                },
                {
                    text: i18n.participantList.addCoorganizer,
                    onPress: () => {
                        // @TODO:
                    },
                }
            ],
        );
    };

    return (
        <View
            className={'flex flex-col'}
        >
            {participants?.map((participant: EventUser, index: number) => {
                const isFriend: boolean = userData?.friends?.includes(participant.id) ?? false;
                const isOwner: boolean = participant?.role === EventUserRoleEnum.OWNER;
                const isYou: boolean = participant.uuid === userData?.uuid;
                const isCompany: boolean = !!participant?.company_name;

                return (
                    <TouchableOpacity
                        key={index}
                        className="flex flex-row justify-start items-center py-2 border-b border-b-gray-100"
                        onPress={() => showParticipantModal(participant)}
                    >
                        <TouchableOpacity
                            onPress={() => {
                                setIsModalOpen(false);
                                router.push(`/user/${participant.uuid}`);
                            }}
                        >
                            <Image
                                source={{uri: participant?.avatar?.url ?? `${apiConfig.publicUrl}/assets/user.png`}}
                                style={{
                                    width: 64,
                                    height: 64,
                                    borderRadius: 64
                                }}
                                className={'mr-3'}
                            />
                        </TouchableOpacity>
                        <View
                            className="flex flex-col justify-start items-start"
                        >
                            <View
                                className="flex flex-row justify-start items-center gap-2"
                            >
                                <Text>{participant?.name}</Text>
                                {isCompany && (
                                    <FontAwesome5 name={'building'} size={18} color={'grey'}/>
                                )}
                                {isOwner && (
                                    <FontAwesome5 name={'crown'} size={18} color={'orange'}/>
                                )}
                                {isFriend && (
                                    <FontAwesome5 name={'handshake'} size={18} color={'grey'}/>
                                )}
                                {isYou && (
                                    <FontAwesome5 name={'user-alt'} size={18} color={'purple'}/>
                                )}
                            </View>
                            <Text
                                className={'text-xs color-gray-400 mt-2'}
                            >{i18n.participantList.addedOn} {getFormattedDate(new Date(participant?.created_at))}</Text>
                        </View>
                    </TouchableOpacity>
                );
            })}
        </View>
    );
};
