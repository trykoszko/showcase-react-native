import {Text, View} from "react-native";
import React from "react";
import {EventElement} from "../../types/EventElement.type";

type Props = {
    element: EventElement,
    selected?: boolean
}

export const EventElementComponent = ({ element, selected = false }: Props) => {
    return (
        <View
            className={`flex flex-row justify-stretch items-center w-full`}
            style={selected ? {
                borderWidth: 1,
                borderColor: element?.color,
                borderRadius: 12,
                padding: 4
            } : {
                padding: 5
            }}
        >
            <View
                className="flex flex-row justify-center items-center relative"
                style={{
                    width: 48,
                    height: 48
                }}
            >
                {/*<Ionicons name="location-sharp" size={22} color="#672171"/>*/}
                <Text>{element?.icon}</Text>
                <View
                    className={"w-full h-full rounded-xl absolute top-0 left-0"}
                    style={{
                        backgroundColor: element?.color,
                        opacity: .1
                    }}
                />
            </View>
            <Text className="ml-3 mr-auto">{element?.name}</Text>
        </View>
    )
}