import Ionicons from '@expo/vector-icons/Ionicons';
import {Image} from 'expo-image';
import {Alert, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {getDateRange, getDaysLeft} from '../../helpers/date-helpers';
import i18n from '../../i18n';
import {EventOffer} from "../../types/EventOffer.type";
import {useAuthStore} from "../../store/AuthStore";
import {apiConfig} from "../../config/variables";
import {Link} from "expo-router";

type Props = {
    item: EventOffer;
}

export const CoorganiseOffer = ({item}: Props) =>
{
    const userData = useAuthStore(state => state.userData);
    const isFavourite = userData?.saved_event_event_elements?.find(uuid => uuid === item.uuid) !== undefined ?? false;
    const isMine = item?.provider?.uuid === userData?.uuid;

    return (
        <View
            className="border border-borderGrey rounded-lg p-4 mb-3 flex flex-col bg-white shadow-md w-full"
        >
            <View className="flex flex-row justify-between items-center">
                <View className="flex flex-row items-center">
                    <Link href={`/event/${item?.event_element?.event?.uuid}`}>
                        <Image
                            source={{uri: item?.event_element?.event?.image?.url ?? `${apiConfig.publicUrl}/assets/event-bg.jpg`}}
                            className="w-12 h-12 rounded-md"
                        />
                    </Link>
                    <View className="ml-3 py-2 px-3 border border-primary rounded-md">
                        <Text className="color-primary">
                            {item?.event_element?.event?.event_template?.icon} {item?.event_element?.event?.event_template?.name}
                        </Text>
                    </View>
                </View>
                <Text className="color-primary">
                    {item?.price} {i18n.helpers.currency}
                </Text>
                <TouchableOpacity onPress={() =>
                {
                    Alert.alert('Add to favourites');
                }}>
                    <Ionicons name={isFavourite ? 'heart' : 'heart-outline'} size={24} color="#952aa9"/>
                </TouchableOpacity>
            </View>
            <View className="flex flex-col mt-3">
                {item?.event_element?.event?.start_date && (
                    <Text className="color-primary">
                        {getDateRange(new Date(), item.event_element.event.start_date)}
                    </Text>
                )}
                <Text className="font-bold text-lg font-bold mt-1">
                    {item.event_element.event.title}
                </Text>
            </View>
            <View className="flex flex-row items-center mt-4">
                <View className="flex flex-row items-center">
                    <Ionicons name="location-sharp" size={16} color="#FFEE4A"/>
                    <Text className="ml-1">
                        {item?.event_element?.event?.city?.name}
                    </Text>
                </View>
                <View className="flex flex-row items-center ml-3">
                    <Ionicons name="calendar-outline" size={16}/>
                    <Text className="ml-1">
                        {i18n.coorganiseOffer.daysLeft(getDaysLeft(item.event_element.event.start_date))}
                    </Text>
                </View>
            </View>
            {isMine ? (<></>) : (
                <View className="flex flex-row gap-x-2 mt-3 pt-3 border-t border-borderGrey">
                    <View
                        className={`rounded-md bg-white flex flex-grow`}
                    >
                        <TouchableOpacity
                            onPress={() =>
                            {
                                Alert.alert(i18n.coorganiseOffer.sendOffer);
                            }}
                        >
                            <Text
                                className={`text-base font-bold py-3 px-3 text-center text-red-600`}>
                                {i18n.coorganiseOffer.reject}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View
                        className={`rounded-md bg-primary flex flex-grow`}
                    >
                        <TouchableOpacity
                            onPress={() =>
                            {
                                Alert.alert(i18n.coorganiseOffer.sendOffer);
                            }}
                        >
                            <Text
                                className={`text-base font-bold py-3 px-3 text-center text-white`}>
                                {i18n.coorganiseOffer.accept}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </View>
    );
};
