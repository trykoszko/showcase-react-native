import Ionicons from '@expo/vector-icons/Ionicons';
import {Alert, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {getDateRange, getDaysLeft} from '../../helpers/date-helpers';
import i18n from '../../i18n';
import {Event} from '../../types/Event.type';
import {Image} from 'expo-image';
import {apiConfig} from "../../config/variables";

type Props = {
    event: Event;
}

export const EventInvite = ({event}: Props) =>
{
    return (
        <View
            className="border border-borderGrey rounded-lg p-4 mb-3 flex flex-col bg-white shadow-md w-full"
        >
            <View
                className="flex flex-row items-start"
            >
                <Image
                    source={{uri: event?.image?.url ?? `${apiConfig.publicUrl}/assets/event-bg.jpg`}}
                    style={{
                        width:        64,
                        height:       64,
                        borderRadius: 10
                    }}
                    className="mr-3"
                />
                <View
                    className="flex flex-col"
                >
                    <View className="flex flex-col">
                        <Text className="text-primaryLight text-xs">
                            {getDateRange(new Date(), event.start_date)}
                        </Text>
                        <Text className="font-bold text-lg font-bold mt-0.5">
                            {event.title}
                        </Text>
                    </View>
                    <View className="flex flex-row items-center mt-0.5">
                        <View className="flex flex-row items-center">
                            <Ionicons name="location-sharp" size={16} color="#FFEE4A"/>
                            <Text className="ml-1 text-xs text-gray-600">
                                {event.city.name}
                            </Text>
                        </View>
                        <View className="flex flex-row items-center ml-3">
                            <Ionicons name="calendar-outline" size={16}/>
                            <Text className="ml-1 text-xs text-gray-600">
                                {i18n.eventInvite.daysLeft(getDaysLeft(event.start_date))}
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
            <View className="flex flex-row gap-x-2 mt-3 pt-3 border-t border-borderGrey">
                <View
                    className={`rounded-md bg-primary flex flex-grow`}
                >
                    <TouchableOpacity
                        onPress={() =>
                        {
                            Alert.alert(i18n.eventInvite.sendOffer);
                        }}
                    >
                        <Text
                            className={`text-base font-bold py-3 px-3 text-center text-white`}
                        >
                            Dołącz
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};
