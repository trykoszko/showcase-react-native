import {Pressable, Text, View} from 'react-native';
import React from 'react';
import {ChatMessage} from '../../types/ChatMessage.type';
import {Image} from 'expo-image';
import {apiConfig} from "../../config/variables";
import {getFormattedMessageDate} from "../../helpers/date-helpers";
import {useRouter} from "expo-router";
import {ChatMessageTypeEnum} from "../../enums/ChatMessageType.enum";
import {CoorganiseOffer} from "./CoorganiseOffer";
import i18n from "../../i18n";

type Props = {
    message: ChatMessage,
    currentUserUuid: string | undefined,
    currentUserAvatar?: string,
    friendUuid: string,
    friendName?: string,
    friendAvatar?: string,
}

export const Message = ({message, currentUserUuid, currentUserAvatar, friendUuid, friendName, friendAvatar}: Props) => {
    const router = useRouter();
    const isAuthor = message?.author_uuid === currentUserUuid;

    return (
        <View
            className={`mb-1`}
        >
            <View
                className={'flex flex-col w-full'}
            >
                {(message?.type === ChatMessageTypeEnum.OFFER && message?.offer) && (
                    <View
                        className={`flex flex-row items-center w-full`}
                    >
                        <CoorganiseOffer item={message.offer}/>
                    </View>
                )}
                <View
                    className={`flex flex-row items-start px-3 py-4 rounded-xl ${isAuthor ? 'ml-auto bg-white border border-gray-200' : 'mr-auto bg-white border border-gray-200'}`}
                    style={{
                        maxWidth: '90%'
                    }}
                >
                    {(friendAvatar && !isAuthor) && (
                        <Pressable
                            onPress={() => {
                                router.push(`/user/${friendUuid}`);
                            }}
                            className={'flex flex-row items-center'}
                        >
                            <Image
                                source={{uri: friendAvatar ?? `${apiConfig.publicUrl}/assets/user.png`}}
                                style={{
                                    width: 40,
                                    height: 40,
                                    borderRadius: 40
                                }}
                                className={'mr-3'}
                            />
                        </Pressable>
                    )}
                    <View
                        className={`flex flex-col items-start}`}
                        style={{
                            maxWidth: '85%'
                        }}
                    >
                        <View
                            className={`flex flex-row items-center mb-2`}
                        >
                            <Text
                                className={'font-bold'}
                            >{isAuthor ? i18n.chatMessage.you : friendName}</Text>
                            <View className={'w-1 h-1 bg-gray-500 rounded-full mx-2'}/>
                            <Text
                                className={'text-xs'}
                            >
                                {getFormattedMessageDate(message?.created_at ?? new Date())}
                            </Text>
                        </View>
                        <Text
                            className={'text-sm'}
                        >{message.text}</Text>
                        {/*{message?.type === 'friendInvite' && (*/}
                        {/*    <View*/}
                        {/*        className={`flex flex-row items-center mt-3`}*/}
                        {/*    >*/}
                        {/*        <TouchableOpacity*/}
                        {/*            className={'bg-primary px-4 py-3 rounded-md'}*/}
                        {/*            onPress={() =>*/}
                        {/*            {*/}
                        {/*                Alert.alert(`friend invite accept ${message.authorUuid}`);*/}
                        {/*            }}*/}
                        {/*        >*/}
                        {/*            <Text*/}
                        {/*                className={'text-sm text-white font-bold'}*/}
                        {/*            >*/}
                        {/*                Akceptuj zaproszenie do znajomych*/}
                        {/*            </Text>*/}
                        {/*        </TouchableOpacity>*/}
                        {/*    </View>*/}
                        {/*)}*/}
                        {/*{(message?.type === 'eventInvite' && message?.event) && (*/}
                        {/*    <View*/}
                        {/*        className={`flex flex-row items-center mt-3`}*/}
                        {/*    >*/}
                        {/*        <View*/}
                        {/*            className={`flex flex-row items-center w-full`}*/}
                        {/*        >*/}
                        {/*            <EventInvite event={message.event}/>*/}
                        {/*        </View>*/}
                        {/*    </View>*/}
                        {/*)}*/}
                    </View>
                    {(isAuthor) && (
                        <Pressable
                            onPress={() => {
                                router.push('/profile');
                            }}
                            className={'flex flex-row items-center'}
                        >
                            <Image
                                source={{uri: currentUserAvatar ?? `${apiConfig.publicUrl}/assets/user.png`}}
                                style={{
                                    width: 40,
                                    height: 40,
                                    borderRadius: 40
                                }}
                                className={'ml-3'}
                            />
                        </Pressable>
                    )}
                </View>
            </View>
        </View>
    );
};
