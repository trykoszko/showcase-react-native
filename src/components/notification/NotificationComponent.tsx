import {Dimensions, Linking, Platform, StatusBar, Text, TouchableOpacity, View} from "react-native";
import React, {useEffect} from "react";
import Constants from "expo-constants";
import {Gesture, GestureDetector, PanGesture} from "react-native-gesture-handler";
import {usePushNotificationStore} from "../../store/PushNotificationStore";
import Ionicons from "@expo/vector-icons/Ionicons";
import i18n from "../../i18n";

type Props = {}

export const NotificationComponent = ({}: Props) => {
    const notification = usePushNotificationStore(state => state.notification);
    const setNotification = usePushNotificationStore(state => state.setNotification);

    const closeNotification = () => {
        const emptyNotification: any = {};
        setNotification(emptyNotification);
    }

    useEffect(() => {
        setTimeout(() => {
            closeNotification()
        }, 3000);
    }, []);

    const pan: PanGesture = Gesture.Pan().onStart((event) => {
        if (Math.abs(event.translationX) > 10) {
            closeNotification()
        }
    });

    if (!notification || !notification?.request?.content?.title) {
        return <></>;
    }

    return (
        <View
            style={{
                position: 'absolute',
                top:
                    ((Platform.OS === 'android' ? StatusBar.currentHeight : Constants.statusBarHeight) ?? 0) + 10,
                left: 0,
                right: 0,
                width: Dimensions.get('window').width
            }}
            className={"px-3"}
        >
            <GestureDetector gesture={pan}>
                <TouchableOpacity
                    className={"flex flex-col bg-white w-full px-3 pt-3 pb-4 shadow-2xl rounded-lg"}
                    onPress={() => {
                        const url: string = notification?.request?.content.data?.url ?? null;
                        if (url) {
                            closeNotification();
                            Linking.openURL(notification.request.content.data.url);
                        }
                    }}
                >
                    <View
                        className={"flex-row"}
                    >
                        <View
                            className={"w-9 h-9 bg-primary rounded-full flex items-center justify-center mr-3 mt-1"}
                        >
                            <Ionicons name={"notifications"} size={19} color={"yellow"}/>
                        </View>
                        <View
                            className={"flex-col"}
                            style={{
                                maxWidth: '84%'
                            }}
                        >
                            {notification?.request?.content?.title && (
                                <Text
                                    className={"font-bold text-xl mb-1 leading-6"}
                                >{notification.request.content.title}</Text>
                            )}
                            {notification?.request?.content?.body && (
                                <Text
                                    className={"mb-3 text-darkGrey"}
                                >{notification.request.content.body}</Text>
                            )}
                            {notification?.request?.content.data?.url && (
                                <Text
                                    className={"text-primary"}
                                >{i18n.notificationComponent.tapToView}</Text>
                            )}
                        </View>
                    </View>
                </TouchableOpacity>
            </GestureDetector>
        </View>
    )
}
