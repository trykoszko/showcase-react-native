import { ImageBackground } from "expo-image"
import { View } from "react-native"
import SplashScreenLogo from "./components/SplashScreenLogo"
import { StatusBar } from "expo-status-bar"
import Loader from "../loader/Loader"

const splashImageBackground = require('../../../assets/background.jpg')

const SplashScreen = () => {
    return (
        <>
            <StatusBar style="light" />
            <View
                className="h-screen w-screen flex items-center justify-center">
                <ImageBackground
                    source={splashImageBackground}
                    style={{ width: '100%', height: '100%' }}
                    className="flex items-center justify-center"
                >
                    <SplashScreenLogo />
                    <Loader />
                </ImageBackground>
            </View>
        </>
    )
}

export default SplashScreen
