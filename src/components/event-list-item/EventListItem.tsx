import {Text, TouchableOpacity, View} from 'react-native';
import {Event} from '../../types/Event.type';
import {Image, ImageBackground} from 'expo-image';
import Ionicons from '@expo/vector-icons/Ionicons';
import i18n from '../../i18n';
import {router} from 'expo-router';
import {getDaysLeft, getFormattedDate} from '../../helpers/date-helpers';
import {share} from '../../helpers/share';
import {apiConfig} from "../../config/variables";
import {EventUserRoleEnum} from "../../enums/EventUserRole.enum";
import {useApi} from "../../hooks/useApi";
import {useAuthStore} from "../../store/AuthStore";
import {MyUser} from "../../types/MyUser.type";
import {useState} from "react";
import {UserEvent} from "../../types/UserEvent.type";
import {User} from "../../types/User.type";

const blackAlphaGradient = require('../../../assets/black-alpha-gradient.png');

type Props = {
    event: Event
}

export const EventListItem = ({event}: Props) => {
    const ownerUser: UserEvent | undefined =
        event
            .users
            .find(user => user.role === EventUserRoleEnum.OWNER);

    const owner: User | undefined = ownerUser?.user;

    const {saveEvent, unsaveEvent} = useApi();
    const userData: MyUser | undefined = useAuthStore(state => state.userData);
    const [isSaved, setIsSaved] = useState<boolean>(
        userData?.saved_events?.find(uuid => uuid === event.uuid) !== undefined
    );

    return (
        <View className="bg-white border border-gray-200 shadow-lg mb-3 rounded-lg">
            <TouchableOpacity
                onPress={() => {
                    router.push(`/event/${event.uuid}`);
                }}
            >
                <ImageBackground
                    className="w-full h-full flex flex-col justify-between"
                    style={{
                        height: 300,
                    }}
                    source={{uri: event?.image?.url ?? `${apiConfig.publicUrl}/assets/event-bg.jpg`}}
                    imageStyle={{borderTopLeftRadius: 6, borderTopRightRadius: 6}}
                >
                    <View
                        className={'flex flex-row items-center justify-between p-4'}
                    >
                        <View
                            className={'flex flex-row items-center bg-white rounded-md p-2'}
                        >
                            <Text>{event.event_template.icon} {event.event_template.name}</Text>
                        </View>
                        <TouchableOpacity
                            className={'flex flex-row items-center bg-white rounded-full p-2 ml-auto'}
                            onPress={async () => {
                                if (isSaved) {
                                    await unsaveEvent(event.uuid);
                                } else {
                                    await saveEvent(event.uuid);
                                }
                                setIsSaved(!isSaved);
                            }}
                        >
                            <Ionicons name={isSaved ? 'heart' : 'heart-outline'} size={20} color={'black'}/>
                        </TouchableOpacity>
                        <TouchableOpacity
                            className={'flex flex-row items-center bg-white rounded-full p-2 ml-3'}
                            onPress={() => share(event.uuid)}
                        >
                            <Ionicons name={'share-social'} size={20} color={'black'}/>
                        </TouchableOpacity>
                    </View>
                    <ImageBackground
                        source={blackAlphaGradient}
                        style={{width: '100%', height: 100}}
                        imageStyle={{justifyContent: 'flex-start'}}
                        contentFit={'cover'}
                        className="flex flex-col justify-end p-4"
                    >
                        <View
                            className={'flex flex-row items-center justify-end'}
                        >
                            <Text
                                className={'text-white'}
                            >{i18n.eventListItem.startsIn(getDaysLeft(event.start_date))}</Text>
                        </View>
                    </ImageBackground>
                </ImageBackground>
            </TouchableOpacity>
            {owner && (
                <View className="flex flex-row justify-start items-center py-3 mx-3 border-b border-b-gray-200">
                    <TouchableOpacity
                        onPress={() => router.push(`/user/${owner.uuid}`)}
                    >
                        <Image
                            source={{uri: owner?.avatar?.url ?? `${apiConfig.publicUrl}/assets/user.png`}}
                            style={{width: 48, height: 48, borderRadius: 48}}
                            className="mr-3"
                        />
                    </TouchableOpacity>
                    <View className="flex flex-col justify-start w-full">
                        <Text className="text-sm text-primary">{i18n.eventDetails.creator}</Text>
                        <Text
                            className="text-sm text-darkGrey font-bold"
                        >
                            {owner.name}
                        </Text>
                        <View className="flex flex-row justify-start items-center flex-wrap max-w-full">
                            {/*{owner.role && (*/}
                            {/*    <Text className="text-sm text-grey-500">{owner.role}</Text>*/}
                            {/*)}*/}
                            {/*{(owner.social_handle && owner.social_url) && (*/}
                            {/*    <>*/}
                            {/*        <View*/}
                            {/*            style={{*/}
                            {/*                width:        4,*/}
                            {/*                height:       4,*/}
                            {/*                borderRadius: 2,*/}
                            {/*            }}*/}
                            {/*            className="mx-1 bg-lightGrey"*/}
                            {/*        />*/}
                            {/*        <TouchableOpacity*/}
                            {/*            onPress={() => router.push(owner.social_url)}*/}
                            {/*        >*/}
                            {/*            <Text className="text-sm text-blue-400">{owner.social_handle}</Text>*/}
                            {/*        </TouchableOpacity>*/}
                            {/*    </>*/}
                            {/*)}*/}
                        </View>
                    </View>
                </View>
            )}
            <View className="px-4 pt-2 pb-4">
                <TouchableOpacity
                    onPress={() => {
                        router.push(`/event/${event.uuid}`);
                    }}
                >
                    <Text
                        className={'color-purple-400 text-sm'}
                    >{getFormattedDate(event.start_date)}</Text>
                    <Text className="text-xl font-bold mb-2">{event.title}</Text>
                    <Text className="text-sm text-gray-500">{event?.description?.substring(0, 100)}...</Text>
                    <View className="flex-row items-center mt-4">
                        <Ionicons name={'location'} size={20} color={'orange'}/>
                        <Text
                            className={'ml-2'}
                        >{event.city.name}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
};
