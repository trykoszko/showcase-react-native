import i18n from "../../i18n";
import {Text, TextInput, TouchableOpacity, View} from "react-native";
import {router} from "expo-router";
import {ModalComponent} from "../modal/Modal";
import React, {useState} from "react";
import {useApi} from "../../hooks/useApi";
import Loader from "../loader/Loader";

type Props = {
    isOfferModalVisible: boolean,
    setIsOfferModalVisible: (isVisible: boolean) => void,
    eventUuid: string,
    offerModalUuid: string,
    setOfferModalUuid: (uuid: string) => void
};

export const EventCoorganiseModal = ({
                                         isOfferModalVisible,
                                         setIsOfferModalVisible,
                                         eventUuid,
                                         offerModalUuid,
                                         setOfferModalUuid
                                     }: Props) => {
    const {sendCoorganizeOffer} = useApi();
    const [offerPrice, setOfferPrice] = useState<number>(0);
    const [offerDescription, setOfferDescription] = useState<string>('');
    const [isLoading, setIsLoading] = useState<boolean>(false);

    return (
        <ModalComponent
            isVisible={isOfferModalVisible}
            setIsVisible={setIsOfferModalVisible}
            title={i18n.eventScreen.offerModalTitle}
            content={<View className="flex-col">
                {isLoading ? (
                    <View
                        className={"flex w-full flex-grow items-center justify-center py-10"}
                    >
                        <Loader/>
                    </View>
                    ) : (
                  <>
                      <Text
                          className={"text-gray-800 mb-2"}
                      >
                          {i18n.eventScreen.yourOfferPrice}
                      </Text>
                      <TextInput
                          onChangeText={(val) => {
                              if (val && parseFloat(val) > 0) {
                                  setOfferPrice(parseFloat(val));
                              }
                          }}
                          className={`border py-3 px-2 rounded mb-6 border-gray-300`}
                          placeholder={i18n.eventScreen.yourOfferPrice}
                          keyboardType="decimal-pad"
                          autoCorrect={false}
                          autoFocus={true}
                      />
                      <Text
                          className={"text-gray-800 mb-2"}
                      >
                          {i18n.eventScreen.yourOfferDescription}
                      </Text>
                      <TextInput
                          onChangeText={(val) => {
                              if (val && val.length > 0) {
                                  setOfferDescription(val);
                              }
                          }}
                          className={`border py-3 px-2 rounded mb-6 border-gray-300`}
                          placeholder={i18n.eventScreen.yourOfferDescription}
                          keyboardType="default"
                          autoCorrect={false}
                          autoFocus={false}
                          multiline={true}
                          numberOfLines={6}
                          style={{
                              textAlignVertical: 'top',
                              paddingTop: 8,
                              paddingBottom: 8,
                              minHeight: 100
                          }}
                      />
                      <View
                          className={"flex-row items-center justify-start w-full"}
                      >
                          <TouchableOpacity
                              className={`rounded-md bg-primary flex px-4 py-3`}
                              onPress={async () => {
                                  if (offerPrice && offerDescription && offerModalUuid) {
                                      setIsLoading(true);

                                      const offer = await sendCoorganizeOffer(eventUuid, offerModalUuid, offerPrice, offerDescription);

                                      setIsOfferModalVisible(false);

                                      if (offer?.data) {
                                          router.push(`/chat/${offer.data}`);
                                      }

                                      setIsLoading(false);
                                  }
                              }}
                          >
                              <Text
                                  className={"text-white"}
                              >{i18n.eventScreen.submitOffer}</Text>
                          </TouchableOpacity>
                      </View>
                  </>
                )}
            </View>}/>
    )
}
