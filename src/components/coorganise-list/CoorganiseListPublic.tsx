import {BlurView} from 'expo-blur';
import {useRouter} from 'expo-router';
import {Text, View} from 'react-native';
import i18n from '../../i18n';
import {CoorganizeListItem} from './CoorganiseListItem';
import {AxiosInstance, AxiosResponse} from "axios";
import {useQuery} from "react-query";
import {EventEventElement} from "../../types/EventEventElement.type";
import React, {useState} from "react";
import {UnauthenticatedButtons} from "../unauthenticated-buttons/UnauthenticatedButtons";
import Ionicons from "@expo/vector-icons/Ionicons";
import {usePublicAxios} from "../../hooks/usePublicAxios";

type Props = {
}

export const CoorganiseListPublic = ({}: Props) => {
    const router = useRouter();
    const axios: AxiosInstance = usePublicAxios();
    const [isError, setIsError] = useState<boolean>(false);

    const {status, data, isFetching, error} = useQuery<unknown, unknown, { data: EventEventElement[], meta: any }>(
        ['coorganize_items_list_component', `isLocked=${true}`],
        async () => {
            try {
                setIsError(false);
                const res: AxiosResponse = await axios.get(`public/event-coorganize-item?limit=1`);

                return res?.data ?? [];
            } catch (e) {
                setIsError(true);
                console.log('events_carousel exception', e)
            }
        }
    )

    return (
        <View className="mb-3">

            <View className="flex flex-row justify-between items-center px-3 mb-3">
                <Text className="text-2xl font-bold">{i18n.coorganiseListPublic.title}</Text>
            </View>

            <View
                className="relative w-full"
            >
                {status === 'success' && (
                    data?.data?.map((item: EventEventElement, index: number) => (
                        <View key={index} className={"px-3"}>
                            <CoorganizeListItem item={item} isLocked={true}/>
                        </View>
                    ))
                )}
            </View>

            {isError ? (
                        <View
                            className={"flex flex-col items-center justify-center"}
                            style={{
                                height: 315
                            }}
                        >
                            <Ionicons name={'alert-circle-outline'} size={32} color={'grey'}/>
                            <Text
                                className={"text-center text-md text-darkGrey pt-2 pb-4"}
                            >
                                Błąd pobierania elementów
                            </Text>
                        </View>
                    ) : (
                        <>
                            <BlurView
                                intensity={25}
                                className="z-20 absolute top-10 left-0 right-0 bottom-0"
                            />
                            <View
                                className="absolute w-full px-3 z-30"
                                style={{
                                    top: 150
                                }}
                            >
                                <UnauthenticatedButtons/>
                            </View>
                        </>
            )}
        </View>
    );
};
