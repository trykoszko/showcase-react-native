import {useRouter} from 'expo-router';
import {Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import i18n from '../../i18n';
import {CoorganizeListItem} from './CoorganiseListItem';
import {AxiosInstance, AxiosResponse} from "axios";
import {useAxios} from "../../hooks/useAxios";
import {useQuery} from "react-query";
import {EventEventElement} from "../../types/EventEventElement.type";
import React, {useState} from "react";
import {EventCoorganiseModal} from "../coorganise-modal/EventCoorganiseModal";

type Props = {
}

export const CoorganiseList = ({}: Props) => {
    const router = useRouter();
    const axios: AxiosInstance = useAxios();
    const [isError, setIsError] = useState<boolean>(false);
    const [isOfferModalVisible, setIsOfferModalVisible] = useState<boolean>(false);
    const [offerModalUuid, setOfferModalUuid] = useState<string>('');
    const [offerModalEventUuid, setOfferModalEventUuid] = useState<string>('');

    const {status, data, isFetching, error} = useQuery<unknown, unknown, { data: EventEventElement[], meta: any }>(
        ['coorganize_items_list_component', `isLocked=${false}`],
        async () => {
            try {
                setIsError(false);
                const res: AxiosResponse = await axios.get(`event-coorganize-item?limit=6`);

                return res?.data ?? [];
            } catch (e) {
                setIsError(true);
                console.log('coorganize_items_list_component exception', e)
            }
        }
    )

    return (
        <View className="mb-3">

            <View className="flex flex-row justify-between items-center px-3 mb-3">
                <Text className="text-2xl font-bold">{i18n.coorganiseList.title}</Text>
                    <TouchableOpacity
                        onPress={() => router.push('/coorganize')}
                    >
                        <Text className="text-primary text-sm">{i18n.coorganiseList.seeAll}</Text>
                    </TouchableOpacity>
            </View>

            <View
                className="relative w-full"
            >
                {status === 'success' && (
                    data?.data?.map((item: EventEventElement, index: number) => (
                        <View key={index} className={"px-3"}>
                            <CoorganizeListItem
                                item={item}
                                isLocked={false}
                                setIsOfferModalVisible={setIsOfferModalVisible}
                                setOfferModalUuid={setOfferModalUuid}
                                setOfferModalEventUuid={setOfferModalEventUuid}
                            />
                        </View>
                    ))
                )}
            </View>

            <EventCoorganiseModal
                isOfferModalVisible={isOfferModalVisible}
                setIsOfferModalVisible={setIsOfferModalVisible}
                eventUuid={offerModalEventUuid}
                offerModalUuid={offerModalUuid}
                setOfferModalUuid={setOfferModalUuid}
            />
        </View>
    );
};
