import Ionicons from '@expo/vector-icons/Ionicons';
import {Image} from 'expo-image';
import {Pressable, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import i18n from '../../i18n';
import {EventEventElement} from "../../types/EventEventElement.type";
import {getDaysLeft, getEventDates} from "../../helpers/date-helpers";
import {apiConfig} from "../../config/variables";
import {useApi} from "../../hooks/useApi";
import {MyUser} from "../../types/MyUser.type";
import {useAuthStore} from "../../store/AuthStore";
import {useState} from "react";
import {useRouter} from "expo-router";

type Props = {
    item: EventEventElement;
    isLocked?: boolean;
    setIsOfferModalVisible: (isVisible: boolean) => void;
    setOfferModalUuid: (uuid: string) => void;
    setOfferModalEventUuid?: (uuid: string) => void;
}

export const CoorganizeListItem = ({
                                       item,
                                       isLocked = false,
                                       setOfferModalUuid,
                                       setIsOfferModalVisible,
                                       setOfferModalEventUuid
                                   }: Props) => {
    const {saveCoorganizeItem, unsaveCoorganizeItem} = useApi();
    const router = useRouter();
    const userData: MyUser | undefined = useAuthStore(state => state.userData);
    const [isSaved, setIsSaved] = useState<boolean>(
        userData?.saved_event_event_elements?.find(uuid => uuid === item.uuid) !== undefined
    );

    const toggleFavorite = async () => {
        if (isSaved) {
            await unsaveCoorganizeItem(item.uuid);
        } else {
            await saveCoorganizeItem(item.uuid);
        }
        setIsSaved(!isSaved);
    }

    const coorganise = () => {
        if (setOfferModalEventUuid) {
            setOfferModalEventUuid(item.event.uuid);
        }
        setIsOfferModalVisible(true);
        setOfferModalUuid(item.uuid);
    }

    return (
        <View
            className="border border-borderGrey rounded-lg p-4 mb-3 flex flex-col bg-white shadow-md"
        >
            <View className="flex flex-row justify-between items-center">
                <View className="flex flex-row items-center">
                    <Pressable
                        onPress={() => {
                            router.push(`/event/${item.event.uuid}`)
                        }}
                    >
                        <Image
                            source={{uri: item?.event?.image?.url ?? `${apiConfig.publicUrl}/assets/event-bg.jpg`}}
                            className="w-12 h-12 rounded-md"
                        />
                    </Pressable>
                    <View className="ml-3 py-2 px-3 border border-primary rounded-md">
                        <Text className="color-primary">
                            {item?.event_element?.icon} {item?.event_element?.name}
                        </Text>
                    </View>
                </View>
                <Text className="color-primary">
                    {item.offer_price} {i18n.helpers.currency}
                </Text>
                <TouchableOpacity
                    onPress={toggleFavorite}
                >
                    <Ionicons name={isSaved ? 'heart' : 'heart-outline'} size={24} color="#952aa9"/>
                </TouchableOpacity>
            </View>
            <View className="flex flex-col mt-3">
                <Text className="color-primary">
                    {getEventDates(item.event.start_date, item?.event?.duration ?? null)}
                </Text>
                <Text className="font-bold text-lg font-bold mt-1">
                    {item.event.title}
                </Text>
                {!isLocked && (
                    <Text className={"text-sm"}>
                        {item.client_description}
                    </Text>
                )}
            </View>
            <View className="flex flex-row items-center mt-4">
                {item?.event?.city && (
                    <View className="flex flex-row items-center">
                        <Ionicons name="location-sharp" size={16} color="#FFEE4A"/>
                        <Text className="ml-1">
                            {item.event.city.name}
                        </Text>
                    </View>
                )}
                {item?.event?.start_date && (
                    <View className="flex flex-row items-center ml-3">
                        <Ionicons name="calendar-outline" size={16}/>
                        <Text className="ml-1">
                            {i18n.coorganiseListItem.daysLeft(getDaysLeft(item.event.start_date))}
                        </Text>
                    </View>
                )}
            </View>
            <View className="mt-3 pt-3 border-t border-borderGrey">
                <TouchableOpacity
                    className={`rounded-md ${isLocked ? 'border border-primary' : 'bg-primary'}`}
                    onPress={coorganise}
                >
                    <Text
                        className={`text-base font-bold py-3 px-3 text-center ${isLocked ? 'text-primary' : 'text-white'}`}>
                        {i18n.coorganiseListItem.sendOffer}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};
