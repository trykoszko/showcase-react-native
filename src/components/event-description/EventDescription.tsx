import {useState} from 'react';
import {Pressable, Text} from 'react-native';
import i18n from '../../i18n';

type Props = {
    description: string
}

export const EventDescription = ({description}: Props) => {
    const [isDescriptionVisible, setIsDescriptionVisible] = useState<boolean>(false);

    return (
        <Pressable
            className="flex flex-col mx-3 px-4 mb-3 py-5 bg-white border border-borderGrey rounded-2xl shadow-xl shadow-gray-200"
            onPress={() => setIsDescriptionVisible(!isDescriptionVisible)}
            // onLongPress={() => {
            //     Alert.alert(
            //         i18n.eventDescription.removeElement,
            //         undefined,
            //         [
            //             {
            //                 text: i18n.eventDescription.cancel,
            //                 style: 'cancel'
            //             },
            //             {
            //                 text: i18n.eventDescription.remove,
            //                 onPress: () => removeEventDescription(),
            //                 style: 'destructive'
            //             }
            //         ],
            //     );
            // }}
        >
            <Text
                className="text-lg leading-6 mb-3">{isDescriptionVisible ? description : `${description?.substring(0, 100)}...`}</Text>
            <Text
                className="font-bold text-darkGrey underline">{isDescriptionVisible ? i18n.eventDescription.showLess : i18n.eventDescription.showMore}</Text>
        </Pressable>
    );
};