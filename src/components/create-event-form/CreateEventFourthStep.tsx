import {Alert, Platform, Switch, Text, TextInput, TouchableOpacity, View} from 'react-native';
import i18n from '../../i18n';
import {useRouter} from 'expo-router';
import {EventStepLayout} from './EventStepLayout';
import Ionicons from '@expo/vector-icons/Ionicons';
import React, {useEffect, useState} from 'react';
import {ModalComponent} from '../modal/Modal';
import {ImageBackground} from 'expo-image';
import * as ImagePicker from 'expo-image-picker';
import Slider from '@react-native-community/slider';
import {formatHour} from '../../helpers/string-helpers';
import {CreateEventFormDataType} from '../../types/CreateEventFormDataType.type';
import DateTimePickerModal from "react-native-modal-datetime-picker"
import RNDateTimePicker from "@react-native-community/datetimepicker";
import {getFormattedDateString, getFormattedTimeString} from "../../helpers/date-helpers";

const bg = require('../../../assets/event-bg.jpg');

type Props = {
    setCurrentStep: (step: number) => void;
    formData: CreateEventFormDataType;
    setFormData: (formData: CreateEventFormDataType) => void;
}

export const CreateEventFourthStep = ({setCurrentStep, formData, setFormData}: Props) => {
    const router = useRouter();
    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
    const [isNextButtonDisabled, setIsNextButtonDisabled] = useState<boolean>(true);
    const [isHelpModalOpen, setIsHelpModalOpen] = useState<boolean>(false);
    const [helpModalTitle, setHelpModalTitle] = useState<string>('');
    const [helpModalContent, setHelpModalContent] = useState<string>('');
    const [eventDuration, setEventDuration] = useState<number>(1);
    const [datepickerVisible, setDatepickerVisible] = useState<boolean>(false)
    const [timepickerVisible, setTimepickerVisible] = useState<boolean>(false)

    useEffect(() => {
        setIsNextButtonDisabled(!(formData?.title && formData?.title?.length > 3));
    }, [formData.title]);

    useEffect(() => {
        setFormData({
            ...formData,
            duration: eventDuration
        });
    }, [eventDuration]);

    const pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.canceled) {
            setFormData({
                ...formData,
                image: result.assets[0].uri
            });
        }
    };

    return (
        <EventStepLayout
            noPadding={true}
            buttons={
                <>
                    <TouchableOpacity
                        className={`${isNextButtonDisabled ? 'bg-gray-400' : 'bg-primary'} rounded-md w-full`}
                        onPress={() => {
                            if (!formData.title || formData?.title?.length <= 3) {
                                Alert.alert(
                                    i18n.createEventFourthStep.error,
                                    i18n.createEventFourthStep.titleError,
                                );
                            }
                            if (!isNextButtonDisabled) {
                                setCurrentStep(5);
                            }
                        }}
                    >
                        <Text
                            className="text-base font-bold py-3 px-3 text-center text-white mt-auto"
                        >{i18n.createEventFourthStep.continue}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        className="w-full"
                        onPress={() => {
                            router.push('/home');
                        }}
                    >
                        <Text
                            className="text-base font-bold py-3 px-3 text-center text-red-600 mt-auto"
                        >{i18n.createEventFourthStep.leave}</Text>
                    </TouchableOpacity>
                </>
            }
            backButton={
                <TouchableOpacity
                    onPress={() => {
                        setCurrentStep(3);
                    }}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
            }
            rightButton={
                <TouchableOpacity
                    onPress={() => {
                        setIsModalOpen(true);
                    }}
                    className="border px-2 py-1 rounded-full border-gray-300"
                >
                    <Text>{i18n.createEventFourthStep.haveQuestions}</Text>
                </TouchableOpacity>
            }
        >
            <ImageBackground
                className={'bg-amber-300 py-24 px-4 flex flex-row items-center justify-center'}
                source={formData.image ?? bg}
            >
                <TouchableOpacity
                    onPress={pickImage}
                    className={'bg-white rounded-md py-3 px-4 flex flex-row items-center'}
                >
                    <Ionicons name={'cloud-upload-outline'} size={24} color={'black'}/>
                    <Text className={'ml-2'}>{i18n.createEventFourthStep.upload}</Text>
                </TouchableOpacity>
            </ImageBackground>
            <View className={'p-3 mt-4'}>

                <Text
                    className="font-bold mb-1 text-primary"
                >{i18n.createEventFourthStep.step} 3</Text>
                <Text
                    className="text-2xl font-bold mb-6"
                >{i18n.createEventFourthStep.title}</Text>

                <View className="mb-4">
                    <Text className="mb-2">{i18n.createEventFourthStep.title}</Text>
                    <TextInput
                        className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                        placeholder={i18n.createEventFourthStep.titlePlaceholder}
                        value={formData.title}
                        onChange={(e) => setFormData({
                            ...formData,
                            title: e.nativeEvent.text
                        })}
                    />
                </View>

                <View className="mb-4">
                    <Text className="mb-2">{i18n.createEventFourthStep.description}</Text>
                    <TextInput
                        className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                        multiline={true}
                        numberOfLines={6}
                        value={formData.description}
                        placeholder={i18n.createEventFourthStep.descriptionPlaceholder}
                        onChange={(e) => setFormData({
                            ...formData,
                            description: e.nativeEvent.text
                        })}
                    />
                </View>

                <View className="mb-4">
                    <Text className="mb-2">{i18n.createEventFourthStep.startDate}</Text>

                    {Platform.OS === 'ios' ? (
                        <RNDateTimePicker
                            value={formData.startDate ?? new Date()}
                            minimumDate={new Date()}
                            mode="datetime"
                            display="default"
                            onChange={(e, date) => {
                                if (date) {
                                    setFormData({
                                        ...formData,
                                        startDate: date
                                    });
                                }
                            }}
                        />
                    ) : (
                        <>
                            <View
                                className={"flex flex-row items-center justify-end"}
                            >
                                <TouchableOpacity
                                    onPress={() => setDatepickerVisible(true)}
                                    className={'border border-gray-300 rounded-md px-3 py-3 mr-3'}
                                >
                                    <Text>{getFormattedDateString(formData?.startDate ?? new Date())}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => setTimepickerVisible(true)}
                                    className={'border border-gray-300 rounded-md px-3 py-3'}
                                >
                                    <Text>{getFormattedTimeString(formData?.startDate ?? new Date())}</Text>
                                </TouchableOpacity>
                            </View>

                            <DateTimePickerModal
                                isVisible={datepickerVisible}
                                mode="date"
                                locale="pl"
                                is24Hour={true}
                                date={formData?.startDate ?? new Date()}
                                minimumDate={new Date()}
                                onConfirm={(val) => {
                                    if (val) {
                                        setFormData({
                                            ...formData,
                                            startDate: val
                                        });
                                    }
                                    setDatepickerVisible(false)
                                }}
                                onCancel={() => {
                                    setDatepickerVisible(false)
                                }}
                            />

                            <DateTimePickerModal
                                isVisible={timepickerVisible}
                                mode="time"
                                locale="pl"
                                is24Hour={true}
                                date={formData?.startDate ?? new Date()}
                                onConfirm={(val) => {
                                    if (val) {
                                        setFormData({
                                            ...formData,
                                            startDate: val
                                        });
                                    }
                                    setTimepickerVisible(false)
                                }}
                                onCancel={() => {
                                    setTimepickerVisible(false)
                                }}
                            />
                        </>
                    )}

                </View>

                <View className={'w-full h-0.5 bg-gray-200 mb-4'}/>

                <View className="mb-4 flex flex-row items-center justify-between">
                    <Text>{i18n.createEventFourthStep.unlimitedPeople}</Text>

                    <Switch
                        value={formData.unlimitedPeople ?? false}
                        onValueChange={(value) => setFormData({
                            ...formData,
                            unlimitedPeople: value
                        })}
                    />
                </View>

                {!formData.unlimitedPeople && (
                    <View className="mb-4 flex flex-row items-center justify-between">
                        <Text>{i18n.createEventFourthStep.peopleLimit}</Text>

                        <View className={'flex flex-row items-center gap-3'}>
                            <TouchableOpacity
                                onPress={() => {
                                    setFormData({
                                        ...formData,
                                        peopleLimit: formData.peopleLimit ? formData.peopleLimit - 1 : 9
                                    });
                                }}
                                className={'border border-gray-300 rounded-md px-3 py-3'}
                            >
                                <Text>-</Text>
                            </TouchableOpacity>
                            <TextInput
                                className="border border-gray-300 rounded-md px-4 py-3"
                                value={formData.peopleLimit?.toString() ?? '10'}
                                onChange={(e) => setFormData({
                                    ...formData,
                                    peopleLimit: +e.nativeEvent.text
                                })}
                            />
                            <TouchableOpacity
                                onPress={() => {
                                    setFormData({
                                        ...formData,
                                        peopleLimit: formData.peopleLimit ? formData.peopleLimit + 1 : 11
                                    });
                                }}
                                className={'border border-gray-300 rounded-md px-3 py-3'}
                            >
                                <Text>+</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )}

                <View className={'w-full h-0.5 bg-gray-200 mb-4'}/>

                <View className="mb-4 flex flex-row items-center justify-between">
                    <Text>{i18n.createEventFourthStep.duration}</Text>

                    <TouchableOpacity
                        className={'ml-auto mr-3'}
                        onPress={() => {
                            setHelpModalTitle(i18n.createEventFourthStep.duration);
                            setHelpModalContent(i18n.createEventFourthStep.durationHelpContent);
                            setIsHelpModalOpen(true);
                        }}
                    >
                        <Ionicons name={'help-circle-outline'} size={20} color={'black'}/>
                    </TouchableOpacity>
                    <Switch
                        value={formData.limitedDuration ?? false}
                        onValueChange={(value) => setFormData({
                            ...formData,
                            limitedDuration: value
                        })}
                    />
                </View>

                {formData.limitedDuration === true && (
                    <View className="mb-4 flex flex-row items-center justify-between">
                        <Slider
                            style={{width: 270, height: 40}}
                            minimumValue={1}
                            maximumValue={24}
                            step={1}
                            minimumTrackTintColor="#952aa9"
                            maximumTrackTintColor="#ececec"
                            value={eventDuration}
                            onValueChange={value => setEventDuration(value)}
                        />
                        <Text>{formatHour(eventDuration)}</Text>
                    </View>
                )}

                <View className="mb-4 flex flex-row items-center justify-between">
                    <Text>{i18n.createEventFourthStep.privateEvent}</Text>

                    <TouchableOpacity
                        className={'ml-auto mr-3'}
                        onPress={() => {
                            setHelpModalTitle(i18n.createEventFourthStep.privateEvent);
                            setHelpModalContent(i18n.createEventFourthStep.privateEventHelpContent);
                            setIsHelpModalOpen(true);
                        }}
                    >
                        <Ionicons name={'help-circle-outline'} size={20} color={'black'}/>
                    </TouchableOpacity>
                    <Switch
                        value={formData.privateEvent ?? false}
                        onValueChange={(value) => setFormData({
                            ...formData,
                            privateEvent: value
                        })}
                    />
                </View>

                <View className="mb-4 flex flex-row items-center justify-between">
                    <Text>{i18n.createEventFourthStep.reviewNeeded}</Text>

                    <TouchableOpacity
                        className={'ml-auto mr-3'}
                        onPress={() => {
                            setHelpModalTitle(i18n.createEventFourthStep.reviewNeeded);
                            setHelpModalContent(i18n.createEventFourthStep.reviewNeededHelpContent);
                            setIsHelpModalOpen(true);
                        }}
                    >
                        <Ionicons name={'help-circle-outline'} size={20} color={'black'}/>
                    </TouchableOpacity>
                    <Switch
                        value={formData.reviewNeeded ?? false}
                        onValueChange={(value) => setFormData({
                            ...formData,
                            reviewNeeded: value
                        })}
                    />
                </View>

                <View className="mb-4 flex flex-row items-center justify-between">
                    <Text>{i18n.createEventFourthStep.paidEvent}</Text>

                    <TouchableOpacity
                        className={'ml-auto mr-3'}
                        onPress={() => {
                            setHelpModalTitle(i18n.createEventFourthStep.paidEvent);
                            setHelpModalContent(i18n.createEventFourthStep.paidEventHelpContent);
                            setIsHelpModalOpen(true);
                        }}
                    >
                        <Ionicons name={'help-circle-outline'} size={20} color={'black'}/>
                    </TouchableOpacity>
                    <Switch
                        value={formData.paidEvent ?? false}
                        onValueChange={(value) => setFormData({
                            ...formData,
                            paidEvent: value
                        })}
                    />
                </View>

                <View className="mb-4 flex flex-row items-center justify-between">
                    <Text>{i18n.createEventFourthStep.canJoinInProgress}</Text>

                    <TouchableOpacity
                        className={'ml-auto mr-3'}
                        onPress={() => {
                            setHelpModalTitle(i18n.createEventFourthStep.canJoinInProgress);
                            setHelpModalContent(i18n.createEventFourthStep.canJoinInProgressHelpContent);
                            setIsHelpModalOpen(true);
                        }}
                    >
                        <Ionicons name={'help-circle-outline'} size={20} color={'black'}/>
                    </TouchableOpacity>
                    <Switch
                        value={formData.canJoinInProgress ?? false}
                        onValueChange={(value) => setFormData({
                            ...formData,
                            canJoinInProgress: value
                        })}
                    />
                </View>
            </View>

            <ModalComponent
                isVisible={isModalOpen}
                setIsVisible={setIsModalOpen}
                title={i18n.createEventFourthStep.helpTitle}
                content={(
                    <View className="flex-col">
                        <Text>
                            {i18n.createEventFourthStep.helpDescription}
                        </Text>
                    </View>
                )}
            />
            <ModalComponent
                isVisible={isHelpModalOpen}
                setIsVisible={setIsHelpModalOpen}
                title={helpModalTitle}
                content={(
                    <View className="flex-col">
                        <Text>
                            {helpModalContent}
                        </Text>
                    </View>
                )}
            />
        </EventStepLayout>
    );
};
