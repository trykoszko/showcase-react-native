import {EventTemplate} from '../../types/EventTemplate.type';
import {Text, TouchableOpacity, View} from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import React from 'react';

type Props = {
    template: EventTemplate;
    chosenEventTemplate: EventTemplate | undefined;
    setChosenEventTemplate: (eventTemplate: EventTemplate) => void;
}

export const EventTemplateBox = ({template, chosenEventTemplate, setChosenEventTemplate}: Props) =>
{
    const isActive = chosenEventTemplate?.id === template.id;

    const onPress = () =>
    {
        setChosenEventTemplate(template);
    };

    return (
        <TouchableOpacity
            onPress={onPress}
            className={'mt-3'}
            key={template.id}
        >
            <View
                className={`border rounded-lg flex flex-row items-center justify-between p-4 ${isActive ? 'bg-pink-100 border-primary' : 'border-gray-200'}`}
            >
                <View className={'flex flex-row items-center w-full'}>
                    <Ionicons name={template.icon ?? 'aperture'} size={24} color={'black'}/>
                    <Text className={'ml-3'}>{template.title}</Text>
                    <View
                        className={`ml-auto flex flex-row items-center justify-center w-6 h-6 rounded-md border ${isActive ? 'border-primary bg-primary' : 'border-gray-200'}`}
                    >
                        {isActive ? (
                            <Ionicons name={'checkmark'} size={20} color={'white'}/>
                        ) : (
                             <></>
                         )}
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
};