import {Alert, ScrollView, Text, TextInput, TouchableOpacity, View} from 'react-native';
import i18n from '../../i18n';
import {useRouter} from 'expo-router';
import {EventStepLayout} from './EventStepLayout';
import Ionicons from '@expo/vector-icons/Ionicons';
import React, {useEffect, useRef, useState} from 'react';
import {ModalComponent} from '../modal/Modal';
import {EventElement} from '../../types/EventElement.type';
import {useGlobalLoaderStore} from '../../store/GlobalLoaderStore';
import {Provider} from '../../types/Provider.type';
import {Image} from 'expo-image';
import {CreateEventFormDataType} from '../../types/CreateEventFormDataType.type';

type Props = {
    setCurrentStep: (step: number) => void;
    formData: CreateEventFormDataType;
    setFormData: (formData: CreateEventFormDataType) => void;
    submitForm: () => void;
}

export const CreateEventFifthStep = ({setCurrentStep, formData, setFormData, submitForm}: Props) =>
{
    const router                                                                    = useRouter();
    const [isModalOpen, setIsModalOpen]                                             = useState<boolean>(false);
    const setIsLoading                                                              = useGlobalLoaderStore(state => state.setIsLoading);
    const [currentElement, setCurrentElement]                                       = useState<EventElement | undefined>(formData?.eventElements?.length ? formData.eventElements[0] : undefined);
    const [currentElementStep, setCurrentElementStep]                               = useState<number>(0);
    const [currentElementClientHas, setCurrentElementClientHas]                     = useState<boolean>(false);
    const [currentElementClientDescription, setCurrentElementClientDescription]     = useState<string | null>(null);
    const [currentElementChosenProviderUuids, setCurrentElementChosenProviderUuids] = useState<string[]>([]);
    // @TODO: add providers
    const [providers, setProviders]                                                 = useState<Provider[]>([]);

    const scrollViewRef = useRef<ScrollView>(null);

    const scrollToTop = () =>
    {
        if (scrollViewRef?.current) {
            scrollViewRef.current?.scrollTo({
                                                y:        0,
                                                animated: true
                                            });
        }
    };

    const submit = async () =>
    {
        setIsLoading(true);
        await submitForm();
    };

    useEffect(() =>
              {
                  if (!currentElement) {
                      submit();
                  }
              }, []);

    useEffect(() =>
              {
                  setCurrentElementClientHas(false);
                  setCurrentElementChosenProviderUuids([]);
                  setCurrentElementClientDescription(null);
              }, [currentElementStep]);

    useEffect(() =>
              {
                  setFormData({
                                  ...formData,
                                  eventElements: formData?.eventElements?.map((element, index) =>
                                                                              {
                                                                                  if (index === currentElementStep) {
                                                                                      return {
                                                                                          ...element,
                                                                                          clientNeed: currentElementClientHas
                                                                                      };
                                                                                  }

                                                                                  return element;
                                                                              })
                              });
              }, [currentElementClientHas]);

    useEffect(() =>
              {
                  setFormData({
                                  ...formData,
                                  eventElements: formData?.eventElements?.map((element, index) =>
                                                                              {
                                                                                  if (index === currentElementStep) {
                                                                                      return {
                                                                                          ...element,
                                                                                          clientDescription: currentElementClientDescription
                                                                                      };
                                                                                  }

                                                                                  return element;
                                                                              })
                              });
              }, [currentElementClientDescription]);

    useEffect(() =>
              {
                  setFormData({
                                  ...formData,
                                  eventElements: formData?.eventElements?.map((element, index) =>
                                                                              {
                                                                                  if (index === currentElementStep) {
                                                                                      return {
                                                                                          ...element,
                                                                                          currentElementChosenProviderUuids: currentElementChosenProviderUuids
                                                                                      };
                                                                                  }

                                                                                  return element;
                                                                              })
                              });
              }, [currentElementChosenProviderUuids]);

    return (
        <EventStepLayout
            scrollViewRef={scrollViewRef}
            buttons={
                <>
                    <TouchableOpacity
                        className="bg-primary rounded-md w-full"
                        onPress={async () =>
                        {
                            if (formData?.eventElements?.length) {
                                if (currentElementStep === (formData.eventElements.length - 1)) {
                                    submit();
                                } else {
                                    scrollToTop();
                                    setCurrentElementStep(currentElementStep + 1);
                                    setCurrentElement(formData.eventElements[currentElementStep + 1]);
                                }
                            }
                        }}
                    >
                        <Text
                            className="text-base font-bold py-3 px-3 text-center text-white mt-auto"
                        >{i18n.createEventFifthStep.continue}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        className="w-full"
                        onPress={() =>
                        {
                            router.push('/home');
                        }}
                    >
                        <Text
                            className="text-base font-bold py-3 px-3 text-center text-red-600 mt-auto"
                        >{i18n.createEventFifthStep.leave}</Text>
                    </TouchableOpacity>
                </>
            }
            backButton={
                <TouchableOpacity
                    onPress={() =>
                    {
                        if (formData?.eventElements?.length && currentElementStep > 1) {
                            setCurrentElementStep(currentElementStep - 1);
                            setCurrentElement(formData.eventElements[currentElementStep]);
                        } else {
                            setCurrentStep(4);
                        }
                    }}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
            }
            rightButton={
                <TouchableOpacity
                    onPress={() =>
                    {
                        setIsModalOpen(true);
                    }}
                    className="border px-2 py-1 rounded-full border-gray-300"
                >
                    <Text>{i18n.createEventFifthStep.haveQuestions}</Text>
                </TouchableOpacity>
            }
        >
            {currentElement?.name ? (
                <>
                    <Text
                        className="font-bold mb-1 text-primary"
                    >{i18n.createEventFifthStep.step} {4 + currentElementStep}</Text>

                    <Text
                        className="text-2xl font-bold mb-3"
                    >{currentElement.name}</Text>

                    <TouchableOpacity
                        onPress={() =>
                        {
                            setCurrentElementClientHas(true);
                            setCurrentElementChosenProviderUuids([]);
                            setCurrentElementClientDescription(null);
                        }}
                        className={`w-full p-4 rounded-md border border-gray-200 flex flex-row items-center mb-3 ${currentElementClientHas ? 'bg-primary' : ''}`}
                    >
                        <Ionicons name={'checkmark'} size={24} color={currentElementClientHas ? 'white' : 'black'}/>
                        <Text className={`ml-3 ${currentElementClientHas ? 'text-white' : ''}`}>{i18n.createEventFifthStep.iHave}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        onPress={() =>
                        {
                            setCurrentElementClientHas(false);
                            setCurrentElementChosenProviderUuids([]);
                            setCurrentElementClientDescription(null);
                        }}
                        className={`w-full p-4 rounded-md border border-gray-200 flex flex-row items-center ${currentElementClientHas ? '' : 'bg-primary'}`}
                    >
                        <Ionicons name={'close'} size={24} color={currentElementClientHas ? 'black' : 'white'}/>
                        <Text className={`ml-3 ${currentElementClientHas ? '' : 'text-white'}`}>{i18n.createEventFifthStep.iDontHave}</Text>
                    </TouchableOpacity>

                    {currentElementClientHas ? (
                        <View
                            className={'flex flex-col w-full mt-6 mb-2'}
                        >
                            <Text>Opis</Text>
                            <TextInput
                                className="mt-1 block w-full border border-gray-300 rounded-md px-2 py-3"
                                multiline={true}
                                numberOfLines={10}
                                placeholder={'Mój lokal ma 60 metrów kwadratowych...'}
                                autoFocus={true}
                                onChange={(event) =>
                                {
                                    setCurrentElementClientDescription(event.nativeEvent.text);
                                }}
                            />
                        </View>
                    ) : (
                         <>
                             <View
                                 className={'flex flex-row justify-between items-center mt-6 mb-2'}
                             >
                                 <Text
                                     className="text-2xl font-bold"
                                 >Usługodawcy</Text>
                                 <TouchableOpacity
                                     onPress={() =>
                                     {
                                         Alert.alert('all providers');
                                     }}
                                 >
                                     <Text className={'text-primary'}>{i18n.createEventFifthStep.seeAll}</Text>
                                 </TouchableOpacity>
                             </View>
                             <Text
                                 className={'mb-4'}
                             >
                                 Wybierz firmy z którymi chciałbyś współpracować w ramach twojego eventu.
                             </Text>
                             <View
                                 className={'flex flex-col w-full'}
                             >
                                 {providers?.map((provider, index) =>
                                                 {
                                                     const isChosen = currentElementChosenProviderUuids.includes('' + provider.uuid);

                                                     return (
                                                         <TouchableOpacity
                                                             key={index}
                                                             onPress={() =>
                                                             {
                                                                 if (isChosen) {
                                                                     setCurrentElementChosenProviderUuids(currentElementChosenProviderUuids.filter(uuid => uuid !== '' + provider.uuid));
                                                                 } else {
                                                                     setCurrentElementChosenProviderUuids([...currentElementChosenProviderUuids, '' + provider.uuid]);
                                                                 }
                                                             }}
                                                             className={`flex flex-row justify-between items-center border-b border-gray-200 py-3`}
                                                         >
                                                             {provider.image ? (
                                                                 <Image
                                                                     source={{uri: provider.image}}
                                                                     style={{width: 50, height: 50}}
                                                                     className={'rounded-full'}
                                                                 />
                                                             ) : (
                                                                  <View
                                                                      style={{width: 50, height: 50}}
                                                                      className={'rounded-full bg-gray-200 flex flex-row items-center justify-center'}
                                                                  >
                                                                      <Ionicons name={'ios-checkmark-done'} size={24}
                                                                                color={'black'}/>
                                                                  </View>
                                                              )}
                                                             <Text
                                                                 className={'ml-3 mr-auto'}
                                                             >{provider.title}</Text>
                                                             {isChosen && (
                                                                 <View
                                                                     className={'flex flex-row items-center bg-primary rounded-md p-1'}
                                                                 >
                                                                     <Ionicons name={'checkmark'} size={16}
                                                                               color={'white'}/>
                                                                 </View>
                                                             )}
                                                         </TouchableOpacity>
                                                     );
                                                 })}
                             </View>
                         </>
                     )}

                </>
            ) : (<></>)}

            <ModalComponent
                isVisible={isModalOpen}
                setIsVisible={setIsModalOpen}
                title={i18n.createEventFifthStep.helpTitle}
                content={(
                    <View className="flex-col">
                        <Text>
                            {i18n.createEventFifthStep.helpDescription}
                        </Text>
                    </View>
                )}
            />
        </EventStepLayout>
    );
};
