import {Alert, Text, TextInput, TouchableOpacity, View} from 'react-native';
import i18n from '../../i18n';
import {EventStepLayout} from './EventStepLayout';
import {useRouter} from 'expo-router';
import Ionicons from '@expo/vector-icons/Ionicons';
import React, {useEffect, useState} from 'react';
import {ModalComponent} from '../modal/Modal';
import {EventTemplate} from '../../types/EventTemplate.type';
import Loader from '../loader/Loader';
import {EventTemplateBox} from './EventTemplateBox';
import {CreateEventFormDataType} from '../../types/CreateEventFormDataType.type';

type Props = {
    setCurrentStep: (step: number) => void;
    formData: CreateEventFormDataType;
    setFormData: (formData: CreateEventFormDataType) => void;
}

export const CreateEventSecondStep = ({setCurrentStep, formData, setFormData}: Props) =>
{
    const router                                              = useRouter();
    const [isHelpModalOpen, setIsHelpModalOpen]               = useState<boolean>(false);
    const [isTemplatesModalOpen, setIsTemplatesModalOpen]     = useState<boolean>(false);
    const [searchQuery, setSearchQuery]                       = useState<string>();
    const [eventTemplates, setEventTemplates]                 = useState<EventTemplate[]>();
    const [filteredEventTemplates, setFilteredEventTemplates] = useState<EventTemplate[]>();
    const [myEventTemplates, setMyEventTemplates]             = useState<EventTemplate[]>();
    const [chosenEventTemplate, setChosenEventTemplate]       = useState<EventTemplate>();
    const [isLoading, setIsLoading]                           = useState<boolean>(false);
    const [isNextButtonDisabled, setIsNextButtonDisabled]     = useState<boolean>(true);

    useEffect(() =>
              {
                  setIsLoading(true);
                  // query for eventTemplates
                  setTimeout(() =>
                             {
                                 // @TODO: fetch for real event elements
                                 // setEventTemplates(eventTemplatesMock);
                                 // setMyEventTemplates(myEventTemplatesMock);
                                 setIsLoading(false);
                             }, 1000);
              }, []);

    useEffect(() =>
              {
                  if (chosenEventTemplate) {
                      // setFormData({
                      //                 ...formData,
                      //                 eventTemplate: chosenEventTemplate,
                      //                 eventElements: chosenEventTemplate.elements
                      //             });
                      // @TODO: fix
                      setIsNextButtonDisabled(false);
                  }
              }, [chosenEventTemplate]);

    useEffect(() =>
              {
                  // if (searchQuery && searchQuery.length > 1) {
                  //     setFilteredEventTemplates(eventTemplates?.filter((template: EventTemplate) =>
                  //                                                          template.title.toLowerCase()
                  //                                                                  .includes(searchQuery.toLowerCase())));
                  //
                  // } else {
                  //     setFilteredEventTemplates(eventTemplates);
                  // }
                  // @TODO: fix
              }, [searchQuery]);

    return (
        <EventStepLayout
            buttons={
                <>
                    <TouchableOpacity
                        className={`${isNextButtonDisabled ? 'bg-gray-400' : 'bg-primary'} rounded-md w-full`}
                        onPress={() =>
                        {
                            if (isNextButtonDisabled) {
                                Alert.alert(
                                    i18n.createEventSecondStep.errorTitle,
                                    i18n.createEventSecondStep.errorDescription,
                                );
                            } else {
                                setCurrentStep(3);
                            }
                        }}
                    >
                        <Text
                            className="text-base font-bold py-3 px-3 text-center text-white mt-auto"
                        >{i18n.createEventSecondStep.continue}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        className="w-full"
                        onPress={() =>
                        {
                            router.push('/home');
                        }}
                    >
                        <Text
                            className="text-base font-bold py-3 px-3 text-center text-red-600 mt-auto"
                        >{i18n.createEventSecondStep.leave}</Text>
                    </TouchableOpacity>
                </>
            }
            backButton={
                <TouchableOpacity
                    onPress={() =>
                    {
                        setCurrentStep(1);
                    }}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
            }
            rightButton={
                <TouchableOpacity
                    onPress={() =>
                    {
                        setIsHelpModalOpen(true);
                    }}
                    className="border px-2 py-1 rounded-full border-gray-300"
                >
                    <Text>{i18n.createEventSecondStep.haveQuestions}</Text>
                </TouchableOpacity>
            }
        >
            <Text
                className="font-bold mb-1 text-primary"
            >{i18n.createEventSecondStep.step} 1</Text>
            <Text
                className="text-2xl font-bold mb-2"
            >{i18n.createEventSecondStep.title}</Text>
            <Text
                className="text-base"
            >{i18n.createEventSecondStep.description}</Text>

            {isLoading ? (
                <View
                    className={'flex flex-row justify-center items-center mt-24'}
                >
                    <Loader/>
                </View>
            ) : (
                 <>
                     <TouchableOpacity
                         onPress={() =>
                         {
                             setIsTemplatesModalOpen(true);
                         }}
                         className={'mt-5'}
                     >
                         <View
                             className={'border border-gray-200 rounded-lg flex flex-row items-center justify-between p-4'}>
                             <View className={'flex flex-row items-center'}>
                                 <Ionicons name={'document-text-outline'} size={24} color={'black'}/>
                                 <Text className={'ml-3'}>{i18n.createEventSecondStep.templatesTitle}</Text>
                             </View>
                             <Ionicons name={'chevron-down'} size={24} color={'black'}/>
                         </View>
                     </TouchableOpacity>

                     <View
                         className={'mt-5 pt-5 pb-2 border-t border-t-gray-200 relative'}
                     >
                         <View
                             className={'flex-row items-center border border-gray-300 rounded-l-full rounded-r-full p-3'}
                         >
                             <TextInput
                                 placeholder={i18n.createEventSecondStep.templatesSearch}
                                 className={'flex-grow'}
                                 onChange={(e) =>
                                 {
                                     setSearchQuery(e.nativeEvent.text);
                                 }}
                             />
                             <Ionicons
                                 name={'search'}
                                 size={24}
                                 color={'gray'}
                             />
                         </View>
                     </View>

                     {(searchQuery && filteredEventTemplates) ? filteredEventTemplates.map((template: EventTemplate) => (
                         <EventTemplateBox
                             key={template.id}
                             template={template}
                             chosenEventTemplate={chosenEventTemplate}
                             setChosenEventTemplate={setChosenEventTemplate}
                         />
                     )) : eventTemplates?.map((template: EventTemplate) => (
                         <EventTemplateBox
                             key={template.id}
                             template={template}
                             chosenEventTemplate={chosenEventTemplate}
                             setChosenEventTemplate={setChosenEventTemplate}
                         />
                     ))}
                 </>
             )}

            <ModalComponent
                isVisible={isTemplatesModalOpen}
                setIsVisible={setIsTemplatesModalOpen}
                title={i18n.createEventSecondStep.templatesTitle}
                content={(
                    <View className="flex-col">
                        {myEventTemplates && myEventTemplates.map((template: EventTemplate) => (
                            <TouchableOpacity
                                onPress={() =>
                                {
                                    setChosenEventTemplate(template);
                                    setIsTemplatesModalOpen(false);
                                }}
                                className={'mt-3'}
                                key={template.id}
                            >
                                <View
                                    className={`border rounded-lg flex flex-row items-center justify-between p-4 ${chosenEventTemplate?.id === template.id ? 'bg-pink-100 border-primary' : 'border-gray-200'}`}
                                >
                                    <View className={'flex flex-row items-center'}>
                                        {/*<Ionicons name={template.icon ?? 'aperture'} size={24} color={'black'}/>*/}
                                        {/*<Text className={'ml-3'}>{template.title}</Text>*/}
                                    {/*    @TODO: fix */}
                                    </View>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                )}
            />
            <ModalComponent
                isVisible={isHelpModalOpen}
                setIsVisible={setIsHelpModalOpen}
                title={i18n.createEventSecondStep.helpTitle}
                content={(
                    <View className="flex-col">
                        <Text>
                            {i18n.createEventSecondStep.helpDescription}
                        </Text>
                    </View>
                )}
            />
        </EventStepLayout>
    );
};
