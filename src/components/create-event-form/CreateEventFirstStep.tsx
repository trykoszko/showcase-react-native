import {Text, TouchableOpacity, View} from 'react-native';
import {Image} from 'expo-image';
import i18n from '../../i18n';
import React from 'react';
import {EventStepLayout} from './EventStepLayout';
import Ionicons from '@expo/vector-icons/Ionicons';
import {useRouter} from 'expo-router';
import {CreateEventFormDataType} from '../../types/CreateEventFormDataType.type';

const eventCreatorImage = require('../../../assets/event-creator.png');

type Props = {
    setCurrentStep: (step: number) => void;
    formData: CreateEventFormDataType;
    setFormData: (formData: CreateEventFormDataType) => void;
}

export const CreateEventFirstStep = ({setCurrentStep, formData, setFormData}: Props) =>
{
    const router = useRouter();

    return (
        <EventStepLayout
            buttons={
                <TouchableOpacity
                    className="bg-primary rounded-md w-full"
                    onPress={() =>
                    {
                        setCurrentStep(2);
                    }}
                >
                    <Text
                        className="text-base font-bold py-3 px-3 text-center text-white mt-auto"
                    >{i18n.createEventFirstStep.start}</Text>
                </TouchableOpacity>
            }
            backButton={
                <TouchableOpacity
                    onPress={() =>
                    {
                        router.push('/home');
                    }}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
            }
        >
            <View className="flex flex-col justify-center items-center grow py-24">
                <Image
                    source={eventCreatorImage}
                    style={{
                        width:  100,
                        height: 100
                    }}
                    className="mb-6"
                />
                <Text
                    className="text-2xl font-bold mb-1 text-center"
                >{i18n.createEventFirstStep.title}</Text>
                <Text
                    className="text-base text-center text-primary"
                >{i18n.createEventFirstStep.subtitle}</Text>
            </View>
        </EventStepLayout>
    );
};
