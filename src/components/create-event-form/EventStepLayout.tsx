import {ScrollView, View} from 'react-native';
import Layout from '../layout/Layout';
import React from 'react';

type Props = {
    children: React.ReactNode;
    buttons: React.ReactNode;
    backButton?: React.ReactNode;
    title?: string;
    rightButton?: React.ReactNode;
    noPadding?: boolean,
    scrollViewRef?: React.RefObject<ScrollView>
}

export const EventStepLayout = ({children, buttons, backButton, title, rightButton, noPadding, scrollViewRef}: Props) =>
{
    return (
        <Layout
            backButton={backButton}
            title={title}
            rightButton={rightButton}
        >
            <View
                className="flex flex-col justify-between items-stretch h-full relative"
            >
                <ScrollView
                    ref={scrollViewRef ?? undefined}
                    className="flex flex-col"
                >
                    <View
                        className={noPadding ? '' : 'py-6 px-3'}
                        style={{
                            paddingBottom: 280
                        }}
                    >
                        {children}
                    </View>
                </ScrollView>
                <View
                    className="flex flex-col justify-stretch w-full pb-6 pt-3 bg-white px-3 border-t border-t-gray-200 pb-10"
                    style={{
                        position: 'absolute',
                        bottom:   100,
                        left:     0
                    }}
                >
                    {buttons}
                </View>
            </View>
        </Layout>
    );
};
