import {EventElement} from '../../types/EventElement.type';
import {Text, TouchableOpacity, View} from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import React from 'react';

type Props = {
    element: EventElement;
    chosenEventElements: EventElement[];
    setChosenEventElements: (eventElements: EventElement[]) => void;
}

export const EventElementBox = ({element, chosenEventElements, setChosenEventElements}: Props) =>
{
    const isActive = chosenEventElements.map(element => element.id).includes(element.id);

    const onPress = () =>
    {
        if (chosenEventElements?.map(item => item.slug)?.includes(element.slug))
        {
            setChosenEventElements(chosenEventElements.filter((e) => e.slug !== element.slug));
        } else {
            setChosenEventElements([...chosenEventElements, element]);
        }
    };

    return (
        <TouchableOpacity
            onPress={onPress}
            className={'mt-3'}
            key={element.id}
        >
            <View
                className={`border rounded-lg flex flex-row items-center justify-between p-4 ${isActive ? 'bg-pink-100 border-primary' : 'border-gray-200'}`}
            >
                <View className={'flex flex-row items-center w-full'}>
                    <Ionicons name={element.icon ?? 'aperture'} size={24} color={'black'}/>
                    <Text className={'ml-3'}>{element.title}</Text>
                    <View
                        className={`ml-auto flex flex-row items-center justify-center w-6 h-6 rounded-md border ${isActive ? 'border-primary bg-primary' : 'border-gray-200'}`}
                    >
                        {isActive ? (
                            <Ionicons name={'checkmark'} size={20} color={'white'}/>
                        ) : (
                             <></>
                         )}
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
};