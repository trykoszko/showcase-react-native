import {Text, TextInput, TouchableOpacity, View} from 'react-native';
import i18n from '../../i18n';
import {useRouter} from 'expo-router';
import {EventStepLayout} from './EventStepLayout';
import Ionicons from '@expo/vector-icons/Ionicons';
import React, {useEffect, useState} from 'react';
import {ModalComponent} from '../modal/Modal';
import {EventElement} from '../../types/EventElement.type';
import Loader from '../loader/Loader';
import {EventElementBox} from './EventElementBox';
import {CreateEventFormDataType} from '../../types/CreateEventFormDataType.type';

type Props = {
    setCurrentStep: (step: number) => void;
    formData: CreateEventFormDataType;
    setFormData: (formData: CreateEventFormDataType) => void;
}

export const CreateEventThirdStep = ({setCurrentStep, formData, setFormData}: Props) =>
{
    const router                                            = useRouter();
    const [isModalOpen, setIsModalOpen]                     = useState<boolean>(false);
    const [searchQuery, setSearchQuery]                     = useState<string>('');
    const [eventElements, setEventElements]                 = useState<EventElement[]>([]);
    const [chosenEventElements, setChosenEventElements]     = useState<EventElement[]>([]);
    const [filteredEventElements, setFilteredEventElements] = useState<EventElement[]>();
    const [isLoading, setIsLoading]                         = useState<boolean>(false);

    useEffect(() =>
              {
                  setIsLoading(true);

                  setTimeout(() =>
                             {
                                 // @TODO:
                                 // setEventElements(eventElementsMock);
                                 if (formData?.eventElements?.length) {
                                     setChosenEventElements(formData.eventElements);
                                 }
                                 setIsLoading(false);
                             }, 1000);
              }, []);

    useEffect(() =>
              {
                  if (chosenEventElements?.length) {
                      setFormData({
                                      ...formData,
                                      eventElements: chosenEventElements
                                  });
                  }
              }, [chosenEventElements]);

    useEffect(() =>
              {
                  if (searchQuery && searchQuery.length > 1) {
                      setFilteredEventElements(eventElements?.filter((element: EventElement) =>
                                                                         element.name.toLowerCase()
                                                                                .includes(searchQuery.toLowerCase())));

                  } else {
                      setFilteredEventElements(eventElements);
                  }
              }, [searchQuery]);

    return (
        <EventStepLayout
            buttons={
                <>
                    <TouchableOpacity
                        className="bg-primary rounded-md w-full"
                        onPress={() =>
                        {
                            setCurrentStep(4);
                        }}
                    >
                        <Text
                            className="text-base font-bold py-3 px-3 text-center text-white mt-auto"
                        >{i18n.createEventThirdStep.continue}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        className="w-full"
                        onPress={() =>
                        {
                            router.push('/home');
                        }}
                    >
                        <Text
                            className="text-base font-bold py-3 px-3 text-center text-red-600 mt-auto"
                        >{i18n.createEventThirdStep.leave}</Text>
                    </TouchableOpacity>
                </>
            }
            backButton={
                <TouchableOpacity
                    onPress={() =>
                    {
                        setCurrentStep(2);
                    }}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
            }
            rightButton={
                <TouchableOpacity
                    onPress={() =>
                    {
                        setIsModalOpen(true);
                    }}
                    className="border px-2 py-1 rounded-full border-gray-300"
                >
                    <Text>{i18n.createEventThirdStep.haveQuestions}</Text>
                </TouchableOpacity>
            }
        >
            <Text
                className="font-bold mb-1 text-primary"
            >{i18n.createEventThirdStep.step} 2</Text>
            <Text
                className="text-2xl font-bold mb-2"
            >{i18n.createEventThirdStep.title}</Text>
            <Text
                className="text-base"
            >{i18n.createEventThirdStep.description}</Text>

            {isLoading ? (
                <View
                    className={'flex flex-row justify-center items-center mt-24'}
                >
                    <Loader/>
                </View>
            ) : (
                 <>
                     <View
                         className={'mt-5 pt-5 pb-2 border-t border-t-gray-200 relative'}
                     >
                         <View
                             className={'flex-row items-center border border-gray-300 rounded-l-full rounded-r-full p-3'}
                         >
                             <TextInput
                                 placeholder={i18n.createEventThirdStep.elementsSearch}
                                 className={'flex-grow'}
                                 onChange={(e) =>
                                 {
                                     setSearchQuery(e.nativeEvent.text);
                                 }}
                             />
                             <Ionicons
                                 name={'search'}
                                 size={24}
                                 color={'gray'}
                             />
                         </View>
                     </View>

                     {(searchQuery && filteredEventElements) ? filteredEventElements.map((element: EventElement, key: number) => (
                         <EventElementBox
                             key={key}
                             element={element}
                             chosenEventElements={chosenEventElements}
                             setChosenEventElements={setChosenEventElements}
                         />
                     )) : eventElements.map((element: EventElement, key: number) => (
                         <EventElementBox
                             key={key}
                             element={element}
                             chosenEventElements={chosenEventElements}
                             setChosenEventElements={setChosenEventElements}
                         />
                     ))}
                 </>
             )}

            <ModalComponent
                isVisible={isModalOpen}
                setIsVisible={setIsModalOpen}
                title={i18n.createEventThirdStep.helpTitle}
                content={(
                    <View className="flex-col">
                        <Text>
                            {i18n.createEventThirdStep.helpDescription}
                        </Text>
                    </View>
                )}
            />
        </EventStepLayout>
    );
};
