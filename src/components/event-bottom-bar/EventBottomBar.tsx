import {Alert, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import i18n from '../../i18n';
import {useAuthStore} from '../../store/AuthStore';
import React from 'react';
import Ionicons from '@expo/vector-icons/Ionicons';
import {UserEvent} from "../../types/UserEvent.type";
import {MyUser} from "../../types/MyUser.type";
import {EventUserRoleEnum} from "../../enums/EventUserRole.enum";
import {useRouter} from "expo-router";
import {useApi} from "../../hooks/useApi";
import {EventDetail} from "../../types/EventDetail.type";

type Props = {
    event: EventDetail,
    refetchEvent: () => void
}

export const EventBottomBar = ({event, refetchEvent}: Props) => {
    const router = useRouter();
    const {deleteEvent, leaveEvent, joinEvent} = useApi();
    const eventUuid = event.uuid;

    const userData: MyUser | undefined = useAuthStore(store => store.userData);

    const ownedEvents: string[] = userData
        ?.events
        ?.filter((event: UserEvent) => event.role === EventUserRoleEnum.OWNER)
        ?.map((event: UserEvent) => event?.event?.uuid || '') || [];

    const joinedEvents: string[] = userData
        ?.events
        ?.filter((event: UserEvent) => [EventUserRoleEnum.OWNER, EventUserRoleEnum.COORGANIZER, EventUserRoleEnum.PARTICIPANT].includes(event.role))
        ?.map((event: UserEvent) => event?.event?.uuid || '') || [];

    const pendingEvents: string[] = userData
        ?.events
        ?.filter((event: UserEvent) => event.role === EventUserRoleEnum.PENDING_APPROVAL)
        ?.map((event: UserEvent) => event?.event?.uuid || '') || [];

    const isOwner: boolean = ownedEvents?.includes(eventUuid);
    const isJoined: boolean = joinedEvents?.includes(eventUuid);
    const isPending: boolean = pendingEvents?.includes(eventUuid);

    return (
        <>
            {isOwner && (
                <View
                    className="absolute bottom-8 right-5 w-14 h-36"
                >
                    <TouchableHighlight
                        style={{
                            width: 56,
                            height: 56,
                            borderRadius: 56,
                            backgroundColor: '#a31caf',
                            elevation: 2,
                            shadowColor: 'black',
                            shadowOpacity: .4,
                            shadowOffset: {
                                width: 0,
                                height: 4
                            }
                        }}
                        onPress={() => Alert.alert('add element')}
                        className="flex-row items-center justify-center bg-primary rounded-full"
                    >
                        <Ionicons
                            name="add"
                            size={44}
                            color="white"
                            style={{
                                marginLeft: 3
                            }}
                        />
                    </TouchableHighlight>
                </View>
            )}

            <View
                className="absolute bottom-0 left-0 w-full flex-row items-center justify-between pb-10 pt-3 px-4 border-t border-borderGrey bg-white"
            >
                <View className="flex-row items-center w-full">
                    {isOwner ? (
                        <>
                            <TouchableOpacity
                                className="w-1/2 flex-row items-center justify-center"
                                onPress={() => {
                                    router.push(`/(auth)/edit-event/${eventUuid}`)
                                }}
                            >
                                <Ionicons name={'construct'} size={24} color="black"/>
                                <Text
                                    className={`text-base font-bold py-3 px-3 text-center`}>
                                    {i18n.eventBottomBar.edit}
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                className="w-1/2 flex-row items-center justify-center"
                                onPress={() => {
                                    Alert.alert(
                                        `Czy na pewno chcesz usunąć wydarzenie?`,
                                        'Tej akcji nie da się cofnąć',
                                        [
                                            {
                                                text: 'Usuń',
                                                onPress: async () => {
                                                    await deleteEvent(eventUuid);
                                                    router.push('/(auth)/home');
                                                },
                                                style: 'destructive'
                                            },
                                            {
                                                text: 'Anuluj',
                                                onPress: () => {
                                                },
                                                style: 'cancel'
                                            }
                                        ]
                                    );
                                }}
                            >
                                <Ionicons name={'trash-outline'} size={24} color="red"/>
                                <Text
                                    className={`text-base font-bold py-3 px-3 text-center text-red-600`}>
                                    {i18n.eventBottomBar.remove}
                                </Text>
                            </TouchableOpacity>
                        </>
                    ) : (
                        <>
                            {isJoined && (
                                <TouchableOpacity
                                    className="w-full bg-primary rounded-md"
                                    onPress={() => {
                                        Alert.alert(
                                            `Czy na pewno chcesz opuścić to wydarzenie?`,
                                            undefined,
                                            [
                                                {
                                                    text: 'Opuść',
                                                    onPress: async () => {
                                                        await leaveEvent(eventUuid);
                                                        refetchEvent();
                                                    },
                                                    style: 'destructive'
                                                },
                                                {
                                                    text: 'Anuluj',
                                                    onPress: () => {
                                                    },
                                                    style: 'cancel'
                                                }
                                            ]
                                        );
                                    }}
                                >
                                    <Text
                                        className={`text-base font-bold py-3 px-3 text-center text-white`}>
                                        {i18n.eventBottomBar.leave}
                                    </Text>
                                </TouchableOpacity>
                            )}
                            {isPending && (
                                <>
                                    <TouchableOpacity
                                        className="w-full"
                                        onPress={() => {
                                            Alert.alert(
                                                `Czy na pewno chcesz anulować prośbę o dołączenie?`,
                                                undefined,
                                                [
                                                    {
                                                        text: 'Tak',
                                                        onPress: async () => {
                                                            await leaveEvent(eventUuid);
                                                            refetchEvent();
                                                        },
                                                        style: 'destructive'
                                                    },
                                                    {
                                                        text: 'Nie',
                                                        onPress: () => {
                                                        },
                                                        style: 'cancel'
                                                    }
                                                ]
                                            );
                                        }}
                                    >
                                        <Text
                                            className={`text-base underline py-3 px-3 text-center text-darkGrey`}>
                                            {i18n.eventBottomBar.cancelJoinRequest}
                                        </Text>
                                    </TouchableOpacity>
                                </>
                            )}
                            {(!isJoined && !isPending) && (
                                <>
                                    <TouchableOpacity
                                        className="w-1/2"
                                        onPress={() => {
                                            router.push(`/(auth)/chat/event:${eventUuid}`)
                                        }}
                                    >
                                        <Text
                                            className={`text-base underline py-3 px-3 text-center text-darkGrey`}>
                                            {i18n.eventBottomBar.sendRequest}
                                        </Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                        className="w-1/2 bg-primary rounded-md"
                                        onPress={() => {
                                            Alert.alert(
                                                `Czy na pewno chcesz dołączyć do tego wydarzenia?`,
                                                undefined,
                                                [
                                                    {
                                                        text: 'Dołącz',
                                                        onPress: async () => {
                                                            await joinEvent(eventUuid);
                                                            refetchEvent();
                                                        }
                                                    },
                                                    {
                                                        text: 'Anuluj',
                                                        onPress: () => {
                                                        },
                                                        style: 'cancel'
                                                    }
                                                ]
                                            );
                                        }}
                                    >
                                        <Text
                                            className={`text-base font-bold py-3 px-3 text-center text-white`}>
                                            {i18n.eventBottomBar.join}
                                        </Text>
                                    </TouchableOpacity>
                                </>
                            )}
                        </>
                    )}
                </View>
            </View>
        </>
    );
};
