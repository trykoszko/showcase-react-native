import Ionicons from '@expo/vector-icons/Ionicons';
import {useRouter} from 'expo-router';
import {useRouteInfo} from 'expo-router/src/hooks';
import {Platform, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {BottomBarItem} from './BottomBarItem';
import {useAuthStore} from '../../store/AuthStore';
import i18n from '../../i18n';
import {Router} from 'expo-router/build/types';
import {useSafeAreaInsets} from "react-native-safe-area-context";
import {sizes} from "../../constants/sizes";

type Props = {}

export default function BottomBar({}: Props) {
    const apiToken: string | undefined = useAuthStore(state => state.apiToken);
    const router: Router = useRouter();
    const currentRoute: string = useRouteInfo().pathname.replace('/', '');
    const insets = useSafeAreaInsets();

    if (!apiToken || apiToken === '') {
        return <></>;
    }

    return (
        <View
            className="absolute w-full pt-4 bottom-0 left-0 flex-row items-center justify-between px-4 border-t border-borderGrey bg-white"
            style={{
                paddingBottom:
                    Platform.OS === 'ios'
                        ? insets.bottom
                        : 20,
                height: Platform.OS === 'ios' ? sizes.bottomBarHeight : (80 + insets.bottom)
            }}
        >
            <View className="flex-row items-center">
                <BottomBarItem
                    title={i18n.bottomBar.home}
                    icon="home"
                    onPress={() => router.push('/home')}
                    isActive={currentRoute === 'home'}
                />
                <BottomBarItem
                    title={i18n.bottomBar.coorganize}
                    icon="briefcase"
                    onPress={() => router.push('/coorganize')}
                    isActive={currentRoute === 'coorganize'}
                />
            </View>
            <View className="flex-row items-center">
                <BottomBarItem
                    title={i18n.bottomBar.messages}
                    icon="chatbox"
                    onPress={() => router.push('/messages')}
                    isActive={currentRoute === 'messages'}
                />
                <BottomBarItem
                    title={i18n.bottomBar.profile}
                    icon="person"
                    onPress={() => router.push('/profile')}
                    isActive={currentRoute === 'profile'}
                />
            </View>
            <View
                className="absolute"
                style={{
                    top: -28,
                    left: '47.5%',
                }}
            >
                <TouchableOpacity
                    style={{
                        width: 56,
                        height: 56,
                        borderRadius: 56,
                        backgroundColor: '#a31caf',
                        elevation: 2,
                        shadowColor: 'black',
                        shadowOpacity: .4,
                        shadowOffset: {
                            width: 0,
                            height: 4
                        }
                    }}
                    onPress={() => router.push('/create-event')}
                    className="flex-row items-center justify-center bg-primary rounded-full"
                >
                    <Ionicons
                        name="add"
                        size={44}
                        color="white"
                        style={{
                            marginLeft: 3
                        }}
                    />
                </TouchableOpacity>
            </View>
        </View>
    );
}
