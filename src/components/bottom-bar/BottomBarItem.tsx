import Ionicons from '@expo/vector-icons/Ionicons';
import { Text, View } from "react-native";
import { TouchableOpacity } from 'react-native-gesture-handler';

type Props = {
    title: string,
    icon: any,
    onPress: () => void,
    isActive: boolean
}

export const BottomBarItem = ({ title, icon, onPress, isActive }: Props) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            className="mx-3"
            style={{
                width: 50
            }}
        >
            <View className="flex-column items-center">
                <Ionicons
                    name={isActive ? icon : `${icon}-outline`}
                    size={22}
                    color={isActive ? '#a31caf' : '#d9d9d9'}
                />
                <Text
                    className="mt-1 text-xs"
                    style={{
                        color: isActive ? '#a31caf' : '#d9d9d9',
                    }}
                >{title}</Text>
            </View>
        </TouchableOpacity>
    )
}