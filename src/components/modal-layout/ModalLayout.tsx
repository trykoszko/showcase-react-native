import { View } from "react-native"

type Props = {
    children: React.ReactNode
}

export default function ModalLayout({ children }: Props) {
    return (
        <View className="p-4">
            {children}
        </View>
    )
}
