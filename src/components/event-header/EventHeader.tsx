import {Text, TouchableOpacity, View} from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import i18n from '../../i18n';
import {useRouter} from 'expo-router';
import {isLive} from '../../helpers/date-helpers';
import {share} from '../../helpers/share';

type Props = {
    dateTime: Date,
    duration: number,
    eventUuid: string
}

export const EventHeader = ({dateTime, duration, eventUuid}: Props) =>
{
    const router      = useRouter();
    const isEventLive = isLive(dateTime, duration);

    return (
        <View className="flex flex-row justify-between items-center mx-3 mb-5">
            <TouchableOpacity
                className="bg-white rounded-full p-2"
                onPress={() => router.back()}
            >
                <Ionicons name="arrow-back" size={20} color="black"/>
            </TouchableOpacity>

            {isEventLive && (
                <View className="flex flex-row justify-start items-center px-3 py-3">
                    <View
                        style={{
                            width:           4,
                            height:          4,
                            backgroundColor: 'red'
                        }}
                    />
                    <Text className="ml-2 text-white">{i18n.eventHeader.live}</Text>
                </View>
            )}

            <TouchableOpacity
                onPress={() => share(eventUuid)}
                className="bg-white rounded-full p-2"
            >
                <Ionicons name="share-social" size={20} color="black"/>
            </TouchableOpacity>
        </View>
    );
};
