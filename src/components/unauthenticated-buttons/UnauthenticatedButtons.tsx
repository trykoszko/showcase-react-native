import {useRouter} from 'expo-router/src/hooks';
import {Text, TouchableOpacity, View} from 'react-native';
import i18n from '../../i18n';
import * as WebBrowser from 'expo-web-browser';

export const UnauthenticatedButtons = () => {
    const router = useRouter();

    return (
        <View className="flex flex-col w-full">
            <TouchableOpacity
                className="rounded-md bg-primary"
                onPress={() => {
                    router.push('/authentication');
                }}
            >
                <Text className="text-white py-3 px-4 text-center">
                    {i18n.unauthenticatedButtons.logIn}
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
                className="rounded-md mt-3"
                onPress={async () => {
                    await WebBrowser.openBrowserAsync('https://app.com/about');
                }}
            >
                <Text className="text-primary py-3 px-4 text-center">
                    {i18n.unauthenticatedButtons.about}
                </Text>
            </TouchableOpacity>
        </View>
    );
};
