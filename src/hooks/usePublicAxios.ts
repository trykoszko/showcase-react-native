import axios, {AxiosInstance, AxiosResponse} from "axios"
import {apiConfig} from "../config/variables"
import {useGlobalLoaderStore} from "../store/GlobalLoaderStore";
import {Alert} from "react-native";

export const usePublicAxios = (contentType?: string) => {
    const setIsLoading = useGlobalLoaderStore(state => state.setIsLoading);

    const axiosClient: AxiosInstance = axios.create({
        baseURL: apiConfig.url,
        headers: {
            'Content-Type': contentType ?? 'application/json',
            'Accept-Language': 'pl'
        }
    });

    axiosClient.interceptors.response.use(
        async (response: AxiosResponse) => {
            /**
             * This callback handles all 100-399 responses
             */
            setIsLoading(false);

            return response;
        },
        (error) => {
            setIsLoading(false);

            Alert.alert('Błąd', error.response.data);

            console.log('Error', error.response.data);
        }
    );

    return axiosClient;
}
