import { SplashScreen } from "expo-router"
import { useEffect, useState } from "react"
import * as Font from 'expo-font'

export default function useCachedResources() {
    const [isLoadingComplete, setLoadingComplete] = useState(false)
    
    useEffect(() => {
      async function loadResourcesAndDataAsync() {
        try {
          SplashScreen.preventAutoHideAsync()
  
          await Font.loadAsync({
            'Poppins': require('../../assets/Poppins-Regular.ttf'),
            'PoppinsBold': require('../../assets/Poppins-SemiBold.ttf'),
          })
        } catch (e) {
          console.warn(e)
        } finally {
          setLoadingComplete(true)
          SplashScreen.hideAsync()
        }
      }
  
      loadResourcesAndDataAsync()
    }, [])
  
    return { isLoadingComplete }
}
