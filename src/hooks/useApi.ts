import {useAxios} from "./useAxios";
import {MyUser} from "../types/MyUser.type";
import {useAuthStore} from "../store/AuthStore";
import {EventUserRoleEnum} from "../enums/EventUserRole.enum";
import {Alert} from "react-native";
import {UserEvent} from "../types/UserEvent.type";
import {ChatMessageTypeEnum} from "../enums/ChatMessageType.enum";

export const useApi = () => {
    const axios = useAxios();
    const userData: MyUser | undefined = useAuthStore(state => state.userData);
    const setUserData = useAuthStore(state => state.setUserData);

    const saveEvent = async (uuid: string) => {
        await axios.post(`event/${uuid}/save`);

        const savedEvents = userData?.saved_events ?? [];
        savedEvents.push(uuid);

        setUserData(<MyUser>{...userData, saved_events: savedEvents});
    }

    const unsaveEvent = async (uuid: string) => {
        await axios.post(`event/${uuid}/unsave`);

        const savedEvents = userData?.saved_events ?? [];
        const index = savedEvents.findIndex(savedEventUuid => savedEventUuid === uuid);
        savedEvents.splice(index, 1);

        setUserData(<MyUser>{...userData, saved_events: savedEvents});
    }

    const saveCoorganizeItem = async (uuid: string) => {
        await axios.post(`event-coorganize-item/${uuid}/save`);

        const savedCoorganizeItems = userData?.saved_event_event_elements ?? [];
        savedCoorganizeItems.push(uuid);

        setUserData(<MyUser>{...userData, saved_event_event_elements: savedCoorganizeItems});
    }

    const unsaveCoorganizeItem = async (uuid: string) => {
        await axios.post(`event-coorganize-item/${uuid}/unsave`);

        const savedCoorganizeItems = userData?.saved_event_event_elements ?? [];
        const index = savedCoorganizeItems.findIndex(savedItemUuid => savedItemUuid === uuid);
        savedCoorganizeItems.splice(index, 1);

        setUserData(<MyUser>{...userData, saved_event_event_elements: savedCoorganizeItems});
    }

    const deleteFriend = async (id: number) => {
        await axios.delete(`user/friend/${id}`);

        const newFriends: number[] = userData?.friends?.filter(friendId => friendId !== id) ?? [];

        setUserData(<MyUser>{...userData, friends: newFriends});
    }

    const addFriend = async (friendId: number) => {
        await axios.post(`user/friend/${friendId}`);

        const friendInvitesSent: number[] = userData?.friend_invites_sent ?? [];
        friendInvitesSent.push(friendId);

        setUserData(<MyUser>{...userData, friend_invites_sent: friendInvitesSent});
    };

    const acceptFriendRequest = async (friendId: number) => {
        await axios.post(`user/friend/${friendId}/accept`);

        const friendInvitesSent: number[] = userData?.friend_invites_sent?.filter(friendInviteSentId => friendInviteSentId !== friendId) ?? [];

        const friends: number[] = userData?.friends ?? [];
        friends.push(friendId);

        setUserData(<MyUser>{
            ...userData,
            friend_invites_sent: friendInvitesSent,
            friends
        });
    };

    const deleteEvent = async (uuid: string) => {
        await axios.delete(`event/${uuid}`);

        const events = userData?.events?.filter(event => event.uuid !== uuid) ?? [];

        setUserData(<MyUser>{...userData, events: events});
    }

    const leaveEvent = async (uuid: string) => {
        await axios.post(`event/${uuid}/leave`);

        const events: UserEvent[] = userData
                ?.events
                ?.filter((userEvent: UserEvent) => userEvent.uuid !== uuid)
            ?? [];

        setUserData(<MyUser>{
            ...userData,
            events
        });
    }

    const joinEvent = async (uuid: string) => {
        const joined: any = await axios.post(`event/${uuid}/join`);

        if (joined) {
            if (joined?.data?.statusCode > 300) {
                Alert.alert('Błąd', joined?.data?.message);
                return;
            }

            const events = userData?.events ?? [];

            events.push({
                uuid: uuid,
                role: joined?.data?.role ?? EventUserRoleEnum.PARTICIPANT,
                created_at: new Date()
            });

            setUserData(<MyUser>{...userData, events: events});
        } else {
            Alert.alert('Błąd', 'Nie udało się dołączyć do wydarzenia');
        }
    }

    const sendChatMessage = async (chatUuid: string, message: string) => {
        return axios.post(`chat/message`, {
            text: message,
            type: ChatMessageTypeEnum.DEFAULT,
            chat_uuid: chatUuid
        });
    }

    const sendCoorganizeOffer = async (eventUuid: string, eventElementUuid: string, price: number, description: string) => {
        return axios.post(`event/${eventUuid}/offer`, {
            event_element_uuid: eventElementUuid,
            price,
            description
        });
    }

    return {
        saveEvent,
        unsaveEvent,
        saveCoorganizeItem,
        unsaveCoorganizeItem,
        deleteFriend,
        addFriend,
        acceptFriendRequest,
        deleteEvent,
        leaveEvent,
        joinEvent,
        sendChatMessage,
        sendCoorganizeOffer
    }
}
