import axios, {AxiosInstance, AxiosResponse, InternalAxiosRequestConfig} from "axios"
import {apiConfig} from "../config/variables"
import {useAuthStore} from "../store/AuthStore"
import {useGlobalLoaderStore} from "../store/GlobalLoaderStore";
import {Alert} from "react-native";

const refreshJwtToken = async (
    refreshToken: string
): Promise<any> => {
    try {
        const response: AxiosResponse = await axios
            .get(`${apiConfig.url}/auth/refresh`, {
                headers: {
                    'Authorization': `Bearer ${refreshToken}`
                }
            });

        if (response?.data) {
            return {
                apiToken: response?.data?.access_token,
                refreshToken: response?.data?.refresh_token,
                eol: response?.data?.eol
            };
        } else {
            return {
                apiToken: '',
                refreshToken: '',
                eol: 1
            }
        }
    } catch (error: any) {
        console.log('refreshJwtToken Error', error);

        Alert.alert('Nie udało się zalogować ponownie', error?.response?.data?.message);

        return {
            apiToken: '',
            refreshToken: '',
            eol: 1
        }
    }
}

export const useAxios = (contentType?: string) => {
    const apiTokenFromState = useAuthStore(state => state.apiToken);
    const setApiToken = useAuthStore(state => state.setApiToken);

    const refreshTokenFromState = useAuthStore(state => state.refreshToken);
    const setRefreshToken = useAuthStore(state => state.setRefreshToken);

    const eolFromState = useAuthStore(state => state.eol);
    const setEol = useAuthStore(state => state.setEol);

    const setIsLoading = useGlobalLoaderStore(state => state.setIsLoading);

    const axiosClient: AxiosInstance = axios.create({
        baseURL: apiConfig.url,
        headers: {
            'Content-Type': 'application/json',
            'Accept-Language': 'pl'
        }
    });

    axiosClient.interceptors.request.use(async (config: InternalAxiosRequestConfig) => {
        let tokenToUse: string = apiTokenFromState ?? '';

        if (eolFromState && (new Date() > new Date(eolFromState))) {
            const refreshTokenData = await refreshJwtToken(
                "" + refreshTokenFromState
            );

            const {
                apiToken,
                refreshToken,
                eol
            } = refreshTokenData;

            setApiToken(apiToken);
            setRefreshToken(refreshToken);
            setEol(eol);

            tokenToUse = apiToken;
        }

        config.headers.Authorization = (tokenToUse?.length > 0) ? `Bearer ${tokenToUse}` : '';

        return config;
    })

    axiosClient.interceptors.response.use(
        async (response: AxiosResponse) => {
            /**
             * This callback handles all 100-399 responses
             */
            setIsLoading(false);

            return response;
        },
        async (error) => {
            setIsLoading(false);

            console.log('axiosClient Error', {
                response: error.response
            });

            Alert.alert('Wystąpił błąd', error.response.data.message);
        }
    );

    return axiosClient;
}
