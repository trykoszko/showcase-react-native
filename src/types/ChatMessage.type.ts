import {ChatMessageTypeEnum} from "../enums/ChatMessageType.enum";
import {EventOffer} from "./EventOffer.type";

export type ChatMessage = {
    id: number;
    uuid: string;
    text: string;
    type: ChatMessageTypeEnum;
    created_at: Date;

    author_id: number;
    author_uuid: string;

    offer?: EventOffer;
}
