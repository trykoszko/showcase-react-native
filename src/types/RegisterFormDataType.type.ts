import {City} from "./City.type";
import {EventElement} from "./EventElement.type";

export type RegisterFormDataTypeType = {
    type: 'company' | 'private',
    company_name?: string,
    company_description?: string,
    email?: string,
    password?: string,
    image?: any,
    company_type?: string,
    kind?: string,
    people_limit?: number,
    has_staff?: boolean,
    can_travel?: boolean,
    can_invoice?: boolean,
    handled_events: string[],
    handled_languages: string[],
    age_restriction?: string,
    preferred_contact: string[],
    other_info?: string,
    service_time?: string,
    materials?: string,
    aim?: string,
    style?: string,
    first_name?: string,
    last_name?: string,
    birthdate?: Date,

    city?: City,
    elements: EventElement[]
}
