export type UserSocialProfile = {
    id: number;
    uuid: string;
    name: string;
    url: string;
    created_at: Date;
}
