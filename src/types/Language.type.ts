export type Language = {
    id: number;
    uuid: string;
    name: string;
    native_name: string;
}
