import {UserTypeEnum} from "../enums/UserType.enum";
import {EventUserRoleEnum} from "../enums/EventUserRole.enum";
import {Asset} from "./Asset.type";

export type EventUser = {
    id: number;
    uuid: string;
    name: string;
    first_name: string;
    last_name: string;
    type: UserTypeEnum;
    company_name: string;
    company_description: string;
    role: EventUserRoleEnum;
    avatar: Asset;
    created_at: Date;
}
