export type Asset = {
    id: number;
    uuid: string;
    url: string;
}
