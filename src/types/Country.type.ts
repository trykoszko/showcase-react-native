export type Country = {
    id: number;
    uuid: string;
    name: string;
    native_name: string;
    alpha_2: string;
}
