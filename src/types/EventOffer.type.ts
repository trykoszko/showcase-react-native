import {User} from "./User.type";
import {EventEventElement} from "./EventEventElement.type";

export type EventOffer = {
    id: number;
    uuid: string;
    price: number;
    description: string;
    is_accepted: boolean;
    provider: User;
    event_element: EventEventElement;
}
