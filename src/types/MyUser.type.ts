import {UserTypeEnum} from "../enums/UserType.enum";
import {Asset} from "./Asset.type";
import {City} from "./City.type";
import {UserSocialProfile} from "./UserSocialProfile.type";
import {UserEvent} from "./UserEvent.type";
import {EventElement} from "./EventElement.type";

export type MyUser = {
    id: number;
    uuid: string;
    email: string;
    name: string;
    first_name: string;
    last_name: string;
    gender: string;
    type: UserTypeEnum;
    birthdate: Date;
    company_name: string;
    company_description: string;
    created_at: Date;

    avatar?: Asset;
    city?: City;
    event_elements?: EventElement[];
    social_profiles?: UserSocialProfile[];
    friends?: number[];
    settings?: any[]; // @TODO:
    events?: UserEvent[];
    saved_events?: string[];
    saved_event_event_elements?: string[];

    friend_invites_sent?: number[];
    friend_invites_received?: number[];

    events_viewed?: string[];
}
