export type CoorganiseType = {
    id: number,
    title: string,
    slug: string
}
