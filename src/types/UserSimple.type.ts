import {Asset} from "./Asset.type";
import {UserTypeEnum} from "../enums/UserType.enum";

export type UserSimple = {
    id: number;
    uuid: string;
    name: string;
    first_name: string;
    last_name: string;
    company_name: string;
    type: UserTypeEnum;
    avatar?: Asset;
}
