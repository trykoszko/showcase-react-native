export type Provider = {
    uuid?: string;
    title: string;
    image?: string;
}
