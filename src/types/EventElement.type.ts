export type EventElement = {
    id: number;
    uuid: string;
    name: string;
    slug: string;
    icon: string;
    color: string;
}
