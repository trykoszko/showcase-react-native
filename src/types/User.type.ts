import {UserTypeEnum} from "../enums/UserType.enum";
import {Asset} from "./Asset.type";
import {City} from "./City.type";
import {EventElement} from "./EventElement.type";
import {UserSocialProfile} from "./UserSocialProfile.type";
import {UserEvent} from "./UserEvent.type";

export type User = {
    id: number;
    uuid: string;

    name: string;

    first_name: string;
    last_name: string;
    gender: string | null;
    type: UserTypeEnum;
    birthdate: Date;
    company_name: string | null;
    avatar?: Asset;

    city?: City;
    event_elements?: EventElement[];
    social_profiles?: UserSocialProfile[];
    events?: UserEvent[];
    friends_count?: number;
}
