import {EventTemplate} from './EventTemplate.type';
import {EventElement} from './EventElement.type';

export type CreateEventFormDataType = {
    title?: string,
    description?: string,
    eventTemplate?: EventTemplate,
    eventElements?: EventElement[],
    startDate?: Date,
    unlimitedPeople?: boolean,
    peopleLimit?: number,
    limitedDuration?: boolean,
    duration?: number,
    privateEvent?: boolean,
    reviewNeeded?: boolean,
    paidEvent?: boolean,
    canJoinInProgress?: boolean,
    image?: string
}
