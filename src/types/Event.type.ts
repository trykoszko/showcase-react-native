import {Asset} from "./Asset.type";
import {City} from "./City.type";
import {EventTemplate} from "./EventTemplate.type";
import {UserEvent} from "./UserEvent.type";

export type Event = {
    id: number;
    uuid: string;
    title: string;
    slug: string;
    description: string;
    start_date: Date;
    unlimited_people: boolean;
    people_limit?: number;
    limited_duration: boolean;
    duration?: number;
    private_event: boolean;
    review_needed: boolean;
    paid_event: boolean;
    can_join_in_progress: boolean;
    city: City;
    image: Asset;
    event_template: EventTemplate;
    users: UserEvent[];
    owner_id: number;
    participant_ids: number[];
}
