import {ChatMessage} from "./ChatMessage.type";
import {UserSimple} from "./UserSimple.type";
import {ChatTypeEnum} from "../enums/ChatType.enum";
import {Event} from "./Event.type";

export type Chat = {
    id: number;
    uuid: string;
    title: string;
    created_at: Date;
    updated_at: Date;
    type?: ChatTypeEnum;
    event?: Event;
    messages: ChatMessage[];
    recipient: UserSimple;
}
