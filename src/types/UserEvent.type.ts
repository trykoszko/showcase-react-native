import {EventUserRoleEnum} from "../enums/EventUserRole.enum";
import {Event} from "./Event.type";
import {User} from "./User.type";

export type UserEvent = {
    uuid: string;
    user?: User;
    role: EventUserRoleEnum;
    event?: Event;
    created_at: Date;
}
