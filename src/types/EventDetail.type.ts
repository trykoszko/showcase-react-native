import {City} from "./City.type";
import {Asset} from "./Asset.type";
import {EventTemplate} from "./EventTemplate.type";
import {EventUser} from "./EventUser.type";
import {EventEventElement} from "./EventEventElement.type";

export type EventDetail = {
    id: number;
    uuid: string;
    title: string;
    slug: string;
    description: string;
    start_date: Date;
    unlimited_people: boolean;
    people_limit: number;
    limited_duration: boolean;
    duration: number;
    private_event: boolean;
    review_needed: boolean;
    paid_event: boolean;
    can_join_in_progress: boolean;
    city: City;
    image: Asset;
    event_template: EventTemplate;
    event_elements: EventEventElement[];
    users: EventUser[];
}
