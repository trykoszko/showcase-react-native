import {Country} from "./Country.type";

export type City = {
    id: number;
    uuid: string;
    name: string;
    country: Country;
}