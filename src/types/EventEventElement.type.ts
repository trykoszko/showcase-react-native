import {EventElement} from "./EventElement.type";
import {Event} from "./Event.type";
import {EventOffer} from "./EventOffer.type";

export type EventEventElement = {
    id: number;
    uuid: string;
    client_has: boolean;
    client_description: string;
    offer_price: number | null;
    event_element: EventElement;
    offers: EventOffer[];
    event: Event;
}
