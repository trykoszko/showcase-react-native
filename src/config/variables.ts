import Constants from 'expo-constants';

export const apiConfig = {
    url: process?.env?.EXPO_PUBLIC_API_URL ?? 'https://app.com/api/v1',
    publicUrl: process?.env?.EXPO_PUBLIC_URL ?? 'https://app.com',
};

export const appConfig = {
    name:        'App',
    frontendUrl: 'https://app.com'
};

export const facebookConfig = {
    appId:       Constants?.expoConfig?.extra?.FACEBOOK_TOKEN ?? '123456789',
    permissions: ['public_profile', 'email'],
    fields:      [
        'email',
        'first_name',
        'last_name',
        'gender',
        'link',
        'birthday'
    ]
};

// export const sizes = {
//     bottomBarHeight: 80
// }

// export const colors = {
//     purple1: '#D946EF',
//     purple2: '#A21CAF',
//     purple3: '#701A75',
//     yellow: '#FFEE4A'   
// }

export const sizes = {
    layout: {
        unauthenticated: {
            topBarHeight: 60
        }
    }
};
