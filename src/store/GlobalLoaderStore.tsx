import { create } from 'zustand'
import { subscribeWithSelector } from 'zustand/middleware'

type GlobalLoaderStore = {
    isLoading: boolean,
    setIsLoading: (value: boolean) => void
}

export const useGlobalLoaderStore = create(subscribeWithSelector<GlobalLoaderStore>(set => ({
    isLoading: false,
    setIsLoading: (value: boolean) => set({ isLoading: value }),
})));
