import {create} from 'zustand';
import {subscribeWithSelector} from 'zustand/middleware';

type TopBlurStore = {
    isTopBlurVisible: boolean,
    setIsTopBlurVisible: (value: boolean) => void
}

export const useTopBlurStore = create(subscribeWithSelector<TopBlurStore>(set => ({
    isTopBlurVisible:    false,
    setIsTopBlurVisible: (value: boolean) => set({isTopBlurVisible: value}),
})));
