import {create} from 'zustand';
import {createJSONStorage, persist, subscribeWithSelector} from 'zustand/middleware';
import {router, useNavigation, useSegments} from 'expo-router';
import {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import useCachedResources from '../hooks/useCachedResources';
import {MyUser} from "../types/MyUser.type";
import axios from "axios";
import {apiConfig} from "../config/variables";

export type AuthStore = {
    isAuthenticated: boolean,
    apiToken: string | undefined,
    setApiToken: (value: string) => void,
    eol: number | undefined,
    setEol: (value: number) => void,
    refreshToken: string | undefined,
    setRefreshToken: (value: string) => void,
    userData: MyUser | undefined,
    setUserData: (value: MyUser) => void
}

export const useAuthStore = create(
    persist(
        subscribeWithSelector<AuthStore>((set, get) => ({
            isAuthenticated: false,
            apiToken: undefined,
            setApiToken: (value: string) => set(() => ({apiToken: value, isAuthenticated: true})),
            refreshToken: undefined,
            eol: undefined,
            setEol: (value: number) => set(() => ({eol: value})),
            setRefreshToken: (value: string) => set(() => ({refreshToken: value})),
            userData: undefined,
            setUserData: (value: MyUser) => set(() => ({userData: value}))
        })),
        {
            name: 'app-auth-storage',
            storage: createJSONStorage(() => AsyncStorage),
        }
    )
);

useAuthStore.subscribe(state => state.apiToken, async (apiToken: string | undefined) => {
    if (apiToken && apiToken !== '' && !useAuthStore?.getState()?.userData) {
        await axios
            .get(`${apiConfig.url}/user/me`, {
                headers: {
                    'Authorization': `Bearer ${apiToken}`
                }
            })
            .then(response => {
                useAuthStore.setState({
                    isAuthenticated: true,
                    userData: response.data
                });
            })
            .catch(async error => {
                useAuthStore.setState({
                    isAuthenticated: false,
                    userData: undefined,
                    apiToken: undefined,
                    refreshToken: undefined
                });
            })
    } else {
        useAuthStore.setState({
            isAuthenticated: false,
            userData: undefined
        });
    }
});

export const useProtectedRoute = (apiToken: string | undefined) => {
    const segments = useSegments();
    const {isLoadingComplete} = useCachedResources();
    const [isNavigationReady, setIsNavigationReady] = useState(false);

    const navigation = useNavigation();

    navigation.addListener('state', () => {
        setIsNavigationReady(true);
    });

    useEffect(() => {
        const inAuthGroup = (segments[0] === '(auth)');

        if (isLoadingComplete && isNavigationReady) {
            if (!apiToken && inAuthGroup) {
                router.replace('/');
            }
            if (apiToken && !inAuthGroup) {
                if (segments[1] !== '[event]') {
                    router.replace('/(auth)/home');
                }
            }
        }
    }, [isNavigationReady, apiToken, segments, isLoadingComplete]);
};
