import { create } from 'zustand'
import { subscribeWithSelector } from 'zustand/middleware'

type TermsStore = {
    isTermsAccepted: boolean,
    setIsTermsAccepted: (value: boolean) => void
}

export const useTermsStore = create(subscribeWithSelector<TermsStore>(set => ({
    isTermsAccepted: false,
    setIsTermsAccepted: (value: boolean) => set({ isTermsAccepted: value }),
})))
