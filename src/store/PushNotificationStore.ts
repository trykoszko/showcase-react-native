import {Notification} from 'expo-notifications'
import {create} from 'zustand'
import {subscribeWithSelector} from 'zustand/middleware'

type PushNotificationStore = {
    notification: Notification,
    setNotification: (value: Notification) => void,
    expoPushToken: string,
    setExpoPushToken: (value: string) => void
}

export const usePushNotificationStore = create(subscribeWithSelector<PushNotificationStore>(set => ({
    notification: {} as Notification,
    setNotification: (value: Notification) => set({notification: value}),
    expoPushToken: '',
    setExpoPushToken: (value: string) => set({expoPushToken: value})
})));
