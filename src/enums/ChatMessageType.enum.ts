export enum ChatMessageTypeEnum {
    DEFAULT = 'default',
    OFFER = 'offer',
    INVITE = 'invite',
    FRIEND_INVITE = 'friend_invite'
}
