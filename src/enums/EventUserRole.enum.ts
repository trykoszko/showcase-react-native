export enum EventUserRoleEnum {
    OWNER = 'owner',
    PARTICIPANT = 'participant',
    COORGANIZER = 'coorganizer',
    PENDING_APPROVAL = 'pending_approval'
}
