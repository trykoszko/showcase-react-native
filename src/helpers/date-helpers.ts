import i18n from '../i18n';

export const differenceInDays = (date1: Date | string, date2: Date | string): number =>
{
    if (typeof date1 === 'string') {
        date1 = new Date(date1);
    }

    if (typeof date2 === 'string') {
        date2 = new Date(date2);
    }

    const diff = date1.getTime() - date2.getTime();
    return Math.floor(diff / (1000 * 60 * 60 * 24));
};

export const getDaysLeft = (date: Date | string): number =>
{
    if (typeof date === 'string') {
        date = new Date(date);
    }

    const today = new Date();
    return differenceInDays(date, today);
};

export const isLive = (startDate: Date | string, duration: number): boolean =>
{
    if (typeof startDate === 'string') {
        startDate = new Date(startDate);
    }

    const endDate = new Date(startDate);
    endDate.setHours(endDate.getHours() + duration);

    const now = new Date();
    return startDate < now && endDate > now;
};

export const getDateRange = (date1: Date | string, date2: Date | string): string =>
{
    if (typeof date1 === 'string') {
        date1 = new Date(date1);
    }

    if (typeof date2 === 'string') {
        date2 = new Date(date2);
    }

    const daysOfWeek = i18n.helpers.daysOfWeek;
    const months     = i18n.helpers.months;

    function format(date: Date)
    {
        const dayOfWeek  = daysOfWeek[date.getDay()];
        const dayOfMonth = date.getDate();
        const month      = months[date.getMonth()];
        const year       = date.getFullYear();

        return `${dayOfWeek} ${dayOfMonth} ${month} ${year}`;
    }

    if (date1.toDateString() === date2.toDateString()) {
        const dayOfWeek  = daysOfWeek[date1.getDay()];
        const dayOfMonth = date1.getDate();
        const month      = months[date1.getMonth()];
        const hour1      = String(date1.getHours()).padStart(2, '0');
        const minute1    = String(date1.getMinutes()).padStart(2, '0');
        const hour2      = String(date2.getHours()).padStart(2, '0');
        const minute2    = String(date2.getMinutes()).padStart(2, '0');

        return `${dayOfWeek} ${dayOfMonth} ${month} ${hour1}:${minute1}-${hour2}:${minute2}`;
    }

    return `${format(date1)} - ${format(date2)}`;
};

export const getEventDates = (date: Date | string, duration: number | null): string => {
    if (typeof date === 'string') {
        date = new Date(date);
    }

    const addLeadingZero = (number: number): string => String(number).padStart(2, '0');

    const startDate = new Date(date);

    if (duration) {
        const endDate = new Date(date);
        endDate.setHours(endDate.getHours() + duration);

        if (startDate.toDateString() === endDate.toDateString()) {
            return `${getFormattedDate(startDate)} - ${addLeadingZero(endDate.getHours())}:${addLeadingZero(endDate.getMinutes())}`;
        }

        return `${getFormattedDate(startDate)} - ${addLeadingZero(endDate.getHours())}:${addLeadingZero(endDate.getMinutes())}`;
    }

    return getFormattedDate(startDate);
}

export const getFormattedDate = (date: Date | string): string =>
{
    if (typeof date === 'string') {
        date = new Date(date);
    }

    const addLeadingZero = (number: number): string => String(number).padStart(2, '0');

    return `${date.getDate()} ${i18n.helpers.months[date.getMonth()]} ${date.getFullYear()} ${addLeadingZero(date.getHours())}:${addLeadingZero(date.getMinutes())}`;
};

export const getFormattedMessageDate = (date: Date | string): string =>
{
    if (typeof date === 'string') {
        date = new Date(date);
    }

    const addLeadingZero = (number: number): string => String(number).padStart(2, '0');

    // compare if the date is not today
    if (date.getDate() !== new Date().getDate()) {
        return `${date.getDate()} ${i18n.helpers.months[date.getMonth()]} ${date.getFullYear()} ${addLeadingZero(date.getHours())}:${addLeadingZero(date.getMinutes())}`;
    }

    return `${addLeadingZero(date.getHours())}:${addLeadingZero(date.getMinutes())}`;
};

export const getFormattedDateString = (date: Date): string => {
    const addLeadingZero = (number: number): string => String(number).padStart(2, '0');

    return `${date.getDate()} ${i18n.helpers.months[date.getMonth()]} ${date.getFullYear()}`;
}

export const getFormattedTimeString = (date: Date): string => {
    const addLeadingZero = (number: number): string => String(number).padStart(2, '0');

    return `${addLeadingZero(date.getHours())}:${addLeadingZero(date.getMinutes())}`;
}

