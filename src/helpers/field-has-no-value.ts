export default (fieldValue: any): boolean =>
{
    if (fieldValue === null || fieldValue === undefined) {
        return true;
    }

    if (typeof fieldValue === 'string' && fieldValue.trim() === '') {
        return true;
    }

    if (fieldValue instanceof Date && isNaN(fieldValue.getTime())) {
        return true;
    }

    return false;
}
