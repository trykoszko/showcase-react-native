import {Share} from 'react-native';
import i18n from '../i18n';

export const share = (eventUuid: string) =>
{
    Share.share({
                    title:   i18n.share.title,
                    message: i18n.share.message(eventUuid),
                },
                {
                    dialogTitle: i18n.share.title
                });
};

export const shareApp = () =>
{
    Share.share({
                    title:   i18n.share.inviteFriendsModalTitle,
                    message: i18n.share.inviteFriendsModalMessage
                },
                {
                    dialogTitle: i18n.share.inviteFriendsModalTitle,
                    subject:     i18n.share.inviteFriendsModalTitle
                });
};
