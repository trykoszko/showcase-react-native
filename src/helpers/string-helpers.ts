import i18n from '../i18n';

export const slugify = (text: string): string =>
{
    return text
        .toLowerCase()
        .replace(/ /g, '-')
        .replace(/[^\w-]+/g, '');
};

export const formatHour = (hour: number): string =>
{
    if (hour === 1) {
        return `1 ${i18n.helpers.hours.one}`;
    }
    if (hour > 1 && hour < 5) {
        return `${hour} ${i18n.helpers.hours.few}`;
    }
    if (hour >= 5 && hour < 22) {
        return `${hour} ${i18n.helpers.hours.many}`;
    }
    if (hour >= 22) {
        return `${hour} ${i18n.helpers.hours.few}`;
    }

    return '' + hour;
};
