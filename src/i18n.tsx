import {appConfig} from './config/variables';

const pl = {
    helpers: {
        daysOfWeek: ['Nie', 'Pon', 'Wto', 'Śro', 'Czw', 'Pią', 'Sob'],
        months: ['sty', 'lut', 'mar', 'kwi', 'maj', 'cze', 'lip', 'sie', 'wrz', 'paź', 'lis', 'gru'],
        currency: 'zł',
        hours: {
            one: 'godzina',
            few: 'godziny',
            many: 'godzin',
        }
    },
    layout: {
        updateAvailable: 'Dostępna jest nowa wersja aplikacji. Kliknij tutaj, aby zaktualizować.',
        errorFetchingUpdate: (err: any) => `Błąd podczas pobierania aktualizacji: ${JSON.stringify(err)}`
    },
    coorganiseOffer: {
        sendOffer: 'Wyślij ofertę',
        reject: 'Odrzuć',
        accept: 'Akceptuj',
        daysLeft: (days: number) => `Zostało ${days} dni`,
    },
    user: {
        error: 'Błąd',
    },
    authentication: {
        signIn: 'Zaloguj się',
    },
    bottomBar: {
        home: 'Home',
        coorganize: 'Zarabiaj',
        messages: 'Czaty',
        profile: 'Profil'
    },
    chatMessage: {
        you: 'Ty'
    },
    eventInvite: {
        daysLeft: (days: number) => `Zostało ${days} dni`,
        sendOffer: 'Wyślij ofertę'
    },
    coorganiseListItem: {
        daysLeft: (days: number) => `Zostało ${days} dni`,
        sendOffer: 'Wyślij ofertę',
        yourOfferPrice: 'Twoja propozycja ceny (zł)',
        yourOfferDescription: 'Opis Twojej propozycji'
    },
    eventCoorganiseItem: {
        sendOffer: 'Wyślij ofertę',
        offeredBy: 'Dostarczone przez',
        daysLeft: 'Zostało',
        days: 'dni'
    },
    unauthenticatedButtons: {
        logIn: 'Zaloguj lub zarejestruj się',
        about: 'O App'
    },
    coorganiseList: {
        title: 'Współorganizuj',
        seeAll: 'Zobacz wszystkie',
    },
    coorganiseListPublic: {
        title: 'Współorganizuj',
    },
    eventDetails: {
        creator: 'Organizator',
    },
    eventHeader: {
        live: 'Live',
    },
    imagePicker: {
        addImage: 'Dodaj zdjęcie',
    },
    loginForm: {
        heading: 'Zaloguj się',
        emailLabel: 'E-mail',
        passwordLabel: 'Hasło',
        forgotPassword: 'Zapomniałem hasła',
        submit: 'Zaloguj',
        or: 'lub',
        register: 'Zarejestruj się',
    },
    socialLoginButtons: {
        facebook: 'Facebook',
        apple: 'Apple',
        google: 'Google',
    },
    notificationComponent: {
        tapToView: 'Sprawdź'
    },
    participantList: {
        cancel: 'Anuluj',
        remove: 'Usuń',
        addCoorganizer: 'Dodaj współorganizatora',
        addedOn: 'Dodany',
    },
    registerFormFourthStepCompany: {
        title: 'registerFormFourthStepCompany',
        text: 'registerFormFourthStepCompany Text',
        kind: 'Rodzaj',
        peopleLimit: 'Limit osób',
        hasStaff: 'Obsługa',
        hasStaffHelp: 'Obsługa - help info',
        canTravel: 'Możliwość dojazdu',
        canTravelHelp: 'Możliwość dojazdu - help info',
        canInvoice: 'Możliwość wystawienia faktury',
        canInvoiceHelp: 'Możliwość wystawienia faktury - help info',
        handledEvents: 'Obsługiwane rodzaje wydarzeń',
        handledLanguages: 'Obsługiwane języki',
        ageRestriction: 'Ograniczenie wiekowe',
        ageRestrictions: [
            'brak', 'tylko dla dorosłych'
        ],
        preferredContact: 'Preferowana forma kontaktu',
        preferredContacts: [
            'telefon', 'email', 'aplikacja App'
        ],
        otherInfo: 'Pozostałe informacje',
        serviceTime: 'Czas obsługi',
        materials: 'Materiały',
        aim: 'Groupa docelowa',
        style: 'Styl',
    },
    registerFormFourthStepPrivate: {
        title: 'registerFormFourthStepPrivate',
        text: 'registerFormFourthStepPrivate Text',
        inviteFriends: 'Zaproś znajomych',
        skip: 'Pomiń'
    },
    createEventFifthStep: {
        continue: 'Dalej',
        leave: 'Opuść kreator',
        haveQuestions: 'Masz pytania?',
        step: 'Krok',
        iHave: 'Mam',
        iDontHave: 'Nie mam',
        seeAll: 'Zobacz wszystkie',
        helpTitle: 'Pomoc',
        helpDescription: 'createEventFifthStep helpDescription'
    },
    registerFormSecondStepPrivate: {
        title: 'registerFormSecondStepPrivate',
        text: 'registerFormSecondStepPrivate Text',
        bottomTextText: 'bottomTextText',
        bottomTextTermsLink: 'bottomTextTermsLink',
        bottomTextTermsText: 'bottomTextTermsText',
        bottomTextPaymentLink: 'bottomTextPaymentLink',
        bottomTextPaymentText: 'bottomTextPaymentText',
        bottomTextPrivacyLink: 'bottomTextPrivacyLink',
        bottomTextPrivacyText: 'bottomTextPrivacyText',
        join: 'Dołącz',
        logIn: 'Zaloguj się',
        image: 'Zdjęcie',
        password: 'Hasło',
        showPassword: 'Pokaż hasło',
        hidePassword: 'Ukryj hasło',
        email: 'E-mail',
        birthdate: 'Data urodzenia',
        location: 'Lokalizacja',
        pickLocation: 'Wybierz lokalizację',
        name: 'Imię',
        lastName: 'Nazwisko',
    },
    registerFormThirdStepCompany: {
        title: 'registerFormThirdStepCompany',
        text: 'registerFormThirdStepCompany Text',
        chooseAttributes: 'Wybierz atrybuty',
        addAttributes: 'Dodaj atrybuty...',
        facebook: 'Facebook',
        google: 'Google',
        apple: 'Apple',
        addOther: 'Dodaj inne',
        skip: 'Pomiń',
        next: 'Dalej',
        yourPortfolio: 'Twoje portfolio',
    },
    eventParticipants: {
        title: 'Lista osób',
        friends: 'znajomych',
        slots: 'miejsc',
        modalTitle: 'Lista osób'
    },
    registerFormSixthStepCompany: {
        title: 'registerFormSixthStepCompany',
        text: 'registerFormSixthStepCompany Text',
        skip: 'Pomiń',
        submitError: 'Błąd',
        submitErrorText: 'Niektóre pola nie zostały wypełnione poprawnie. Sprawdź poprzednie kroki formularza',
        congratulations: 'Gratulacje!',
        accountCreated: 'Twoje konto zostało założone. Potwierdź swój adres email i zaloguj się, by móc korzystać z aplikacji.',
    },
    registerFormSecondStepCompany: {
        title: 'registerFormSecondStepCompany',
        text: 'registerFormSecondStepCompany Text',
        companyName: 'Nazwa firmy',
        description: 'Opis',
        location: 'Lokalizacja',
        pickLocation: 'Wybierz lokalizację',
        email: 'E-mail',
        password: 'Hasło',
        showPassword: 'Pokaż hasło',
        hidePassword: 'Ukryj hasło',
        image: 'Zdjęcie',
        bottomTextText: 'bottomTextText',
        bottomTextTermsLink: 'bottomTextTermsLink',
        bottomTextTermsText: 'bottomTextTermsText',
        bottomTextPaymentLink: 'bottomTextPaymentLink',
        bottomTextPaymentText: 'bottomTextPaymentText',
        bottomTextPrivacyLink: 'bottomTextPrivacyLink',
        bottomTextPrivacyText: 'bottomTextPrivacyText',
        join: 'Dołącz',
        logIn: 'Zaloguj się',
        emailError: 'E-mail jest niepoprawny'
    },
    registerFormThirdStepPrivate: {
        title: 'registerFormThirdStepPrivate',
        text: 'registerFormThirdStepPrivate Text',
        chooseAttributes: 'Wybierz atrybuty',
        addAttributes: 'Dodaj atrybuty...',
        yourPortfolio: 'Twoje portfolio',
        facebook: 'Facebook',
        google: 'Google',
        apple: 'Apple',
        addOther: 'Dodaj inne',
        next: 'Dalej',
        skip: 'Pomiń'
    },
    authenticationScreen: {
        signIn: 'Zaloguj się do aplikacji',
    },
    indexScreen: {
        title: (name?: string) => `Witaj w App${name ? `, ${name}` : ''}!`,
        subtitle: 'Znajdź wydarzenia dla siebie'
    },
    homeScreen: {
        title: (name?: string) => `Witaj w App${name ? `, ${name}` : ''}!`,
        subtitle: 'Znajdź wydarzenia dla siebie'
    },
    lockedEventScreen: {
        logIn: 'Zaloguj się',
        about: 'O App',
    },
    privacyPolicyScreen: {
        title: 'Polityka prywatności',
        content: ``,
        buttonText: 'OK'
    },
    chatScreen: {
        error: 'Błąd',
        noMessages: 'Brak wiadomości',
        insertMessage: 'Wpisz wiadomość'
    },
    coorganizeScreen: {
        title: 'Współorganizuj',
        filterTitle: 'Filtruj oferty',
        elementsFetchError: 'Błąd podczas pobierania elementów',
        noElements: 'Brak elementów',
        noItemsFoundMatchingFilter: 'Brak elementów spełniających kryteria',
    },
    eventScreen: {
        eventNotFound: 'Nie znaleziono wydarzenia',
        offerModalTitle: 'Wyślij ofertę',
        yourOfferPrice: 'Twoja propozycja ceny (zł)',
        yourOfferDescription: 'Opis Twojej propozycji',
        submitOffer: 'Wyślij propozycję'
    },
    eventsScreen: {
        title: 'Wydarzenia',
        create: 'Stwórz wydarzenie',
        error: 'Błąd',
        noEventsFoundMatchingFilter: 'Brak wydarzeń spełniających kryteria',
        filtersTitle: 'Filtruj wydarzenia',
    },
    friendsScreen: {
        inviteTitle: 'Zaproś znajomych',
        inviteMessage: 'Hej! Sprawdź aplikację App. Stwórz swoje pierwsze wydarzenie i zaproś znajomych już teraz!',
        inviteFriendsOutsideApp: 'Zaproś znajomych spoza App',
        searchFriendInApp: 'Znajdź znajomego w App',
        seeFriend: 'Zobacz znajomego',
        removeFriend: 'Usuń znajomego',
        friendRemoved: 'Usunięto znajomego',
        cancel: 'Anuluj',
        addedOn: 'Dodany',
    },
    messagesScreen: {
        error: 'Błąd',
        chats: 'Czaty',
        noMessagesTitle: 'Brak wiadomości',
        noMessagesText: 'Zacznij rozmowę z kimś, kto jest w App',
    },
    profileScreen: {
        editProfile: 'Edytuj profil',
        friends: 'Znajomi',
        events: 'Wydarzenia',
        userAttributes: 'Atrybuty',
        startEarning: 'Zacznij zarabiać',
        logout: 'Wyloguj',
        attributes: 'Atrybuty',
        sendMessage: 'Wyślij wiadomość',
    },
    eventFilters: {
        all: 'Wszystkie',
        today: 'Dziś',
        recent: 'Ostatnio dodane',
        created: 'Utworzone',
        coorganized: 'Współorganizowane',
        around: 'W okolicy',
        joined: 'Dołączone',
        saved: 'Zapisane',
    },
    share: {
        title: 'Udostępnij',
        message: (eventUuid: string) => `Hej! Sprawdź to wydarzenie w App! ${appConfig.frontendUrl}/event/${eventUuid}`,
        inviteFriendsModalTitle: 'Udostępnij aplikację',
        inviteFriendsModalMessage: 'Hej! Sprawdź aplikację App. Stwórz swoje pierwsze wydarzenie i zaproś znajomych już teraz!',
    },
    coorganizeFilters: {
        all: 'Wszystkie',
        saved: 'Zapisane',
        nearMe: 'W okolicy',
    },
    eventsCarouselPublic: {
        title: 'Wydarzenia',
        seeAll: 'Zobacz wszystkie',
        live: 'Live',
        startsIn: (days: number) => `Start za ${days} dni`
    },
    eventsCarousel: {
        title: 'Wydarzenia',
        seeAll: 'Zobacz wszystkie',
        live: 'Live',
        startsIn: (days: number) => `Start za ${days} dni`
    },
    staticEventsCarousel: {
        title: 'Wydarzenia',
        seeAll: 'Zobacz wszystkie',
        live: 'Live',
        startsIn: (days: number) => `Start za ${days} dni`
    },
    createEventFirstStep: {
        start: 'Rozpocznij',
        title: 'Wybierz typ wydarzenia',
        subtitle: 'Wybierz typ wydarzenia, które chcesz stworzyć',
    },
    createEventSecondStep: {
        errorTitle: 'Błąd',
        errorDescription: 'createEventSecondStep errorDescription',
        continue: 'Dalej',
        leave: 'Opuść kreator',
        haveQuestions: 'Masz pytania?',
        step: 'Krok',
        title: 'Podstawowe informacje',
        description: 'Opis wydarzenia',
        templatesTitle: 'Szablony',
        templatesSearch: 'Szukaj...',
        helpTitle: 'Pomoc',
        helpDescription: 'createEventSecondStep helpDescription'
    },
    createEventThirdStep: {
        continue: 'Dalej',
        leave: 'Opuść kreator',
        haveQuestions: 'Masz pytania?',
        step: 'Krok',
        title: 'Podstawowe informacje',
        description: 'Opis wydarzenia',
        elementsSearch: 'Szukaj elementów...',
        helpTitle: 'Pomoc',
        helpDescription: 'createEventThirdStep helpDescription'
    },
    createEventFourthStep: {
        error: 'Błąd',
        titleError: 'Nazwa wydarzenia jest wymagana i powinna być dłuższa niż 10 znaków',
        continue: 'Dalej',
        leave: 'Opuść kreator',
        haveQuestions: 'Masz pytania?',
        upload: 'Dodaj zdjęcie',
        step: 'Krok',
        title: 'Podstawowe informacje',
        titlePlaceholder: 'Spotkanie ze znajomymi...',
        description: 'Opis wydarzenia',
        descriptionPlaceholder: 'Zapraszam na wydarzenie...',
        startDate: 'Start wydarzenia',
        peopleLimit: 'Ilość osób',
        unlimitedPeople: 'Nieograniczona liczba osób',
        duration: 'Czas trwania',
        privateEvent: 'Wydarzenie prywatne',
        reviewNeeded: 'Wymagana akceptacja',
        paidEvent: 'Wydarzenie płatne',
        canJoinInProgress: 'Można dołączyć w trakcie',
        durationHelpContent: 'Czas trwania - help info',
        canJoinInProgressHelpContent: 'Można dołączyć w trakcie - help info',
        privateEventHelpContent: 'Wydarzenie prywatne - help info',
        reviewNeededHelpContent: 'Wymagana akceptacja - help info',
        paidEventHelpContent: 'Wydarzenie płatne - help info',
        helpTitle: 'Pomoc',
        helpDescription: 'createEventFourthStep helpDescription'
    },
    userScreen: {
        friends: 'Znajomi',
        events: 'Wydarzenia',
        userAttributes: 'Atrybuty',
        sendMessage: 'Wyślij wiadomość',
        attributes: 'Atrybuty',
        addToFriends: 'Dodaj do znajomych',
        addToFriendsQuestion: 'Czy na pewno chcesz dodać tego użytkownika do znajomych?',
        deleteFriend: 'Usuń znajomego',
        deleteFriendQuestion: 'Czy na pewno chcesz usunąć tego użytkownika z znajomych?',
        acceptFriendRequest: 'Akceptuj zaproszenie do znajomych',
        acceptFriendRequestQuestion: 'Czy na pewno chcesz zaakceptować zaproszenie do znajomych?',
        requestSent: 'Wysłano zaproszenie do znajomych',
        yes: 'Tak',
        cancel: 'Anuluj'
    },
    registerFormFirstStep: {
        title: 'Wybierz rodzaj konta',
        text: 'Na początek wybierz rodzaj Twojego konta. Zakładając konto firmowe nastawiasz się na udzielanie usług i wspomaganie innych uczestników współtworząc ich wydarzenia. Konto prywatne natomiast służy głównie tworzeniu i uczestniczeniu w eventach.',
        private: 'Konto prywatne',
        company: 'Konto firmowe',
    },
    eventListItem: {
        startsIn: (days: number) => `Start za ${days} dni`,
        owner: 'Organizator'
    },
    eventDescription: {
        showMore: 'Pokaż więcej',
        showLess: 'Pokaż mniej'
    },
    eventLocation: {
        title: 'Lokalizacja wydarzenia',
        distance: (distance: number) => `${distance}km stąd`
    },
    ownedEventLocation: {
        title: 'Lokalizacja wydarzenia',
        distance: (distance: number) => `${distance}km stąd`
    },
    eventChatInfo: {
        title: 'Czat wydarzenia',
        people: 'osób'
    },
    eventBottomBar: {
        edit: 'Edytuj',
        join: 'Dołącz',
        leave: 'Opuść wydarzenie',
        remove: 'Usuń wydarzenie',
        cancelJoinRequest: 'Anuluj prośbę o dołączenie',
        sendRequest: 'Wyślij zapytanie',
    },
    registerFormFifthStepCompany: {
        title: 'registerFormFifthStepCompany',
        text: 'registerFormFifthStepCompany Text',
        inviteFriends: 'Zaproś znajomych',
        skip: 'Pomiń'
    },
    ownedEventCoorganizeItem: {
        cancel: 'Anuluj',
        remove: 'Usuń',
        daysLeft: (days: number) => `Zostało ${days} dni`,
    },
    registerFormFifthStepPrivate: {
        title: 'registerFormFifthStepPrivate',
        text: 'registerFormFifthStepPrivate Text',
        skip: 'Pomiń',
        registerError: 'Wystąpił błąd podczas rejestracji. Spróbuj ponownie później.',
        submitError: 'Błąd',
        submitErrorText: 'Niektóre pola nie zostały wypełnione poprawnie. Sprawdź poprzednie kroki formularza',
        congratulations: 'Gratulacje!',
        accountCreated: 'Twoje konto zostało założone. Potwierdź swój adres email i zaloguj się, by móc korzystać z aplikacji.',
    }
};

export const i18n = pl;

export default i18n;
