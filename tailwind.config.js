/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./app/**/*.{js,jsx,ts,tsx}", "./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        heading: ["Poppins"],
        headingBold: ["PoppinsBold"],
      },
      colors: {
        lightGrey: '#ececec',
        borderGrey: '#eeedf3',
        darkGrey: '#3c3c3c',
        primary: '#952aa9',
        primaryLight: '#da64f5',
        primaryDark: '#a31caf',
        white: '#ffffff',
        yellow: '#FFEE4A',
      }
    },
  },
  plugins: [],
}

