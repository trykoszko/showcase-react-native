# Events app

A React Native application for event management.

Please note it's still under development and can contain some not-production-ready code.

## Features

- Written in TypeScript
- Uses Expo for builds and deployments
- Uses Zustand for state management
- Uses React Hook Form for form management, Yup for form validation
- Uses Axios, React Query for API requests
- Uses a backend service written by myself in Nest.js (not included in this repository)

## Screenshots

[<img src="./screenshots/IMG_7503.PNG" width="250"/>](./screenshots/IMG_7503.PNG)
[<img src="./screenshots/IMG_7504.PNG" width="250"/>](./screenshots/IMG_7504.PNG)
[<img src="./screenshots/IMG_7505.PNG" width="250"/>](./screenshots/IMG_7505.PNG)
[<img src="./screenshots/IMG_7506.PNG" width="250"/>](./screenshots/IMG_7506.PNG)
[<img src="./screenshots/IMG_7507.PNG" width="250"/>](./screenshots/IMG_7507.PNG)
[<img src="./screenshots/IMG_7508.PNG" width="250"/>](./screenshots/IMG_7508.PNG)
[<img src="./screenshots/IMG_7509.PNG" width="250"/>](./screenshots/IMG_7509.PNG)
[<img src="./screenshots/IMG_7510.PNG" width="250"/>](./screenshots/IMG_7510.PNG)

### Author: Michal Trykoszko
