import AuthenticationScreen from '../src/screens/AuthenticationScreen';
import i18n from '../src/i18n';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {router} from 'expo-router';
import Ionicons from '@expo/vector-icons/Ionicons';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';

export default function Authentication()
{
    return (
        <SafeAreaView
            style={{
                backgroundColor: '#fbfbfb',
                paddingBottom:   0,
                height:          '100%'
            }}
            edges={['top']}
        >
            <View
                className={`flex flex-row justify-between items-center border-gray-300 border-b pt-2 pb-6 px-3`}>
                <TouchableOpacity
                    onPress={() => router.back()}
                >
                    <Ionicons name="chevron-back" size={24} color="black"/>
                </TouchableOpacity>
                <Text className="text-sm font-bold">{i18n.authentication.signIn}</Text>
                <View style={{width: 24}}/>
            </View>
            <ScrollView
                scrollEventThrottle={16}
            >
                <AuthenticationScreen/>
            </ScrollView>
        </SafeAreaView>
    );
}
