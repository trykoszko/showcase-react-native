import {GoogleSignin} from '@react-native-google-signin/google-signin';
import * as Notifications from 'expo-notifications';
import {Stack} from 'expo-router/stack';
import React, {useEffect, useRef} from 'react';
import {QueryClientProvider} from 'react-query';
import {registerForPushNotificationsAsync} from '../src/config/pushNotifications';
import {queryClient} from '../src/query/Query';
import {useAuthStore, useProtectedRoute} from '../src/store/AuthStore';
import {usePushNotificationStore} from '../src/store/PushNotificationStore';
import {usePathname} from 'expo-router';
import BottomBar from '../src/components/bottom-bar/BottomBar';
import GlobalLoader from '../src/components/global-loader/GlobalLoader';
import {NotificationComponent} from "../src/components/notification/NotificationComponent";
import {Alert, Linking, Vibration} from "react-native";
import * as Updates from 'expo-updates';
import i18n from "../src/i18n";

GoogleSignin.configure({
    iosClientId: '123456789'
});

const Layout = () => {
    const apiToken = useAuthStore(state => state.apiToken);
    const setExpoPushToken = usePushNotificationStore(state => state.setExpoPushToken);
    const setNotification = usePushNotificationStore(state => state.setNotification);
    const notificationListener = useRef<any>();
    const responseListener = useRef<any>();
    const route = usePathname();

    const bottomBarHiddenRoutes: string[] = [
        '/event/',
        '/create-event',
        '/chat/'
    ];
    const hideBottomBar: boolean = bottomBarHiddenRoutes.some((routeName) => route.startsWith(routeName));

    const updateAndReloadApp = async () => {
        if (process.env.NODE_ENV !== 'development') {
            try {
                const update: Updates.UpdateCheckResult = await Updates.checkForUpdateAsync();

                if (update.isAvailable) {
                    Alert.alert(i18n.layout.updateAvailable);

                    await Updates.fetchUpdateAsync();
                    await Updates.reloadAsync();
                }
            } catch (error) {
                Alert.alert(i18n.layout.errorFetchingUpdate(error));
            }
        }
    }

    useEffect(() => {
        updateAndReloadApp();
    }, []);

    useEffect(() => {
        registerForPushNotificationsAsync()
            .then(async token => {
                if (token && token !== '') {
                    setExpoPushToken(token);
                }
            });

        notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
            Vibration.vibrate([
                100, 100
            ]);
            setNotification(notification);
        });

        responseListener.current = Notifications.addNotificationResponseReceivedListener(response =>
                                                                                         {
                                                                                             const url: string = response?.notification?.request?.content?.data?.url ?? null;
                                                                                             if (url) {
                                                                                                 Linking.openURL(url);
                                                                                             }
                                                                                         });

        return () => {
            Notifications.removeNotificationSubscription(notificationListener.current);
            Notifications.removeNotificationSubscription(responseListener.current);
        };
    }, []);

    useProtectedRoute(apiToken);

    return (
        <QueryClientProvider client={queryClient}>
            <Stack
                screenOptions={{
                    headerShown: false,
                    contentStyle: {backgroundColor: 'white'}
                }}
            />
            <NotificationComponent/>
            <GlobalLoader/>
            {!hideBottomBar && (
                <BottomBar/>
            )}
            {/*<TopBlur/>*/}
        </QueryClientProvider>
    );
};

export default Layout;
