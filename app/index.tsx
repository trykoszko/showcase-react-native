import IndexScreen from '../src/screens/IndexScreen';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';

export default function Index()
{
    return (
        <SafeAreaView
            style={{
                backgroundColor: 'white',
                paddingBottom:   0,
            }}
            edges={['top', 'bottom']}
        >
            <IndexScreen/>
        </SafeAreaView>
    );
}
