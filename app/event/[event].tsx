import {useRouteInfo} from 'expo-router/src/hooks';
import {EventScreen} from '../../src/screens/authenticated/EventScreen';
import React from 'react';
import {useAuthStore} from '../../src/store/AuthStore';
import {LockedEventScreen} from '../../src/screens/LockedEventScreen';
import {useAxios} from "../../src/hooks/useAxios";
import {useQuery} from "react-query";
import Loader from "../../src/components/loader/Loader";
import {EventDetail} from "../../src/types/EventDetail.type";
import {Text, View} from "react-native";
import {EventUser} from "../../src/types/EventUser.type";
import {AxiosResponse} from "axios";
import {EventUserRoleEnum} from "../../src/enums/EventUserRole.enum";
import {OwnedEventScreen} from "../../src/screens/authenticated/OwnedEventScreen";
import {EventBottomBar} from "../../src/components/event-bottom-bar/EventBottomBar";

export default function SingleEvent() {
    const route = useRouteInfo();
    const userData = useAuthStore(store => store.userData);
    const apiToken = useAuthStore(store => store.apiToken);
    const isLocked = !apiToken || apiToken === '';
    const axios = useAxios();

    const {data, error, isFetching, refetch} = useQuery<unknown, unknown, EventDetail>({
        queryKey: ['single_event', route.params.event],
        queryFn: async () => {
            if (!route.params.event) return null;

            try {
                const res: AxiosResponse = await axios.get(`event/${route.params.event}`)

                return res?.data ?? null;
            } catch (e) {
                console.log('single_event exception', e)
            }
        }
    });

    if (error) {
        return <Text>Error: {JSON.stringify(error)}</Text>;
    }

    if (isLocked) {
        return <LockedEventScreen/>;
    }

    if (!data) {
        return (
            <View
                className={"flex-1 items-center justify-center"}
            >
                <Loader/>
            </View>
        );
    }

    // if (!event) {
    //     return (
    //         <ScrollableLayoutFullheight
    //             pb={100}
    //         >
    //             <Text>{i18n.eventScreen.eventNotFound}</Text>
    //         </ScrollableLayoutFullheight>
    //     );
    // }

    const eventOwner: EventUser | undefined = data?.users.find((eventUser: EventUser) => eventUser.role === EventUserRoleEnum.OWNER);
    if (eventOwner?.uuid === userData?.uuid) {
        return (
            <>
                <OwnedEventScreen event={data} isFetching={isFetching} refetch={refetch}/>
                <EventBottomBar event={data} refetchEvent={refetch}/>
            </>
        );
    }

    return (
        <>
            <EventScreen event={data} isFetching={isFetching} refetch={refetch}/>
            <EventBottomBar event={data} refetchEvent={refetch}/>
        </>
    );
}
