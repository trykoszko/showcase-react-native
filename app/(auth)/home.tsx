import { HomeScreen } from "../../src/screens/authenticated/HomeScreen";

export default function Home() {
    return (
        <HomeScreen />
    )
}
