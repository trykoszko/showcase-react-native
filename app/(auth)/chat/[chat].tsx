import {useLocalSearchParams} from 'expo-router/src/hooks';
import React from "react";
import {ChatScreen} from "../../../src/screens/authenticated/ChatScreen";

export default function SingleChat()
{
    const local    = useLocalSearchParams();
    const chatUuid: string = "" + local.chat;

    return (
        <ChatScreen chatTypeAndUuid={chatUuid} />
    );
}
