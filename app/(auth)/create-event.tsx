import {CreateEventScreen} from '../../src/screens/authenticated/CreateEventScreen';

export default function CreateEvent()
{
    return (
        <CreateEventScreen/>
    );
}
