import {useLocalSearchParams} from 'expo-router/src/hooks';
import {UserScreen} from '../../../src/screens/authenticated/UserScreen';
import {User} from '../../../src/types/User.type';
import {useQuery} from "react-query";
import {AxiosInstance, AxiosResponse} from "axios";
import {Text, View} from "react-native";
import Loader from "../../../src/components/loader/Loader";
import React from "react";
import {useAxios} from "../../../src/hooks/useAxios";
import ScrollableLayoutFullheight from "../../../src/components/layout/ScrollableLayoutFullheight";
import i18n from "../../../src/i18n";

export default function SingleUser()
{
    const local    = useLocalSearchParams();
    const userUuid = local.user;
    const axios: AxiosInstance = useAxios();

    const {data, error, isFetching, refetch} = useQuery<unknown, unknown, User>({
        queryKey: ['single_user', userUuid],
        queryFn: async () => {
            try {
                const res: AxiosResponse = await axios.get(`user/${userUuid}`)

                return res?.data ?? null;
            } catch (e) {
                console.log('single_user exception', e)
            }
        }
    });

    if (error) {
        return (
            <ScrollableLayoutFullheight>
                <Text>{i18n.user.error}: {JSON.stringify(error)}</Text>
            </ScrollableLayoutFullheight>
        )
    }

    if (!data) {
        return (
            <View
                className={"flex-1 items-center justify-center"}
            >
                <Loader/>
            </View>
        );
    }

    return (
        <UserScreen user={data} refetch={refetch}/>
    );
}
