import RegisterScreen from '../src/screens/RegisterScreen';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';

export default function Register()
{
    return (
        <SafeAreaView
            style={{
                backgroundColor: '#fbfbfb',
                paddingBottom:   0,
                height:          '100%'
            }}
            edges={['top']}
        >
            <RegisterScreen/>
        </SafeAreaView>
    );
}
